/*
 * @Author: 红尘痴子
 * @Date: 2020-07-24 11:35:45
 * @LastEditTime: 2020-09-10 09:50:08
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\resource\lib\mystore.js
 * @https://www.jiangcan95.com/
 */
const StoreS = require('./store');
import { http } from '../../utils/http';
import { api } from '../../utils/api.js';
let store = new StoreS({
	// 状态
	state: {
		pageThis:'', // 页面 this指针
		isAuthUserInfo: false, // 是否需要授权用户信息
		isAuthPhone: false, // 是否需要授权手机号
		pageToJumpTo: '', // 即将跳转的页面
		wxUserInfo:'', // 微信用户信息
		getInfo:'', // 后端返回的用户信息
		msg: '这是一个全局状态',
		getPhoneNumber: false,
		login: false,
		user: {
			name: '李四',
		},
	},
	// 方法
	methods: {
		// 微信授权
		bindGetUserInfo(e) {
			// console.log(http, api);
			let that = this;
			//用户按了允许授权按钮
			if (e.detail.userInfo) {
				http({
					url: api.post_my_updateUserInfo,
					data: {
						encryptedData: e.detail.encryptedData,
						iv: e.detail.iv,
						rawData: e.detail.rawData,
						signature: e.detail.signature,
					},
				})
					.then((res) => {
						// that.onLoad();
						this.get_my_getInfo();
					})
					.catch((err) => {});
				getApp().store.setState({
					login: true,
					getPhoneNumber: true,
					// loginType: 1,
				});
				wx.setStorageSync('login', true);
			} else {
				wx.showToast({
					title: '您取消了授权',
					icon: 'none',
					duration: 1500,
				});
			}
		},

		// 获取用户基本数据
		get_my_getInfo() {
			console.log('获取用户基本数据---');
			let that = this;
			http({
				url: api.get_my_getInfo,
				data: {},
				//method: 'get',
			})
				.then((res) => {
					console.log('获取用户基本数据---res--',res)
					// that.setData({
					// 	getInfo: res.data.data,
					// 	profPic: res.data.data[0].avatarurl, //头像
					// 	invitationCd: res.data.data[0].invite_code, // 邀请码
					// });

					store.setState({
						// getInfo: res.data.data,
						infoData: res.data.data[0],
					});

					// console.log('this.state----',store.setState)
					console.log('this.state.wxUserInfo----',this.$state.wxUserInfo)
					// this.state.setState({
					// 	login: true,
					// 	getPhoneNumber: true,
					// 	// loginType: 1,
					// });
					// app.store.setState({
					// 	infoData: res.data.data[0],
					// });
					if (res.data.data[0].phone) {
						app.store.setState({
							getPhoneNumber: false,
						});
					}
				})
				.catch((err) => {});
		},

		// 手机号
		getPhoneNumber(e) {
			console.log('手机号--111--',e)
			let that = this;
			console.log();
			// 登录
			wx.login({
				success(res) {
					let header = {};
					if (res.code) {
						http({
							url: '/user/login/wxLogin',
							data: {
								code: res.code,
							},
							header: header,
						})
							.then((res) => {
								wx.setStorageSync('token', res.data.data.token);
								if (e.detail.iv) {
									http({
										url: api.post_index_bindingPhone,
										data: {
											encryptedData: e.detail.encryptedData,
											iv: e.detail.iv,
										},
									})
										.then((res) => {
											console.log('手机--22--res---',res)
											that.get_my_getInfo();
											getApp().store.setState({
												login: true,
												getPhoneNumber: false,
												loginType: 2,
												isAuthPhone: false,
											});

											// that.onLoad();

										})
										.catch((err) => {});
								} else {
									wx.showToast({
										title: '您取消了授权',
										icon: 'none',
										duration: 1500,
									});
								}
							})
							.catch((err) => {});
					}
				},
				fail: (err) => {},
			});
		},
		// 绑定邀请码
		bindInvitationCd() {
			console.log('---------999999999----------------',getApp().store.$state.invite_code)
			console.log('---------7878787878----------------',getApp().store.$state.token)
			let that = this;
			http({
				url: api.post_my_bindInviteCode,
				data: {
					// code: getApp().store.$state.invite_code,
					code: getApp().store.$state.invite_code,
				},
				header: {
					Authorization: getApp().store.$state.token,
				},
			})
				.then((res) => {
					console.log('--------------------res--565656565656565--',res)
					getApp().store.setState({
						shareIt: 1,
						invite_code: '',
					});
				})
				.catch((err) => {
					console.log('--------------------err--565656565656565--',err)
					getApp().store.setState({
						shareIt: 1,
						invite_code: '',
					});
				});
		},
		sayHello() {},
	},
});
module.exports = store
