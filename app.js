/*
 * @Author: 红尘痴子
 * @Date: 2020-06-29 19:37:25
 * @LastEditors: 红尘痴子
 * @LastEditTime: 2020-09-17 15:36:18
 * @Description:
 */
//app.js
import { http, api_url } from './utils/http.js';
import { qm } from './utils/qm.js';
import util from './utils/util.js';
import { api } from './utils/api.js';
// import QQMapWX from './utils/qqmap-wx-jssdk.min.js';
import store from './resource/lib/mystore';
// // 实例化API核心类
// let qqmapsdk = new QQMapWX({
// 	key: 'S5MBZ-GNQ3P-3O5DC-VB4QY-VGWOT-D6FN6',
// });
App({
	onLaunch: function (options) {
		let that = this;
		// 版本更新管理器
		if (wx.canIUse('getUpdateManager')) {
			const updateManager = wx.getUpdateManager();
			updateManager.onCheckForUpdate(function (res) {
				if (res.hasUpdate) {
					updateManager.onUpdateReady(function () {
						wx.showModal({
							title: '更新提示',
							content: '新版本已经准备好，是否重启应用？',
							success: function (res) {
								wx.clearStorageSync();
								if (res.confirm) {
									updateManager.applyUpdate();
								}
							},
						});
					});
					updateManager.onUpdateFailed(function () {
						wx.showModal({
							title: '已经有新版本了哟~',
							content:
								'新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
						});
					});
				}
			});
		} else {
			wx.showModal({
				title: '提示',
				content:
					'当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。',
			});
		}
		// 登录
		wx.login({
			success(res) {
				let token = wx.getStorageSync('token');
				let header;
				if (token) {
					header = {
						Authorization: token,
					};
				} else {
					header = {};
				}
				if (res.code) {
					that.http({
						url: '/user/login/wxLogin',
						data: {
							code: res.code,
						},
						header: header,
					})
						.then((res) => {
							store.setState({
								infoData: res.data.data,
								token: res.data.data.token,
							});
							if (res.data.data.phone == '0') {
								store.setState({ loginType: 1 });
							} else {
								store.setState({ loginType: 0 });
							}
							wx.setStorageSync('infoData', res.data.data);
							wx.setStorageSync('token', res.data.data.token);
							that.globalData.checkLogin = true;
							// getApp().store.setState({
							// 	phoneNumber: res.data.data.phone
							// });
							//由于这里是网络请求，可能会在 Page.onLoad 之后才返回
							// 所以此处加入 callback 以防止这种情况
							if (that.checkLoginReadyCallback) {
								that.checkLoginReadyCallback(res);
							}
						})
						.catch((err) => {});
				}
			},
			fail: (err) => {},
		});
		// 获取用户信息
		wx.getSetting({
			success: (res) => {
				if (res.authSetting['scope.userInfo']) {
					// 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
					wx.getUserInfo({
						success: (res) => {
							// 可以将 res 发送给后台解码出 unionId
							this.globalData.userInfo = res.userInfo;

							// 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
							// 所以此处加入 callback 以防止这种情况
							if (this.userInfoReadyCallback) {
								this.userInfoReadyCallback(res);
							}
						},
					});
				}
			},
		});
		// 查询用户是否授权过
		wx.getSetting({
			success: (res) => {
				if (res.authSetting['scope.userInfo']) {
					// 已授权
					wx.setStorageSync('login', true);
					store.setState({
						login: true,
					});
				} else {
					// 未授权 登陆状态设置为false
					wx.setStorageSync('login', false);
					store.setState({
						login: false,
					});
				}
			},
		});
		// 自定义导航栏高度
		let sysinfo = wx.getSystemInfoSync();
		let isiOS = sysinfo.system.indexOf('iOS') > -1;
		that.globalData.statusHeight = sysinfo.statusBarHeight;
		if (!isiOS) {
			that.globalData.navHeight = 48;
		} else {
			that.globalData.navHeight = 44;
		}
	},
	// 微信支付
	wxRequestPayment(obj) {
		return new Promise((resolve, reject) => {
			wx.requestPayment({
				timeStamp: obj.timeStamp,
				nonceStr: obj.nonceStr,
				package: obj.package,
				signType: obj.signType,
				paySign: obj.paySign,
				success: (res) => {
					resolve(res);
				},
				fail: (err) => {
					reject(err);
				},
			});
		});
	},
	globalData: {
		pageThis:'', // 页面this
		userInfo: null,
		statusHeight: 0,
		navHeight: 0,
		checkLogin: false,
	},
	http,
	api_url,
	qm,
	api,
	// qqmapsdk,
	util,
	store,
});
