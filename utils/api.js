/*
 * @Author: 红尘痴子
 * @Date: 2020-07-07 15:11:18
 * @LastEditTime: 2020-09-09 10:46:24
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\utils\api.js
 * @https://www.jiangcan95.com/
 */

const api = {
	// 测试删除用户信息
	del: '/user/order/test2',
	orderTest: '/user/order/test3',
	//接口列表
	// 首页
	get_index_banner: '/view/index/Banner', //首页轮播图
	get_index_videoType: '/view/index/videoType', //首页视频分类
	get_index_videos: '/video/index/list', //首页视频列表
	get_index_textLog: '/view/index/textLogB', //首页记录条
	get_index_videoGet: '/video/index/get', //首页视频详情
	get_index_getSlide: '/video/index/getSlide', //视频详情上下切换
	get_index_liveBroadcastBanner: '/view/index/liveBroadcastBanner', //直播栏目广告图
	//商城
	homeDisplay: '/view/index/homeDisplay', //商城首页分类菜单
	get_mall_mallType: '/mall/mallType', //商城-商品分类
	get_mall_malls: '/mall/malls', //商城-商品列表
	get_mall_ProductBanner: '/view/index/ProductBanner', //商城-商城轮播图
	get_mall_list: '/mall/activity/list', //商城-拼团活动列表
	get_mall_getMalls: '/mall/index/getMalls', //商城-商城获取详情
	get_mall_get: '/mall/activity/get', //商城-商城获取详情
	get_mall_commentgetMalls: '/mall/comment/list', //商城-商品评论列表
	get_mall_payData: '/mall/index/payData', //商城-商品评论列表
	get_mall_pay: '/mall/pay', //商城-调用支付api统一下单
	get_mall_notify: '/mall/notify', //商城-用户支付成功后的回调操作
	get_index_getActivityLogId: '/mall/index/getActivityLogId', //根据视频id和活动id获取到该视频发布者对应的拼团列表id，支付页面需要
	get_comment_add: '/mall/comment/add', //商品评论-添加
	get_mall_indexPay: '/mall/index/pay', //商品支付
	get_mall_getBrand: '/mall/index/getBrand', //品牌介绍
	// 我的
	get_my_getInfo: '/user/index/getInfo', //我的页面-用户信息
	post_my_checkInviteCode: '/user/index/checkInviteCode', //我的页面-填写邀请码
	post_my_bindInviteCode: '/user/index/bindInviteCode', //我的页面-确认绑定邀请人
	post_my_updateUserInfo: '/user/index/updateUserInfo', //我的页面-登陆授权信息返回给后台
	get_my_myWallet: '/user/index/myWallet', //我的页面-钱包
	post_my_getBalances: '/user/index/getBalances', //我的页面-钱包页面-余额
	post_my_userViewLevel: '/view/index/userViewLevel', //我的页面-掌柜说明-轮播图
	post_my_myFollow: '/user/video/myFollow', //我的页面-关注列表
	post_my_myFans: '/user/video/myFans', //我的页面-粉丝列表
	post_my_delFollow: '/user/video/delFollow', //我的页面-取消关注
	post_my_addFollow: '/user/video/addFollow', //我的页面-添加关注
	post_my_myVideo: '/user/video/myVideo', //我的视频-头部数据
	post_my_myVideoList: '/user/video/myVideoList', //我的视频-作品
	post_my_myLikeVideo: '/user/video/myLikeVideo', //我的视频-喜欢
	post_my_myVideoComment: '/user/video/myVideoComment', //我的视频-评论
	post_my_delVideo: '/user/video/delVideo', //我的视频-删除
	post_my_videoHome: '/user/video/videoHome', //个人视频页
	post_my_getVolume: '/user/branch/getVolume', //我的页面-购物券
	post_my_rechargeVolume: '/user/branch/rechargeVolume', //我的页面-购买购物券
	post_video_getType: '/user/video/getType', //发布视频-视频分类
	post_branch_myActivity: '/user/branch/myActivity', //我的页面-我的活动
	post_index_addCollect: '/user/index/addCollect', //我的收藏-添加收藏
	post_index_delCollect: '/user/index/delCollect', //我的收藏-取消收藏
	post_index_myCollect: '/user/index/myCollect', //我的收藏-我的收藏
	post_index_videoReward: '/user/index/videoReward', //钱包页面-视频收益
	post_index_bindingPhone: '/user/index/bindingPhone', //绑定手机号
	post_branch_myActivityInfo: '/user/branch/myActivityInfo', //我的活动-活动详情
	post_branch_myAddrList: '/user/branch/myAddrList', //我的收货地址
	post_branch_addAddress: '/user/branch/addAddress', //添加收货地址
	post_branch_delAddress: '/user/branch/delAddress', //添加收货地址
	post_branch_exchangeMiao: '/user/branch/exchangeMiao', //喵呗兑换
	post_branch_tutorWechat: '/user/branch/tutorWechat', //导师微信
	get_order_list: '/order/index/list', //订单列表
	get_order_get: '/order/index/get', //订单详情
	post_video_delVideoFile: '/user/video/delVideoFile', //删除视频文件
	post_video_getGoods: '/user/video/getGoods', //视频-添加分享商品
	post_video_getActivityLog: '/user/video/getActivityLog', //视频-添加分享活动
	post_video_publishVideoLog: '/user/video/publishVideoLog', //发布视频-提交发布
	post_branch_rechargeVolume: '/user/branch/rechargeVolume', //我的页面-购买购物券
	post_index_getLevelPower: '/user/index/getLevelPower', //获取用户特权
	get_share_list: '/share/index/list', //获取用户特权
	get_video_shareVideo: '/video/index/shareVideo', //视频分享
	post_lookActivity: '/user/branch/lookActivity', //增加活动浏览记录
	post_getActivityList: '/user/branch/getActivityList', //获取活动浏览记录
	index_getInfo: '/volume/index/getInfo', //查看购物券详情

	my_withdrawalList: '/user/index/withdrawalList', //我的页面-提现记录
	my_myCustomer: '/user/index/myCustomer', // 我的页面-我的客户
	mb_getMiao: '/user/index/getMiao', // 喵呗页面---喵呗
	mb_getMiaoList: '/user/index/getMiaoList', // 喵呗页面---喵呗兑换记录
	mb_historyList: '/user/index/historyList', // 喵呗页面---历史概况
	wallet_sharingRewards: '/user/index/sharingRewards', // 钱包页面---分享奖励/拼团奖励
	wallet_teamReward: '/user/index/teamReward', // 钱包页面---团队奖励
	withdrawal_getMyCard: '/user/branch/getMyCard', // 提现页面---我的银行卡，选择银行卡
	orcBank: '/user/branch/orcBank', // 上传图片

	branch_addWithdrawal: '/user/branch/addWithdrawal', // 申请提现

	branch_checkCard: '/user/branch/checkCard', // 添加银行卡-校验
	branch_addCard: '/user/branch/addCard', // 添加银行卡
	video_index_newList: '/video/index/newList', // 刚进来的视频详情，3条
	video_comment_list: '/video/comment/list', // 视频评论列表
	video_comment_getChildList: '/video/comment/getChildList', // 视频二级评论列表

	user_video_addComment: '/user/video/addComment', // 添加一级评论
	user_video_addCommentChild: '/user/video/addCommentChild', // 添加二级评论
	user_video_videoChecklike: '/user/video/videoChecklike', // 视频喜欢
	order_index_cancel: '/order/index/cancel', // 取消订单
	order_index_payment: '/order/index/payment', // 付款
	order_index_del: '/order/index/del', // 删除订单
	order_index_receipt: '/order/index/receipt', // 确认收货
	service_index_apply: '/service/index/apply', // 订单-申请售后
	api_index_uploads: '/api/index/uploads', // 上传文件
	service_index_list: '/service/index/list', // 售后列表
	service_index_logisticsList: '/service/index/logisticsList', // 获取物流公司
	service_index_get: '/service/index/get', // 售后详情
	service_index_updateExchange: '/service/index/updateExchange', // 售后详情--买家确认发货物流
	user_video_addWatchRecord: '/user/video/addWatchRecord', // 视频观看记录

	// 规则说明
	ruleSpecifications: '/view/index/ruleSpecifications', //所有等级规则说明
	getRule: '/mall/activity/getRule', //活动规则
	view_index_viewHomeVideoHide: '/view/index/viewHomeVideoHide', // 是否显示视频

	// 合成海报
	wxaCode: '/api/index/wxaCode', //二维码海报
	myCardShare: '/api/index/myCardShare', //分享标题和海报

	//直播
	getList: '/live/index/getList', //直播列表
	createLive: '/live/index/createLive', //创建直播间
	uploadImg: '/live/index/uploadImg', //直播上传图片
	getTypeList: '/live/index/getTypeList', //直播分类列表
	getGoodsList: '/live/index/getGoodsList', // 直播商品池列表
	selectGoods: '/live/index/selectGoods', // 直播商品选择
	delGoods: '/live/index/delGoods', // 移除选中的直播商品
	getliveMiao: '/live/money/getliveMiao', //直播佣金
	getLiveListMiao: '/live/money/getLiveListMiao', //直播佣金
	getNoticeList: '/live/index/getNoticeList', //直播广场预告
	getMyList: '/live/index/getMyList', // 直播中心
	delLive: '/live/index/delLive', // 直播中心
	volumeIndexGetList: '/volume/index/getList', // 购物卷活动列表

	mallIndexSumTotal: '/mall/index/sumTotal', // 商品详情支付页面，计算价格
	mallIndexShopHomeList: '/mall/index/shopHomeList', // 商城首页-爆款、福利、新品
	mallIndexCoupon: '/mall/index/coupon', // 商品详情-优惠卷领取列表
	userCouponApplyCoupon: '/user/coupon/applyCoupon', // 商品详情-优惠卷领取
	mallIndexApply_coupon: '/mall/index/apply_coupon', // 商品支付页面---查询可用的优惠卷
	userCouponMyCouponList: '/user/coupon/myCouponList', // 我的---领取的优惠券列表

	// ========================= 店铺 =========================
	shopIndexCreateShop: '/shop/index/createShop', // 店铺申请
	shopShopGoodsGoodsList: '/shop/shopGoods/goodsList', // 店铺商品
	shopShopGoodsEditStatus: '/shop/shopGoods/editStatus', // 店铺商品-上架下架操作
	shopCouponCreateCoupon: '/shop/coupon/createCoupon', // 店铺-创建优惠券
	shopCouponCouponList: '/shop/coupon/couponList', // 店铺-优惠券列表
	shopIndexInfo: '/shop/index/info', // 我的---我的店铺
	shopShopGoodsEditLives: '/shop/shopGoods/editLives', // 商品-更改直播状态
	shopShopgoodsGoodsDel: '/shop/shopGoods/goodsDel', // 商品-删除
	shopIndexCheckShop: '/shop/index/checkShop', // 判断店铺申请状态
	shopIndexGetReapplyInfo: '/shop/index/getReapplyInfo', // 店铺申请失败后---获取重新申请店铺的数据
	shopOrderMyOrderList: '/shop/order/myOrderList', // 店铺订单列表
	shopOrderGetOrderInfo: '/shop/order/getOrderInfo', // 店铺订单详情
	shopIndexGetRevenue: '/shop/index/getRevenue', // 店铺营收数据
	shopServiceList: '/shop/service/list', // 售后列表
	shopServiceGetInfo: '/shop/service/getInfo', // 店铺订单售后
	shopOrderDeliverGoods: '/shop/order/deliverGoods', // 店铺订单发货
  shopIndexGetShopNavList: '/shop/index/getShopNavList', // 获取线下门店经营类目

	// ========================= 签到打卡 =========================
	signIndexUserSignDetail: '/sign/index/user_sign_detail', // 用户打卡记录
	signIndexSignIn: '/sign/index/sign_in', // 签到打卡

	// ========================= 购物车 =========================
	shoppingIndexShoppingList: '/shopping/index/shoppingList', // 购物车列表
	shoppingIndexCheck: '/shopping/index/check', // 购物车-更改pages/shoppingCart/shoppingCart选中状态
	shoppingIndexFavorites: '/shopping/index/favorites', // 加入购物车
  shoppingIndexEditNum: '/shopping/index/editNum', // 更新购物车商品数量
  mallIndexGetGoodsAttr: '/mall/index/getGoodsAttr', // 获取商品规格
	shoppingIndexReAttr: '/shopping/index/reAttr', // 重新选择商品规格
	shoppingIndexDel: '/shopping/index/del', // 删除购物车商品
	shoppingIndexShoppingListSumMoney: '/shopping/index/shoppingListSumMoney', // 计算金额
	shoppingPayPayList: '/shopping/pay/payList', // 购物车/多商品支付数据
  shoppingPayPay: '/shopping/pay/pay', // 购物车支付
};

export { api };
