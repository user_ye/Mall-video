const formatTime = (date) => {
	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getDate();
	const hour = date.getHours();
	const minute = date.getMinutes();
	const second = date.getSeconds();

	return (
		year + ' 年 ' + formatNumber(month) + ' 月 ' + formatNumber(day) + ' 日'
	);
};

const formatTimeMDHM = (value) => {
	let date = new Date(value * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	let M =
		(date.getMonth() + 1 < 10
			? '0' + (date.getMonth() + 1)
			: date.getMonth() + 1) + '-';
	let D =
		date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
	let h =
		date.getHours() < 10
			? '0' + date.getHours() + ':'
			: date.getHours() + '' + ':';
	let m =
		date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
	console.log('Y + M + D + h + m + s;', M + D + h + m); // 07-20 19:36
	return M + D + h + m;
};

const timestampToTime = (value) => {
	let date = new Date(value * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	let Y = date.getFullYear() + '.';
	let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '.';
	let D = (date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ');
	// let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + '' + ':';
	// let m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
	// let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
	return Y + M + D; // 年月日
}

const formatTimeYMDhms = (value) => {
	let date = new Date(value * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	let Y = date.getFullYear() + '-';
	let M =
		(date.getMonth() + 1 < 10
			? '0' + (date.getMonth() + 1)
			: date.getMonth() + 1) + '-';
	let D =
		date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
	let h =
		date.getHours() < 10
			? '0' + date.getHours() + ':'
			: date.getHours() + '' + ':';
	let m =
		date.getMinutes() < 10
			? '0' + date.getMinutes() + ':'
			: date.getMinutes() + ':';
	let s =
		date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
	return Y + M + D + h + m + s; // 2020-07-23 16:19:01
};

const formatNumber = (n) => {
	n = n.toString();
	return n[1] ? n : '0' + n;
};

// 提示框显示
function showModal(content, isShowCancel = false, title = '提示', cancelText = '取消', confirmText = '确定') {
	return new Promise(function(resolve, reject) {
		wx.showModal({
			title: title,
			content: content,
			showCancel: isShowCancel,
			cancelText: cancelText,
			confirmText: confirmText,
			success(res) {
				if (res.confirm) {
					resolve();
				} else if (res.cancel) {
					reject();
				}
			}
		})
	})
}


/*身份证验证输入是否正确
 *身份证号合法性验证
 *支持15位和18位身份证号
 *支持地址编码、出生日期、校验位验证*/
const getBirthAndSex = (code) => {
	var city = {
		11: '北京',
		12: '天津',
		13: '河北',
		14: '山西',
		15: '内蒙古',
		21: '辽宁',
		22: '吉林',
		23: '黑龙江 ',
		31: '上海',
		32: '江苏',
		33: '浙江',
		34: '安徽',
		35: '福建',
		36: '江西',
		37: '山东',
		41: '河南',
		42: '湖北 ',
		43: '湖南',
		44: '广东',
		45: '广西',
		46: '海南',
		50: '重庆',
		51: '四川',
		52: '贵州',
		53: '云南',
		54: '西藏 ',
		61: '陕西',
		62: '甘肃',
		63: '青海',
		64: '宁夏',
		65: '新疆',
		71: '台湾',
		81: '香港',
		82: '澳门',
		91: '国外 ',
	};
	var tip = '';
	var pass = true;
	var reg = /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/;
	if (!code || !code.match(reg)) {
		tip = '身份证号格式错误';
		pass = false;
	} else if (!city[code.substr(0, 2)]) {
		tip = '地址编码错误';
		pass = false;
	} else {
		//18位身份证需要验证最后一位校验位
		if (code.length == 18) {
			code = code.split('');
			//∑(ai×Wi)(mod 11)
			//加权因子
			var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
			//校验位
			var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
			var sum = 0;
			var ai = 0;
			var wi = 0;
			for (var i = 0; i < 17; i++) {
				ai = code[i];
				wi = factor[i];
				sum += ai * wi;
			}
			var last = parity[sum % 11];
			if (parity[sum % 11] != code[17]) {
				// tip = "校验位错误";
				tip = '请核验身份证信息是否正确';
				pass = false;
			}
		}
	}
	if (pass) return true;
	if (!pass) {
		wx.showToast({
			title: tip,
			icon: 'none',
		});
	}
};

/**
 * 验证手机号
 */
const verifyMobilePhone = (mobile) => {
	let reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
	if (mobile == '') {
		wx.showToast({
			title: '手机号不能为空',
			icon: 'none',
			duration: 1500,
		});
		return false;
	} else if (!reg.test(mobile)) {
		wx.showToast({
			title: '手机号无法识别！',
			icon: 'none',
		});
		return false;
	} else {
		return true;
	}
};

/**
 * 验证邮箱
 */
const checkEmail = (email) => {
	let str = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
	if (str.test(email)) {
		return true;
	} else {
		wx.showToast({
			title: '请输入正确的邮箱号',
			icon: 'none',
		});
		return false;
	}
};

/**
 * 使用微信内置地图查看位置
 */
const wxOpenLocation = (obj) => {
	wx.openLocation(obj);
};

module.exports = {
	showModal,
	formatTime,
	getBirthAndSex,
	verifyMobilePhone,
	checkEmail,
	wxOpenLocation,
	formatTimeMDHM,
	formatTimeYMDhms,
	timestampToTime,
};
