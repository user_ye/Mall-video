let app = getApp();

var touchStartX = 0; //触摸时的原点
var touchStartY = 0; //触摸时的原点
var time = 0; // 时间记录，用于滑动时且时间小于1s则执行左右滑动
var interval = ''; // 记录/清理时间记录
var touchMoveX = 0; // x轴方向移动的距离
var touchMoveY = 0; // y轴方向移动的距离

module.exports = /******/ (function (modules) {
	// webpackBootstrap
	/******/ // The module cache
	/******/ var installedModules = {}; // The require function
	/******/
	/******/ /******/ function __webpack_require__(moduleId) {
		/******/
		/******/ // Check if module is in cache
		/******/ if (installedModules[moduleId]) {
			/******/ return installedModules[moduleId].exports;
			/******/
		} // Create a new module (and put it into the cache)
		/******/ /******/ var module = (installedModules[moduleId] = {
			/******/ i: moduleId,
			/******/ l: false,
			/******/ exports: {},
			/******/
		}); // Execute the module function
		/******/
		/******/ /******/ modules[moduleId].call(
			module.exports,
			module,
			module.exports,
			__webpack_require__
		); // Flag the module as loaded
		/******/
		/******/ /******/ module.l = true; // Return the exports of the module
		/******/
		/******/ /******/ return module.exports;
		/******/
	} // expose the modules object (__webpack_modules__)
	/******/
	/******/
	/******/ /******/ __webpack_require__.m = modules; // expose the module cache
	/******/
	/******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
	/******/
	/******/ /******/ __webpack_require__.d = function (exports, name, getter) {
		/******/ if (!__webpack_require__.o(exports, name)) {
			/******/ Object.defineProperty(exports, name, {
				enumerable: true,
				get: getter,
			});
			/******/
		}
		/******/
	}; // define __esModule on exports
	/******/
	/******/ /******/ __webpack_require__.r = function (exports) {
		/******/ if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
			/******/ Object.defineProperty(exports, Symbol.toStringTag, {
				value: 'Module',
			});
			/******/
		}
		/******/ Object.defineProperty(exports, '__esModule', { value: true });
		/******/
	}; // create a fake namespace object // mode & 1: value is a module id, require it // mode & 2: merge all properties of value into the ns // mode & 4: return value when already ns object // mode & 8|1: behave like require
	/******/
	/******/ /******/ /******/ /******/ /******/ /******/ __webpack_require__.t = function (
		value,
		mode
	) {
		/******/ if (mode & 1) value = __webpack_require__(value);
		/******/ if (mode & 8) return value;
		/******/ if (
			mode & 4 &&
			typeof value === 'object' &&
			value &&
			value.__esModule
		)
			return value;
		/******/ var ns = Object.create(null);
		/******/ __webpack_require__.r(ns);
		/******/ Object.defineProperty(ns, 'default', {
			enumerable: true,
			value: value,
		});
		/******/ if (mode & 2 && typeof value != 'string')
			for (var key in value)
				__webpack_require__.d(
					ns,
					key,
					function (key) {
						return value[key];
					}.bind(null, key)
				);
		/******/ return ns;
		/******/
	}; // getDefaultExport function for compatibility with non-harmony modules
	/******/
	/******/ /******/ __webpack_require__.n = function (module) {
		/******/ var getter =
			module && module.__esModule
				? /******/ function getDefault() {
						return module['default'];
				  }
				: /******/ function getModuleExports() {
						return module;
				  };
		/******/ __webpack_require__.d(getter, 'a', getter);
		/******/ return getter;
		/******/
	}; // Object.prototype.hasOwnProperty.call
	/******/
	/******/ /******/ __webpack_require__.o = function (object, property) {
		return Object.prototype.hasOwnProperty.call(object, property);
	}; // __webpack_public_path__
	/******/
	/******/ /******/ __webpack_require__.p = ''; // Load entry module and return exports
	/******/
	/******/
	/******/ /******/ return __webpack_require__((__webpack_require__.s = 0));
	/******/
})(
	/************************************************************************/
	/******/ [
		/* 0 */
		/***/ function (module, exports, __webpack_require__) {
			'use strict';

			Component({
				options: {
					addGlobalClass: true,
					pureDataPattern: /^_/,
				},
				properties: {
					duration: {
						type: Number,
						value: 500,
					},
					easingFunction: {
						type: String,
						value: 'default',
					},
					loop: {
						type: Boolean,
						value: true,
					},
					// videoList: {
					//     type: Array,
					//     value: [],
					//     observer: function observer() {
					//         var newVal = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
					//
					//         this._videoListChanged(newVal);
					//     }
					// }
				},
				data: {
					nextQueue: [],
					prevQueue: [],
					curQueue: [],
					addtimeNum: '',
					circular: false,
					index: 0,
					videoIndex: 1,
					firstIndex: true,
					srcUrl: '',
					_last: 1,
					_change: -1,
					_invalidUp: 0,
					_invalidDown: 0,
					_videoContexts: [],
					videoList: [
						{
							id: 5,
							video:
								'https://admin.miaommei.com/uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4',
						},
						{
							id: 6,
							video:
								'https://admin.miaommei.com/uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4',
						},
						{
							id: 7,
							video:
								'https://admin.miaommei.com/uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4',
						},
					],
				},
				lifetimes: {
					attached: function attached() {
						this.data._videoContexts = [
							wx.createVideoContext('video_0', this),
							wx.createVideoContext('video_1', this),
							wx.createVideoContext('video_2', this),
						];
					},
				},
				methods: {
					_videoListChanged: function _videoListChanged(newVal) {
						var _this = this;

						var data = this.data;
						newVal.forEach(function (item) {
							data.nextQueue.push(item);
						});
						if (data.curQueue.length === 0) {
							this.setData(
								{
									curQueue: data.nextQueue.splice(0, 3),
								},
								function () {
									_this.playCurrent(1);
								}
							);
						}
					},
					animationfinish: function animationfinish(e) {
						var _data = this.data,
							_last = _data._last,
							_change = _data._change,
							curQueue = _data.curQueue,
							prevQueue = _data.prevQueue,
							nextQueue = _data.nextQueue;

						var current = e.detail.current;
						var diff = current - _last;
						if (diff === 0) return;
						this.data._last = current;
						this.playCurrent(current);
						//this.triggerEvent('change', { activeId: curQueue[current].id });
						var direction =
							diff === 1 || diff === -2 ? 'up' : 'down';
						if (direction === 'up') {
							if (this.data._invalidDown === 0) {
								var change = (_change + 1) % 3;
								var add = nextQueue.shift();
								var remove = curQueue[change];
								if (add) {
									prevQueue.push(remove);
									curQueue[change] = add;
									this.data._change = change;
								} else {
									this.data._invalidUp += 1;
								}
							} else {
								this.data._invalidDown -= 1;
							}
						}
						if (direction === 'down') {
							if (this.data._invalidUp === 0) {
								var _change2 = _change;
								var _remove = curQueue[_change2];
								var _add = prevQueue.pop();
								if (_add) {
									curQueue[_change2] = _add;
									nextQueue.unshift(_remove);
									this.data._change = (_change2 - 1 + 3) % 3;
								} else {
									this.data._invalidDown += 1;
								}
							} else {
								this.data._invalidUp -= 1;
							}
						}
						var circular = true;
						if (nextQueue.length === 0 && current !== 0) {
							circular = false;
						}
						if (prevQueue.length === 0 && current !== 2) {
							circular = false;
						}
						this.setData({
							curQueue: curQueue,
							circular: circular,
						});
					},
					playCurrent: function playCurrent(current) {
						this.data._videoContexts.forEach(function (ctx, index) {
							index !== current ? ctx.pause() : ctx.play();
						});
					},
					onPlay: function onPlay(e) {
						this.trigger(e, 'play');
					},
					onPause: function onPause(e) {
						this.trigger(e, 'pause');
					},
					onEnded: function onEnded(e) {
						this.trigger(e, 'ended');
					},
					onError: function onError(e) {
						this.trigger(e, 'error');
					},
					onTimeUpdate: function onTimeUpdate(e) {
						this.trigger(e, 'timeupdate');
					},
					onWaiting: function onWaiting(e) {
						this.trigger(e, 'wait');
					},
					onProgress: function onProgress(e) {
						this.trigger(e, 'progress');
					},
					// bindchange: function (e) {
					//   // this.trigger(e, 'callSomeFun');
					//   // console.log('------');
					//
					//
					// },
					bindchange: function (e) {
						let that = this;
						// 拿取索引和上一次的索引值
						let current = e.detail.current;
						var lastIndex = that.data.videoIndex;
						var get = { order: '' };

						// 第一次滑动时 swiper 指定的 current 索引为 1
						// 第一次滑动时 lastIndex 视频当前索引为 0
						// 第一次滑动时 that.data.videoList 视频列表长度为 3
						// console.log(current, lastIndex,that.data.videoList.length)
						// console.log('****************************')

						// 上并且初始化 下一条
						// 如果 swiper 指定的索引为 0 ，也就是轮播滑动到第一张是
						// lastIndex 视频当前索引 == 视频长度 - 1
						if (
							current == 0 &&
							lastIndex == that.data.videoList.length - 1
						) {
							// console.log('上 并 初始化')
							current = 0;
							get.order = '>';
							// 下并且初始化 上一条
						} else if (
							current == that.data.videoList.length - 1 &&
							lastIndex == 0
						) {
							// console.log('下 并 初始化111111')
							get.order = '<';
						} else {
							// console.log(current > that.data.videoIndex ? '上滑' : '下滑')
							get.order =
								current > that.data.videoIndex ? '>' : '<';
						}
						// console.log('****************************')
						// 更新索引
						this.setData({
							videoIndex: current,
							current,
						});
						// that.setData({current});
						// console.log(that.data.videoList)
						//get.val = that.data.videoList[0].add_time
						//const val = that.data.add_timeNum.toString() - 0
						var options = that.data.options;
						//console.log('每次切换，滑动后需要拿取类型',options)
						// console.log('请求参数------',get,that.data.addtimeNum)
						// 发起请求
						app.http({
							url: app.api.get_index_getSlide,
							data: {
								// type: options.type,
								// is_hot: options.is_hot,
								type: 0,
								is_hot: 1,
								order: get.order,
								val: that.data.addtimeNum - 0 || 1100,
								//  val: val
							},
							//method: 'get',
						})
							.then((res) => {
								// wx.hideLoading()

								// console.log('切换上下条数据----',res.data.data.list)

								var list = [
									res.data.data.list,
									res.data.data.list,
									res.data.data.list,
								];
								/*
              let newList = this.data.videoList;

              newList[0].add_time =  res.data.data.list.add_time
              newList[1].add_time =  res.data.data.list.add_time
              newList[2].add_time =  res.data.data.list.add_time
              console.log('------------------',newList[2].add_time,'-------------')
              newList.splice(0,1,res.data.data.list)
              var t = ''
              for(var i = 0; i <newList.length;i++){
                t += newList[i].add_time+'---------------------'
              }
              console.log(t)
              */

								//console.log('替换后的操作',newList)
								// var list =  [res.data.data.list]
								// let newList = this.data.videoList;
								// newList.push(...list)
								// newList.push(...list)
								// newList.splice(0,2)
								that.setData({
									videoList: [
										res.data.data.list,
										res.data.data.list,
									],
									addtimeNum: res.data.data.list.add_time,
								});
								setTimeout(() => {
									var index = this.data.current || 0;
									for (var i = 0; i < 2; i++) {
										let bb = wx.createVideoContext(
											`video_${i}`
										);
										setTimeout(() => {
											i == index ? bb.play() : bb.stop();
										}, 50);
									}
								}, 400);
								// let videoId = this.data.videoList[0].id + ''; // 视频id
								// 先停止全部视频，下面再播放，注意：id必须为字符串
								// this.data.videoList.map((res) => {
								// 	let aa = wx.createVideoContext(res.id + '');
								// 	aa.stop();
								// });
								// setTimeout(function () {
								// 	// 播放当前视频进行播放
								// 	let bb = wx.createVideoContext(videoId);
								// 	bb.play();
								// });
							})
							.catch((err) => {
								// console.log(err);
							});
						//that.get_index_getSlide(get);
						// return false;

						// 解决滑动视频时视频上方盒子被隐藏的bug
						// 每次滑动先隐藏box，然后再显示
						this.setData({
							isShowVideoRightBox: false,
							isShowVideoCommodityBox: false,
							commodityBoxLeft: '-600rpx', // 先隐藏下方视频
						});
						setTimeout(function () {
							that.setData({
								isShowVideoRightBox: true, // 显示右边盒子
							});
							// 滑动显示下方商品盒子
							setTimeout(function () {
								that.setData({
									isShowVideoCommodityBox: true, // 先显示
								});
								setTimeout(function () {
									that.setData({
										commodityBoxLeft: '0rpx', // 再偏移
									});
								}, 200);
							}, 350);
						}, 350);
					},
					onLoadedMetaData(e) {
						var bool = this.data.firstIndex;
						if (bool) {
							let aa = wx.createVideoContext('video_1');

							setTimeout(() => {
								aa.play();
							}, 30);
							this.setData({
								firstIndex: false,
							});
						}

						// let aa = wx.createVideoContext('video_0');
						//             let bb = wx.createVideoContext('video_1');
						/*
          for(var i = 0; i < 2;i++){
            let bb = wx.createVideoContext(`video_${i}`);
            setTimeout(()=>{
              i == index ? bb.play() : bb.stop()
            },50)
          }*/

						//e.play()
						//this.trigger(e, 'loadedmetadata');
					},
					trigger: function trigger(e, type) {
						var ext =
							arguments.length > 2 && arguments[2] !== undefined
								? arguments[2]
								: {};

						var detail = e.detail;
						var activeId = e.target.dataset.id;
						this.triggerEvent(
							type,
							Object.assign(
								Object.assign(Object.assign({}, detail), {
									activeId: activeId,
								}),
								ext
							)
						);
					},
				},
			});

			/***/
		},
		/******/
	]
);
