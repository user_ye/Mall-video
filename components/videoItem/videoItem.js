/*
 * @Author: 红尘痴子
 * @Date: 2020-06-30 15:25:04
 * @LastEditors: 红尘痴子
 * @LastEditTime: 2020-08-10 10:59:04
 * @Description:
 */

Component({
	options: {
		addGlobalClass: true,
	},
	/**
	 * 组件的属性列表
	 */
	properties: {
		items: Array,
		isHot: Number,
		type: Number,
	},

	/**
	 * 组件的初始数据
	 */
	data: {},

	/**
	 * 组件的方法列表
	 */
	methods: {
		onVideoItem(e) {
			let that = this;
			let id = e.currentTarget.dataset.id;
			let is_hot = that.data.isHot;
			let type = that.data.type;
			wx.navigateTo({
				url: `/pages/video/video?id=${id}&is_hot=${is_hot}&type=${type}`,
			});
		},
		// 跳到用户页面
		jump(e) {
			let that = this;
			let id = e.currentTarget.dataset.id;
			wx.navigateTo({
				url: `/pages/video/authorInformation/authorInformation?id=${id}`,
			});
		},
	},
});
