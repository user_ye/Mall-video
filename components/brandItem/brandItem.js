/*
 * @Author: 红尘痴子
 * @Date: 2020-06-30 15:25:04
 * @LastEditors: 红尘痴子
 * @LastEditTime: 2020-08-04 19:16:29
 * @Description:
 */

Component({
	options: {
		addGlobalClass: true,
	},
	/**
	 * 组件的属性列表
	 */
	properties: {
		items: Array,
		btnType: Number, //判断按钮显示文案  1是分享  2是开团  3是立即购买
		purSts: Number, //判断是否是拼团  1是 2不是
		activityId: Number, //判断是否是拼团  1是 2不是
		videoid: Number, //判断是否是拼团  1是 2不是
		// zeroPrice: Boolean, // 是否显示零拼价
		zeroPrice:{ // 是否显示零拼价
			type:Boolean,
			value:true
		},
	},

	/**
	 * 组件的初始数据
	 */
	data: {},

	/**
	 * 组件的方法列表
	 */
	methods: {
		_goCoupon(e) {
			console.log('_goCoupon----------------------')
			let that = this;
			let index = 0;
			let id = e.currentTarget.dataset.item.id;
			let activityId =
				that.data.activityId != 0 ? that.data.activityId : '';
			let videoid = that.data.videoid != 0 ? that.data.videoid : '';
			if (that.data.purSts == 1) {
				wx.navigateTo({
					url: `/pages/productDetails/productDetails?id=${id}&type=${1}&activityId=${activityId}&video_id=${videoid}&typeTwo=${1}`,
				});
				console.log('-------11111111------------')
			} else {
				wx.navigateTo({
					url: `/pages/productDetails/productDetails?id=${id}&type=${0}&video_id=${videoid}`,
				});
				console.log('-------2222222222------------')
			}
		},
		attached: function () {
			let that = this;
			that.data.btnType;
			that.data.purSts;
			that.data.activityId;
			that.data.videoid;
			that.data.zeroPrice;
		},
	},
});
