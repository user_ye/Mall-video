// components/Auth/AuthPhoneLayer/AuthPhoneLayer.js

let app = getApp();
import {api} from "../../../utils/api";

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 确定
    btnOk(ev){
      console.log('ev-**-',ev);
      if (ev.detail.iv) {
        // 将微信获取的加密后的手机号码传给后端解密
        app.http({
          url: api.post_index_bindingPhone,
          data: {
            encryptedData: ev.detail.encryptedData,
            iv: ev.detail.iv,
          },
          // header: {
          //   Authorization: ''
          // },
        })
          .then((res) => {
            console.log('手机--22--res---',res)
            app.store.setState({
              isAuthPhone: false
            });
          })
          .catch((err) => {});
      }
    },
    // 取消
    btnNo(){
      app.store.setState({
        isAuthPhone: false
      });
      wx.showToast({
        title: '您取消了授权',
        icon: 'none',
        duration: 1500,
      });
      // wx.navigateBack({
      //   delta: 1
      // })
    }
  }
})
