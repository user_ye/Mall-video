// components/Auth/AuthUserInfoBtn/AuthUserInfoBtn.js
let app = getApp();
import {api} from "../../../utils/api";

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 确定
    btnOk(ev){
      console.log('ev-**-',ev);
      if (ev.detail.userInfo) {
        app.http({
          url: api.post_my_updateUserInfo,
          data: {
            encryptedData: ev.detail.encryptedData,
            iv: ev.detail.iv,
            rawData: ev.detail.rawData,
            signature: ev.detail.signature,
          },
        })
          .then((res) => {
            let requestData = res.data.data
            console.log('用户信息--22--res---',res.data.data)
            // wx.setStorageSync('loginStatus', res.data.data);
            app.store.setState({
              isAuthUserInfo: false,
              wxUserInfo: requestData
            });
          })
          .catch((err) => {});
      }
    },
    // 取消
    btnNo(){
      app.store.setState({
        isAuthUserInfo: false
      });
      wx.showToast({
        title: '您取消了授权',
        icon: 'none',
        duration: 1500,
      });
      // wx.navigateBack({
      //   delta: 1
      // })
    }
  }
})
