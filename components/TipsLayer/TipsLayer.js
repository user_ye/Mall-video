// components/TipsLayer/TipsLayer.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title:{ // 标题
      type: String,
      value: '提示'
    },
    isShowLayer:{ // 是否提示框
      type:Boolean,
      value:true
    },
    isHiddenNoBtn:{ // 是否隐藏取消按钮
      type: Boolean,
      value: true,
    },
    btnNoContent:{
      type: String,
      value: '取消'
    },
    btnOkContent:{
      type: String,
      value: '确定'
    },
    btnOkType:{
      type: String,
      value: 0, // 0常规按钮，1，微信用户信息，2手机号码
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    btnOk(ev){
      // console.log('btnOk---')
      this.triggerEvent('okZ',ev.detail);
    },
    btnNo(ev){
      // console.log('btnNo---')
      this.triggerEvent('onZ',ev.detail);
    }
  }
})
