Component({
  options: {
    addGlobalClass: true, // 使用全局样式app.wxss
  },
  /**
   * 组件的属性列表
   */
  properties: {
    position:{
      type: String,
      value: 'fixed',
    },
    background: {
      type: String,
      // value: 'rgba(255, 255, 255, 1)',
      value: '',
    },
    titleText: {
      type: String,
      value: '导航栏',
    },
    titleFontColor: {
      type: String,
      value: 'rgba(0, 0, 0, 1)',
    },
    titleFontSize: {
      type: Number,
      value: 32,
    },
    backType:{ // 返回类型，back,home
      type: String,
      value: 'back',
    },
    backIconType:{ // 返回图标类型
      type: String,
      value: '1',
    },
    backIconColor:{ // 返回图标颜色
      type: String,
      value: 'rgba(0, 0, 0, 1)',
    },
    backIconBackground:{ // 返回图标背景色
      type: String,
      value: '',
    },
  },
  attached: function () {
    var that = this;
    that.setNavSize();
    that.setStyle();
  },
  data: {},
  methods: {
    // 通过获取系统信息计算导航栏高度
    setNavSize: function () {
      var that = this,
        sysinfo = wx.getSystemInfoSync(),
        statusHeight = sysinfo.statusBarHeight,
        isiOS = sysinfo.system.indexOf('iOS') > -1,
        navHeight;
      if (!isiOS) {
        navHeight = 48;
      } else {
        navHeight = 44;
      }
      that.setData({
        status: statusHeight,
        navHeight: navHeight,
      });
    },
    setStyle: function () {
      var that = this,
        containerStyle,
        textStyle,
        iconStyle;
      containerStyle = ['background:' + that.data.background].join(';');
      textStyle = [
        'color:' + that.data.color,
        'font-size:' + that.data.fontSize + 'px',
      ].join(';');
      iconStyle = [
        'width: ' + that.data.iconWidth + 'px',
        'height: ' + that.data.iconHeight + 'px',
      ].join(';');
      that.setData({
        containerStyle: containerStyle,
        textStyle: textStyle,
        iconStyle: iconStyle,
      });
    },
    // 返回事件
    back: function () {
      // console.log('返回事件-*--')
      wx.navigateBack({
        delta: 1,
      });
      this.triggerEvent('back', { back: 1 });
    },
    home: function () {
      wx.switchTab({
        url: '/pages/tabBar/index/index',
      });
      this.triggerEvent('home', {});
    },
  },
});
