// components/steps/steps.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tipsText:{ // 提示的文字数组
      type: Array,
      value: []
    },
    tipsIndex:{ // 索引
      type: Number,
      value: 1
    },

    fontSize:{ // 字体大小
      type: String,
      value: '24rpx'
    },

    activeColor:{ // 激活状态颜色selectBg
      type: String,
      value: 'white'
    },
    inactiveColor:{ // 未激活状态颜色
      type: String,
      value: '#cccccc'
    },

    iconBoxWidthHeight:{ // icon盒子的宽高
      type: String,
      value: '31rpx'
    },

    isShowIconWrapBorder:{ // 是否显示icon外边框
      type: Boolean,
      value: true
    },
    iconWrapBorderWidthHeight:{ // icon外边框的宽高
      type: String,
      value: '8rpx'
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
