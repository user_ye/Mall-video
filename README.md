# miaomeimei

#### 介绍
商城 视频 带货 活动 拼团 邀请小程序项目

#### 软件架构
  + 商城 视频 带货 活动 拼团 邀请 等
  + 微信小程序
  + 需要先搭建后台 以及api
  +  [gitee 后台管理系统](https://gitee.com/user_ye/zeng_admin)
  +  [gitee 接口api](https://gitee.com/user_ye/zeng_api)
  

### 介绍
  + ![](./docs/1.gif)
  + [后台管理系统](https://madmin.zengye.top/admin/index/index.html)  https://madmin.zengye.top/admin  账号：`xiaozeng` 密码: `123456`

## 教程视频：[首页B站](https://space.bilibili.com/258238619) `https://space.bilibili.com/258238619`
  + [01-搭建项目视频教程](https://www.bilibili.com/video/BV18R4y1M7DJ)  `https://www.bilibili.com/video/BV18R4y1M7DJ`
  + [02-搭建小程序教程](https://www.bilibili.com/video/BV1V8411s7S2) `https://www.bilibili.com/video/BV1V8411s7S2`
  + [03-商品sku 批量商品评论等操作](https://www.bilibili.com/video/BV1bG41177FH) `https://www.bilibili.com/video/BV1bG41177FH`
  + [04-视频操作 批量视频评论等操作](https://www.bilibili.com/video/BV1QW4y1J7t7)  `https://www.bilibili.com/video/BV1QW4y1J7t7`
  + [05-此项目已不再维护 期待我的下一个项目吧 开发神器 前后端 产品 测试 都可使用]

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
