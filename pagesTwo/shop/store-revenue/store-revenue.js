// pagesTwo/shop/store-revenue/store-revenue.js
let app = getApp();

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		selectDate: '2018-09',
		shopRevenueData: {}, // 请求到的店铺营收数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.getToDay(); // 获取年月
		// 禁用分享
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline']
		})
	},

	// 店铺营收数据
	getShopIndexGetRevenue(){
		let that = this;
		app.http({
			url: app.api.shopIndexGetRevenue,
			data: {
				time: that.data.selectDate, // 顶部选择年月
			},
			//method: 'get',
		})
			.then((res) => {
				let requestData = res.data.data;
				// console.log("requestData---", requestData);
				that.setData({
					shopRevenueData: requestData
				})
			})
			.catch((err) => {

			});
	},

	// 获取年月
	getToDay() {
		let data = new Date();
		let year = data.getFullYear();
		let month =
			data.getMonth() + 1 < 10 ? '0' + (data.getMonth() + 1) : data.getMonth() + 1;

		let realDate = year + '-' + month;
		// console.log('realDate---',realDate);
		this.setData({
			selectDate: realDate,
		});
		this.getShopIndexGetRevenue(); // 店铺营收数据
	},

	// 顶部时间选择器
	bindDateChange(e) {
		let selectData = e.detail.value;
		// console.log('selectData---', selectData);
		this.setData({
			selectDate: selectData,
		});
		this.getShopIndexGetRevenue(); 	// 店铺营收数据
	},
	// 提现
	withdraw() {
		wx.showToast({
			title: '暂未开放',
			icon: 'none',
			duration: 1500
		})
		return ;

		wx.navigateTo({
			url: '/pages/withdraw/withdraw',
		});
	},
	// 提现记录
	withdrawal() {
		wx.showToast({
			title: '暂未开放',
			icon: 'none',
			duration: 1500
		})
		return ;

		wx.navigateTo({
			url: '/pages/withdrawalsRec/withdrawalsRec',
		});
	},
});
