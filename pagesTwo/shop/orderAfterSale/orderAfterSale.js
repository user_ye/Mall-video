// pagesTwo/shop/orderAfterSale/orderAfterSale.js
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 步骤条提示文字 --  初始化文字
    // stepsTipsText: [
    //   '提交申请',
    //   '商家已审核',
    //   '待买家发货',
    //   '待退款',
    //   '完成',
    // ],
    // stepsTipsText: [
    //   '待审核',
    //   '审核通过',
    //   '待买家发货',
    //   '商家待收货',
    //   '商家处理中',
    //   '完结',
    //   '拒绝'
    // ],
    stepsTipsIndex: 0, // 步骤条索引

    requestExpressArray: [], // 请求的快递公司
    ExpressArray: [], // 快递公司普通选择器，分割好的数组
    ExpressIndex: 0, // 快递公司普通选择器索引

    courierNumberValue: '', // 输入的快递单号
    orderId: null, // 订单id
    orderItem: {}, // 上个页面传过来的订单数据
    requestAfterSaleData: {}, // 请求到的售后数据

    businessMMessage: '',
    isBusinessMMessage: true, // 是否锁定商家留言
    isShowRefuseLayer: true, // 是否显示拒绝原因弹层
    selectRefuseIndex: 0, // 拒绝原因 radio 索引
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.orderItem) {
      let orderItem = JSON.parse(options.orderItem);
      this.setData({
        orderItem: orderItem,
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.logistics();
  },

  // 编辑商家留言
  editBusinessMMessage(){
    this.setData({
      isBusinessMMessage: !this.data.isBusinessMMessage
    })
  },


  // 售后详情
  afterSaleDetails() {
    let that = this;
    that.data.ExpressArray = [];
    app.http({
      url: app.api.shopServiceGetInfo,
      data: {
        id: that.data.orderItem.id || 63,
      },
      //method: 'get',
    })
      .then((res) => {
        that.data.requestExpressArray.map((mapRes) => {
          if (res.data.data.oneChangeExpress) {
            if (mapRes.id == res.data.data.oneChangeExpress.id) {
              res.data.data.oneChangeExpressName = mapRes.company;
            }
          }
          if (res.data.data.oneMerchantExpress) {
            if (mapRes.id == res.data.data.oneMerchantExpress.id) {
              res.data.data.oneMerchantExpressName = mapRes.company;
            }
          }
        });

        let stepsTipsText = [];
        let stepsTipsIndex = '';

        // 情况 四种 ：   1.退款退货-正常流程    2. 换货-正常流程    3    提交被拒绝     4  商家 没有收到货  则显示 待商家处理

        var linshi = [];
        var index = 1;
        // 如果状态等于  6  已拒绝
        if (res.data.data.status == 6) {
          linshi = ['提交申请', '商家已拒绝'];
          index = 1;
          // 类型 为  2   表示  退款退货   ||
        } else if (res.data.data.type == 2) {
          linshi = [
            '提交申请',
            '待审核',
            res.data.data.status > 3 ? '已发货' : '发货中',
            '处理中',
            '待退款',
            '完成',
          ];
          index = res.data.data.status;
          // 类型 为2 类型1  表示 换货
        } else if (res.data.data.type == 1) {
          linshi = [
            '提交申请',
            '待审核',
            '发货中',
            '处理中',
            '已发货',
            '完成',
          ];
          index = res.data.data.status;
        }
        that.setData({
          stepsTipsText: linshi,
          stepsTipsIndex: index,
        });

        // 装换时间
        res.data.data.add_time = app.util.formatTimeYMDhms(res.data.data.add_time);

        that.setData({
          requestAfterSaleData: res.data.data,
        });
      })
      .catch((err) => {});
  },

  // 获取物流
  logistics() {
    let that = this;
    that.data.ExpressArray = [];
    app.http({
      url: app.api.service_index_logisticsList,
      data: {},
      //method: 'get',
    })
      .then((res) => {
        that.afterSaleDetails();
        that.setData({
          requestExpressArray: res.data.data,
        });
        res.data.data.map((mapRes) => {
          that.data.ExpressArray.push(mapRes.company);
          that.setData({
            ExpressArray: that.data.ExpressArray,
          });
        });
      })
      .catch((err) => {});
  },

  // 复制地址
  copyBtnAddress() {
    let name = this.data.requestAfterSaleData.address.name;
    let tel = this.data.requestAfterSaleData.address.tel;
    let address = this.data.requestAfterSaleData.address.address;
    let data = '收货人：' + name + ' ' + tel + ' ' + address;

    wx.setClipboardData({
      data: data,
      success(res) {
        wx.getClipboardData({
          success(res) {},
        });
      },
    });
  },

  // 复制订单号
  copyBtnSn() {
    let data = this.data.requestAfterSaleData.order_orn

    wx.setClipboardData({
      data: data,
      success(res) {
        wx.getClipboardData({
          success(res) {},
        });
      },
    });
  },

  // 复制电话
  copyBtnPhone() {
    let data = this.data.requestAfterSaleData.address.tel

    wx.setClipboardData({
      data: data,
      success(res) {
        wx.getClipboardData({
          success(res) {},
        });
      },
    });
  },

  // 监听input输入，输入的快递单号
  bindInputCourier(e) {
    let that = this;
    that.setData({
      courierNumberValue: e.detail.value,
    });
  },

  // 确认发货
  expressSubmitBtn() {
    let that = this;
    if (this.data.courierNumberValue == '') {
      wx.showToast({
        title: '请输入快递单号',
        icon: 'none',
        duration: 1500,
      });
    } else {
      let logisticsId = this.data.requestExpressArray[
        this.data.ExpressIndex
        ].id; // 物流id
      // console.log('物流id',logisticsId);
      // console.log('输入的快递单号',this.data.courierNumberValue);
      // console.log('售后id',this.data.requestAfterSaleData.id);

      app.http({
        url: app.api.service_index_updateExchange,
        data: {
          id: that.data.requestAfterSaleData.id, // 售后id
          company_id: logisticsId, // 物流公司-id
          express_sn: that.data.courierNumberValue, // 快递单号
        },
        //method: 'get',
      })
        .then((res) => {
          wx.showModal({
            title: '提示',
            content: '发货成功',
            showCancel: false, // 是否显示取消按钮，默认为 true
            success(res) {
              if (res.confirm) {
                // console.log('用户点击确定')
                wx.navigateBack({
                  delta: 1,
                });
                // 重新加载
                that.afterSaleDetails();
              } else if (res.cancel) {
              }
            },
          });
        })
        .catch((err) => {});
    }
  },

  // 快递公司普通选择器
  bindPickerChangeExpress: function (e) {
    this.setData({
      ExpressIndex: e.detail.value,
    });
  },

  // 打开或关闭拒绝原因弹层
  refuseLayerClose(ev){
    let type = ev.currentTarget.dataset.type;
    if (this.data.selectRefuseIndex == 0){
      this.setData({
        businessMMessage: '非质量问题。',
      })
    }
    if (type == 'submit'){
      if (!this.data.businessMMessage){
        wx.showToast({
          title: '请输入拒绝原因',
          icon: 'none',
          duration: 1500
        })
        return;
      }
      console.log("提交**---",this.data.businessMMessage);
    }

    this.setData({
      isShowRefuseLayer: !this.data.isShowRefuseLayer
    })
  },

  // 输入失去焦点时 拒绝原因
  bindBlurRefuse(ev){
    console.log("ev---", ev.detail.value);
    this.setData({
      businessMMessage: ev.detail.value,
    })
  },

  // 拒绝原因 radio
  selectRefuse(ev){
    let type = ev.currentTarget.dataset.type;
    this.setData({
      businessMMessage: '',
      selectRefuseIndex: type,
    })
  }
});
