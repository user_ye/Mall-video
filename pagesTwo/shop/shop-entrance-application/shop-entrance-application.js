// pagesTwo/shop/shop-entrance-application/shop-entrance-application.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    optionsCode: '', // 邀请码
    shopApplicationStatus: {}, // 店铺状态数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("options---", options);
    if (options.code){
      this.setData({
        optionsCode: options.code
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //判断onLaunch是否执行完毕
    if (app.globalData.checkLogin) {
      this.getShopIndexCheckShop();
    } else {
      app.checkLoginReadyCallback = (res) => {
        this.getShopIndexCheckShop();
      };
    }

  },

  // 获取申请店铺状态
  getShopIndexCheckShop(e){
    let that = this;
    app.http({
      url: app.api.shopIndexCheckShop,
      data: {},
    })
      .then((res) => {
        //  -1申请失败，0没申请，1申请中，2已通过
        console.log('获取申请店铺状态res---',res.data.data);
        that.setData({
          shopApplicationStatus: res.data.data
        })
      })
      .catch((err) => {

      });
  },


  showModal(){
    wx.showModal({
      title: '提示',
      content: '这是一个模态弹窗',
      showCancel: true, // 是否显示取消按钮，默认为 true
      success (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 跳转店铺
  toShop(ev){
    let shopType = ev.currentTarget.dataset.shop_type;
    console.log("shopType---", shopType);
    // shopType: online || 1 // 线上
    // shopType: offline || 2 // 线下
    // apply_status: -1申请失败，0没申请，1申请中，2已通过
    let shopApplicationStatus = this.data.shopApplicationStatus; // 申请店铺状态数据
    let onlineApplyStatus = shopApplicationStatus.online ? shopApplicationStatus.online.apply_status : 0;
    let offlineApplyStatus = shopApplicationStatus.offline ? shopApplicationStatus.offline.apply_status : 0;
    let shopData = {
      code: this.data.optionsCode, // 邀请码
      shopType: shopType,
      onlineApplyStatus: onlineApplyStatus,
      offlineApplyStatus: offlineApplyStatus,
    }
    if (shopType == 1){
      if (onlineApplyStatus == -1){
        let tips = shopApplicationStatus.online.describe ? shopApplicationStatus.online.describe : '';
        app.util.showModal(tips,true,'申请失败','取消','重新申请').then(()=>{
          // wx.navigateBack(1);
          wx.navigateTo({
            url: '/pagesTwo/shop/shop-apply/shop-apply?shopData='+ JSON.stringify(shopData)
          });
        })
      } else if (onlineApplyStatus == 0){
        wx.navigateTo({
          url: '/pagesTwo/shop/shop-apply/shop-apply?shopData='+ JSON.stringify(shopData)
        });
      } else if (onlineApplyStatus == 1){
        // let tips = shopApplicationStatus.online.describe ? shopApplicationStatus.online.describe : '平台将在 3 个工作日内完成资料审核';
        let tips ='平台将在 3 个工作日内完成资料审核';
        app.util.showModal(tips,false,'申请中').then(()=>{
          // wx.navigateBack(1);
        })
      } else if (onlineApplyStatus == 2){
        wx.navigateTo({
          url: '/pagesTwo/shop/my-shop/my-shop',
        });
      }
    } else if (shopType == 2){
      if (offlineApplyStatus == -1){
        let tips = shopApplicationStatus.offline.describe ? shopApplicationStatus.offline.describe : '';
        app.util.showModal(tips,true,'申请失败','取消','重新申请').then(()=>{
          // wx.navigateBack(1);
          wx.navigateTo({
            url: '/pagesTwo/shop/shop-apply/shop-apply?shopData='+ JSON.stringify(shopData)
          });
        })
      } else if (offlineApplyStatus == 0){
        wx.navigateTo({
          url: '/pagesTwo/shop/shop-apply/shop-apply?shopData='+ JSON.stringify(shopData)
        });
      } else if (offlineApplyStatus == 1){
        // let tips = shopApplicationStatus.offline.describe ? shopApplicationStatus.offline.describe : '平台将在 3 个工作日内完成资料审核';
        let tips = '平台将在 3 个工作日内完成资料审核'
        app.util.showModal(tips,false,'申请中').then(()=>{
          // wx.navigateBack(1);
        })
      } else if (offlineApplyStatus == 2){ // 线下店铺申请成功跳转
        let tips = '店铺暂未开发，敬请期待！'
        app.util.showModal(tips,false,'审核成功').then(()=>{
          // wx.navigateBack(1);
        })
        // wx.navigateTo({
        //   url: '/pagesTwo/shop/my-shop/my-shop',
        // });
      }

    }
  },



  // 获取手机授权弹层
  getPhone() {
    app.store.setState({
      getPhoneNumber: true,
    });
    console.log("获取手机授权弹层---");
    // that.onLoad(); // 刷新
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {
  //
  // }
})
