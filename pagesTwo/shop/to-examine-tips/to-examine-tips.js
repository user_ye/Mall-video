// pagesTwo/shop/to-examine-tips/to-examine-tips.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopData: '',	// -1申请失败，1申请中
    shopType: '', // 店铺状态：online 线上，offline 线下
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("options---", options);
    if (options.shopData){
      this.setData({
        shopData: JSON.parse(options.shopData),
        shopType: options.shopType,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 禁用分享
    wx.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  // 重新申请店铺
  shopApply(){
    if (this.data.shopType == 'online'){
      wx.redirectTo({
        url: '/pagesTwo/shop/shop-apply/shop-apply?isShowShopData=' + true,
      });
    } else {

    }
  },

  // 返回
  back(){
    wx.navigateBack({
      delta: 1
    })
  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
