// pagesTwo/shop/shop-apply/shop-apply.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token: '',
    infoData: {}, // 本地缓存的用户信息

    form:{
      name_user:'', // 用户名
      shop_name:'', // 店铺名
      phone:'', // 手机号
      iDNumber:'', // 身份证号
      shop_details:'', // 店铺介绍
      isAgreeAgreement: true, // 是否同意协议

      defaultShopLogoImg:'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/upload/camera.png', // 默认店铺logo
      tempShopLogoImg:'', // 临时生成的店铺logo
      shopLogoImg:'', // 线上店铺logo

      defaultIdPositiveImg:'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/upload/id-positive.png', // 默认的身份证正面
      tempIdPositiveImg:'', // 临时生成的身份证正面
      idPositiveImg:'', // 线上身份证正面

      defaultIdTheOtherSideImg:'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/upload/id-the-other-side.png', // 默认的身份证反面
      tempIdTheOtherSideImg:'', // 临时生成的身份证反面
      idTheOtherSideImg:'', // 线上身份证反面

      defaultBusinessLicenseImgOne:'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/upload/business-license.png', // 默认的营业执照1
      tempBusinessLicenseImgOne:'', // 临时生成的营业执照1
      businessLicenseImgOne:'', // 线上营业执照1

      defaultBusinessLicenseImgTwo:'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/upload/business-license.png', // 默认的营业执照2
      tempBusinessLicenseImgTwo:'', // 临时生成的营业执照2
      businessLicenseImgTwo:'', // 线上营业执照2

      defaultBusinessLicenseImgThree:'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/upload/business-license.png', //  默认的营业执照3
      tempBusinessLicenseImgThree:'', //  临时生成的营业执照3
      businessLicenseImgThree:'', // 线上营业执照3
    },

    showShopData:'', // 回显的店铺数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.isShowShopData){
      console.log('options*---',options);
      this.getShowShopData();
    } else {
      let infoData = wx.getStorageSync('infoData');
      console.log("infoData---", infoData.phone);
      this.setData({
        infoData: infoData,
        ['form.phone']: infoData.phone,
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let token = wx.getStorageSync('token');
    this.setData({
      token: token,
    });

    // 禁用分享
    wx.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  // 显示申请失败的店铺数据
  getShowShopData(){
    let that = this;
    app.http({
      url: app.api.shopIndexGetReapplyInfo,
      data: {},
      //method: 'get',
    })
      .then((res) => {
        let requestData = res.data.data;
        console.log("requestData---", requestData);
        let card_img = JSON.parse(requestData.card_img)

        console.log('card_img[0]---',card_img[0]);

        that.setData({
          showShopData: requestData,
          ['form.name_user']: requestData.real_name, // 用户名
          ['form.shop_name']:requestData.name, // 店铺名
          ['form.phone']: requestData.phone, // 手机号
          ['form.iDNumber']: requestData.identity, // 身份证号
          ['form.shop_details']: requestData.desc, // 店铺介绍

          ['form.tempShopLogoImg']: requestData.logo, // 店铺logo图片地址
          ['form.shopLogoImg']: requestData.logo, // 店铺logo图片地址

          ['form.tempIdPositiveImg']: card_img[0], // 身份证正面图片地址
          ['form.idPositiveImg']: card_img[0], // 身份证正面图片地址

          ['form.tempIdTheOtherSideImg']: card_img[1], // 身份证反面图片地址
          ['form.idTheOtherSideImg']: card_img[1], // 身份证反面图片地址

          ['form.tempBusinessLicenseImgOne']: requestData.certificate_photo, // 证件照：三证合一
          ['form.businessLicenseImgOne']: requestData.certificate_photo, // 证件照：三证合一
        })


      })
      .catch((err) => {

      });
  },

  // 提交审核
  submitBtn(){
    if (!this.data.form.name_user) {
      wx.showToast({
        title: '请输入您的姓名',
        icon: 'none',
        duration: 1500
      })
      return;
    } else  if (!this.data.form.shop_name) {
      wx.showToast({
        title: '请输入店铺名称',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.phone) {
      wx.showToast({
        title: '请输入您的手机号码',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.iDNumber) {
      wx.showToast({
        title: '请输入您的身份证号',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.shop_details) {
      wx.showToast({
        title: '请输入店铺介绍',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.shopLogoImg) {
      wx.showToast({
        title: '请上传店铺logo',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.idPositiveImg) {
      wx.showToast({
        title: '请上传身份证正面',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.idTheOtherSideImg) {
      wx.showToast({
        title: '请上传身份证反面',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.businessLicenseImgOne) {
      wx.showToast({
        title: '请上传营业执照',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.form.isAgreeAgreement){
      wx.showToast({
        title: '请同意用户协议',
        icon: 'none',
        duration: 1500
      })
      return;
    }


    console.log("this.data.form---", this.data.form);

    let paramData = {
      id: this.data.showShopData ? this.data.showShopData.id : '',

      name: this.data.form.shop_name, // 店铺名
      real_name: this.data.form.name_user, // 商家真实姓名
      phone: this.data.form.phone, // 手机号
      identity: this.data.form.iDNumber, // 身份证
      desc: this.data.form.shop_details, // 店铺描述
      logo: this.data.form.shopLogoImg, // 店铺logo图片地址
      card_img_front: this.data.form.idPositiveImg, // 身份证正面图片地址
      card_img_back: this.data.form.idTheOtherSideImg, // 身份证反面图片地址
      certificate_photo: this.data.form.businessLicenseImgOne, // 证件照：三证合一
    };

    app.http({
      url: app.api.shopIndexCreateShop,
      data: paramData,
      //method: 'get',
    })
      .then((res) => {
        console.log("res---", res.data.data);
        if (res.data.data == true){
          app.util.showModal('申请成功').then(()=>{
            wx.navigateBack(1);
          })
        }
      })
      .catch((err) => {});

    console.log('提交审核---',this.data.form);
  },

  // 删除图片
  delImg(ev){
    let that = this;
    let type = ev.currentTarget.dataset.type;
    console.log("删除图片---",type);
    that.setImgData({
      type: type, // 上传的图片类型
      isEmpty: true, // 是否清空图片
    });
  },

  // 上传图片
  chooseImage(ev){
    let that = this;
    let type = ev.currentTarget.dataset.type;
    console.log("type---", type);

    // 微信选择图片api
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths
        that.afterReadShopLogo({
          type: type,
          tempFilePaths: tempFilePaths,
        });
        console.log('tempFilePaths---',tempFilePaths);
      }
    })
  },

  // 上传店铺logo
  afterReadShopLogo(param) {
    let that = this;

    wx.showLoading({
      title: '上传中...',
    });
    let tempFilePaths = param.tempFilePaths[0]; // 临时图片
    let qm = app.qm({ type: 'image' });
    wx.uploadFile({
      url: app.api_url + app.api.api_index_uploads,
      filePath: tempFilePaths,
      name: 'img',
      header: {
        Authorization: that.data.token,
        'content-type': 'application/x-www-form-urlencoded',
      },
      formData: qm,
      success: (res) => {
        console.log('res---',res);
        if (res.data) {
          let requestData = JSON.parse(res.data);
          // that.data.uploadFileListShopLogo.push(requestData.data.result);
          that.setImgData({
            type: param.type, // 上传的图片类型
            result: requestData.data.result, // 后端返回的图片地址
            tempFilePaths: tempFilePaths, // 本地上传的临时图片地址
          });
        } else {
          wx.showToast({
            title: '服务器报错!',
            icon: 'none',
            duration: 1500,
          });
        }
        wx.hideLoading();
      },
      fail: (err) => {},
    });
  },

  // 设置图片数据
  setImgData(param){
    let that = this;
    if (param.type == "shopLogo"){ // 上传店铺logo
      that.setData({
        ['form.tempShopLogoImg']: param.isEmpty ? '' : param.tempFilePaths,
        ['form.shopLogoImg']: param.isEmpty ? '' : param.result,
      })
    } else if (param.type == "idPositive"){ // 身份证正面
      console.log("身份证正面---");
      that.setData({
        ['form.tempIdPositiveImg']: param.isEmpty ? '' : param.tempFilePaths,
        ['form.idPositiveImg']: param.isEmpty ? '' : param.result,
      })
    } else if (param.type == "idTheOtherSide"){ // 身份证反面
      console.log("身份证反面---");
      that.setData({
        ['form.tempIdTheOtherSideImg']: param.isEmpty ? '' : param.tempFilePaths,
        ['form.idTheOtherSideImg']: param.isEmpty ? '' : param.result,
      })
    } else if (param.type == "businessLicenseImgOne"){ // 营业执照1
      console.log("营业执照1---");
      that.setData({
        ['form.tempBusinessLicenseImgOne']: param.isEmpty ? '' : param.tempFilePaths,
        ['form.businessLicenseImgOne']: param.isEmpty ? '' : param.result,
      })
    } else if (param.type == "businessLicenseImgTwo"){ // 营业执照2
      console.log("营业执照2---");
      that.setData({
        ['form.tempBusinessLicenseImgTwo']: param.isEmpty ? '' : param.tempFilePaths,
        ['form.businessLicenseImgTwo']: param.isEmpty ? '' : param.result,
      })
    } else if (param.type == "businessLicenseImgThree"){ // 营业执照3
      console.log("营业执照3---");
      that.setData({
        ['form.tempBusinessLicenseImgThree']: param.isEmpty ? '' : param.tempFilePaths,
        ['form.businessLicenseImgThree']: param.isEmpty ? '' : param.result,
      })
    }
  },

  // 同意协议
  agreeAgreement(){
    this.setData({
      ['form.isAgreeAgreement']: !this.data.form.isAgreeAgreement
    })
  },

  // 输入失去焦点时
  inputBindBlur(e){
    let type = e.currentTarget.dataset.type;
    let value = e.detail.value;
    this.setData({
      ['form.' + type]: value
    })
  },

  // 用户协议
  userAgreement(){
    wx.navigateTo({
      url: '/pages/descrRulePage/descrRulePage?key=shop_agreement'
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
