// pagesTwo/shop/afterSaleList/afterSaleList.js
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    paginationList:[], // 分页列表数据
    pageNum: 1, // 第几页
    limit: 10, // 每页数量
    isMax:false, // 是否已显示最大数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    if (options.tabIndex) {
      that.setData({
        tabIndex: parseInt(options.tabIndex),
      });
    }
  },

  onShow() {
    this.requestList();
    // 禁用分享
    wx.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  // 跳转订单详情
  clickItem(ev){
    let item = ev.currentTarget.dataset.item;
    console.log("item---", item);
    wx.navigateTo({
      url: '/pagesTwo/shop/orderAfterSale/orderAfterSale?orderItem=' + JSON.stringify(item)
    })
  },

  // 获取售后列表
  requestList() {
    let that = this;
    let paginationList = this.data.paginationList; // 分页列表数据
    let pageNum = this.data.pageNum; // 第几页
    let limit = this.data.limit;  // 每页数量
    let isMax = this.data.isMax; // 是否已显示最大数量
    if (!isMax) {
      app.http({
        url: app.api.shopServiceList,
        data: {
          page: pageNum, // 第几页
          limit: limit, // 每页数量
        },
      })
        .then((res) => {
          console.log('请求列表 res.data.data--------',res.data.data);

          let listData = res.data.data.data; // 请求到的列表数据

          console.log("listData---", listData);

          paginationList.push(...listData);

          let total = res.data.data.total; // 列表的总数量
          if (paginationList.length >= total) {
            isMax = true;
            console.log('---------------最大啦---------------')
          }

          pageNum += 1;
          that.setData({
            paginationList: paginationList,
            isMax: isMax,
            pageNum: pageNum
          })

        })
        .catch((err) => {
          console.log(err);
        });
    }
  },

  // 重置分页
  resetPaging(){
    this.setData({
      paginationList:[], // 分页列表数据
      pageNum: 1, // 第几页
      limit: 10, // 每页数量
      isMax:false, // 是否已显示最大数量
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.requestList();
  },
});
