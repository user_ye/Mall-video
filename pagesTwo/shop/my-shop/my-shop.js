// pagesTwo/shop/my-shop/my-shop.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		myShopData:{}, // 我的店铺数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.getMyShop()	// 获取我的店铺数据

		// 禁用分享
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline']
		})
	},

	// 获取我的店铺数据
	getMyShop(){
		let that = this;
		app.http({
			url: app.api.shopIndexInfo,
			data: {},
		})
			.then((res) => {
				let requestData = res.data.data;
				that.setData({
					myShopData:requestData
				})
				console.log('领取优惠卷---requestData---',requestData);
			})
			.catch((err) => {});
	},

	// 跳转订单详情
	jumpOrderManage(ev){
		console.log("跳转订单详情");
		let jumpType = ev.currentTarget.dataset.jump_type;
		console.log('jumpType*---')
		if (jumpType == 'toBeDelivered'){  // 待发货
			wx.navigateTo({
				url: '/pagesTwo/shop/orderManage/orderManage?tabIndex=3',
			});
		} else if (jumpType == 'toBeSold'){  // 待售后
			wx.showToast({
				title: '暂未开放',
				icon: 'none',
				duration: 1500
			})
			return ;
			wx.navigateTo({
				url: '/pagesTwo/shop/afterSaleList/afterSaleList',
			});
		}
	},


	// 点击店铺管理里的选项
	shopManageClick(e) {
		let type = e.currentTarget.dataset.type;

		if (type == 'spgl') {
			// 跳转商品管理
			wx.navigateTo({
				url: '/pages/commodityManagement/commodityManagement',
			});
		} else if (type == 'dpzb') {
			// 跳转店铺直播
			wx.navigateTo({
				url: '/pages/creALiveBroadcast/creALiveBroadcast?dpzb=1',
			});
		} else if (type == 'ddgl') {
			// 跳转订单管理
			wx.navigateTo({
				url: '/pagesTwo/shop/orderManage/orderManage',
			});
		} else if (type == 'dpys') {
			// 跳转店铺营收
			wx.navigateTo({
				url: '/pagesTwo/shop/store-revenue/store-revenue',
			});
		} else if (type == 'yhjsz') {
			// 跳转优惠卷管理
			wx.navigateTo({
				url: '/pages/coupon/coupon-manage/coupon-manage',
			});
		}
		// else {
		// 	// 暂未开放
		// 	wx.showToast({
		// 		title: '敬请期待',
		// 		icon: 'none',
		// 		duration: 1500
		// 	})
		// }
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {},
});
