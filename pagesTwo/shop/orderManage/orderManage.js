// pagesTwo/shop/orderManage/orderManage.js

let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabData: [ //tab标签名字
      { id: 0, text: '全部' },
      { id: -1, text: '已取消' },
      { id: 1, text: '待付款' },
      { id: 2, text: '待发货' },
      { id: 3, text: '待收货' },
      { id: 4, text: '已收货' },
      { id: 5, text: '已完成' },
    ],
    tabIndex: 0, //tab选中标签

    paginationList:[], // 分页列表数据
    pageNum: 1, // 第几页
    limit: 10, // 每页数量
    isMax:false, // 是否已显示最大数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    if (options.tabIndex) {
      that.setData({
        tabIndex: parseInt(options.tabIndex),
      });
    }
  },

  onShow() {
    this.requestList();
    // 禁用分享
    wx.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  // 跳转订单详情
  clickItem(ev){
    let item = ev.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pagesTwo/shop/orderDetail/orderDetail?orderItem=' + JSON.stringify(item)
    })
  },

  // tab标签切换
  onChange(event) {
    let that = this;
    that.setData({
      tabIndex: event.detail.index,
    });
    this.resetPaging(); // 重置分页
    this.requestList(); // 获取订单列表
  },

  // 获取订单列表
  requestList() {
    let that = this;
    let paginationList = this.data.paginationList; // 分页列表数据
    let pageNum = this.data.pageNum; // 第几页
    let limit = this.data.limit;  // 每页数量
    let isMax = this.data.isMax; // 是否已显示最大数量
    if (!isMax) {
      app.http({
        url: app.api.shopOrderMyOrderList,
        data: {
          status: that.data.tabData[that.data.tabIndex].id,
          page: pageNum, // 第几页
          limit: limit, // 每页数量
        },
      })
        .then((res) => {
          console.log('请求列表 res.data.data--------',res.data.data);

          let listData = res.data.data.data; // 请求到的列表数据

          paginationList.push(...listData);

          let total = res.data.data.total; // 列表的总数量
          if (paginationList.length >= total) {
            isMax = true;
            console.log('---------------最大啦---------------')
          }

          pageNum += 1;
          that.setData({
            paginationList: paginationList,
            isMax: isMax,
            pageNum: pageNum
          })

        })
        .catch((err) => {
          console.log(err);
        });
    }
  },

  // 重置分页
  resetPaging(){
    this.setData({
      paginationList:[], // 分页列表数据
      pageNum: 1, // 第几页
      limit: 10, // 每页数量
      isMax:false, // 是否已显示最大数量
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.requestList();
  },
});
