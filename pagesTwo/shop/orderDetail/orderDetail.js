// pagesTwo/shop/orderDetail/orderDetail.js
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    orderDetailData: {}, // 请求到的订单详情数据
    orderDetailId: '', // 订单详情id

    isRefresh: false, // 是否需要刷新
    lastPageOrderItem: '', // 上个页面单个订单数据

    expressArray: [], // 快递单项选择器数据
    expressAllArray: [], // 快递单项选择器所有数据
    expressIndex: 0, // 快递单项选择器索引
    expressValue: '', // 填写的快递单号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    if (options.orderItem){
      this.setData({
        lastPageOrderItem: JSON.parse(options.orderItem)
      })
    }
    // 176 待发货
    // 181 待付款
    // let orderDetailId = options.id;
    // this.setData({
    //   orderDetailId: orderDetailId,
    // });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // if (this.data.isRefresh) {
    //   this.getShopOOrderGetOrderInfo(this.data.orderDetailId);
    // }
    this.getShopOrderGetOrderInfo();
    console.log("11111-----");
  },

  // 复制地址
  copyBtnAddress() {
    let name = this.data.orderDetailData.address.name;
    let tel = this.data.orderDetailData.address.phone;
    let address = this.data.orderDetailData.address.addr;
    let data = '收货人：' + name + ' ' + tel + ' ' + address;

    wx.setClipboardData({
      data: data,
      success(res) {
        wx.getClipboardData({
          success(res) {},
        });
      },
    });
  },

  // 发货
  deliverGoodsBtn(){
    let that = this;
    let expressIndex = this.data.expressIndex; // 快递单项选择器索引
    let expressAllArray = this.data.expressAllArray;
    let expressValue = this.data.expressValue;

    let orderDetailData = this.data.orderDetailData; // 订单数据
    console.log("索引---expressIndex---", expressIndex);
    console.log("全部快递数据---expressAllArray---", expressAllArray);
    if (expressIndex == 0) {
      wx.showToast({
        title: '请选择物流公司',
        icon: 'none',
        duration: 1500
      })
      return ;
    }


    console.log("快递单号---expressValue---", expressValue);
    console.log("选择的快递数据---", expressAllArray[expressIndex]);
    console.log("订单数据---", that.data.orderDetailData);

    app.http({
      url: app.api.shopOrderDeliverGoods,
      data: {
        id: orderDetailData.id, // 订单id
        company_id: expressAllArray[expressIndex].id, // 物流公司id
        express_sn: expressValue, // 物流单号
      },
      //method: 'get',
    })
      .then((res) => {
        let requestData = res.data.data;
        console.log("requestData---", requestData);
        if (requestData == true){
          orderDetailData.status = 3;
          this.setData({
            orderDetailData
          })
          wx.showToast({
            title: '发货成功',
            icon: 'success',
            duration: 1500
          })
        }
      })
      .catch((err) => {

      });

  },

  // 快递单号单项选择器
  bindPickerChange: function(e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      expressIndex: e.detail.value
    })
  },

  // 快递单号失去焦点
  bindBlurExpress(ev){
    let value = ev.detail.value
    console.log("value---", value);
    this.setData({
      expressValue: value
    })
  },
  // 订单详情
  getShopOrderGetOrderInfo() {
    let that = this;
    app.http({
      url: app.api.shopOrderGetOrderInfo,
      data: {
        id: that.data.lastPageOrderItem ? that.data.lastPageOrderItem.id : 1447,
        // id: that.data.lastPageOrderItem.id,
      },
      //method: 'get',
    })
      .then((res) => {
        let requestData = res.data.data;
        console.log("requestData---", requestData);
        console.log("requestData.logistics---", requestData.logistics);
        requestData.logistics.unshift({
          id: 999999,
          company: "请选择物流公司"
        });
        that.setData({
          orderDetailData: requestData,
          expressAllArray: requestData.logistics,
        });

        let expressArray = [];
        requestData.logistics.map(mapRes =>{
          expressArray.push(mapRes.company);
        })
        that.setData({
          expressArray
        })

      })
      .catch((err) => {});
  },

  // 复制订单号
  copyOrderNumber() {
    let that = this;
    wx.setClipboardData({
      data: that.data.orderDetailData.order_sn,
      success(res) {
        wx.getClipboardData({
          success(res) {},
        });
      },
    });
  },
});
