// pages/commodityManagement/commodityManagement.js

const app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		topInputValue: '', //搜索栏
		active: 0, //切换
		tabTipsText: ['上架中', '未上架'], //分类
		tabsIndex: 0, // tabs索引
		// isShowLayer: false, // 弹窗
		typeArray:[], // 分类
		selectTypeItem: '', // 选择的分类
		selectTypeIndex: -1, // 选择的分类索引

		goodItemIndex: 0, // 商品item索引
		goodItem: '', // 选择的商品item数据

		paginationList:[], // 分页列表数据
		pageNum: 1, // 第几页
		limit: 10, // 每页数量
		isMax:false, // 是否已显示最大数量
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.getTypeList();
		console.log(that.data.list);
	},

	onShow(){
		this.getGoodsList({
			type: '', // 商品分类，传分类id
			name: this.data.topInputValue, // 搜索名称
			status: 1, // 是否上架 1 上架 2 下架
		});
	},

	// 获取分类
	getTypeList() {
		let that = this;
		app.http({
			// url: app.api.getTypeList,
			url: app.api.get_mall_mallType,
			data: {},
		})
			.then((res) => {
				let requestData = res.data.data;
				console.log('获取分类***',requestData);
				that.setData({
					typeArray:requestData
				})
			})
			.catch((err) => {});
	},

	// 点击分类按钮
	clickTypeBtn(e) {
		let item = e.currentTarget.dataset.item;
		let index = e.currentTarget.dataset.index;
		console.log("选择标签---", item);
		if (this.data.selectTypeIndex == index){
			this.setData({
				selectTypeItem: '',
				selectTypeIndex: -1,
			});
		} else {
			this.setData({
				selectTypeItem: item,
				selectTypeIndex: index,
			});
		}
		this.resetPaging();
		this.getGoodsList();
	},

	// 获取商品列表
	getGoodsList() {
		let that = this;
		let paginationList = this.data.paginationList; // 分页列表数据
		let pageNum = this.data.pageNum; // 第几页
		let limit = this.data.limit;  // 每页数量
		let isMax = this.data.isMax; // 是否已显示最大数量

		let status = 1; // 是否上架 1 上架 2 下架
		if (this.data.tabsIndex == 0){
			status = 1;
		} else if (this.data.tabsIndex == 1){
			status = 2;
		}

		if (!isMax) {
			app.http({
				url: app.api.shopShopGoodsGoodsList,
				data: {
					page: pageNum, // 第几页
					limit: limit, // 每页数量
					type: that.data.selectTypeItem ? that.data.selectTypeItem.id : '', // 商品分类，传分类id
					name: that.data.topInputValue, // 搜索名称
					status: status, // 是否上架 1 上架 2 下架
				},
				//method: 'get',
			})
				.then((res) => {
					let listData = res.data.data.data; // 请求到的列表数据
					paginationList.push(...listData);
					let total = res.data.data.total; // 列表的总数量
					console.log('商品列表----',listData);
					if (paginationList.length >= total) {
						isMax = true;
						console.log('---------------最大啦---------------')
					}
					pageNum += 1;
					that.setData({
						paginationList: paginationList,
						isMax: isMax,
						pageNum: pageNum
					})
				})
				.catch((err) => {});
		}
	},

	//切换tabs
	onChangeTabs(event) {
		this.setData({
			tabsIndex: event.detail.index
		})

		this.setData({
			topInputValue: '', // 清空输入框
			selectTypeItem: '', // 清空选择的分类
		})

		this.resetPaging(); // 重置分页
		this.getGoodsList(); // 获取商品列表
		// console.log("event.detail.index---",event.detail.index);
	},

	// 点击商品三个点
	dian(ev) {
		let that = this;
		let index = ev.currentTarget.dataset.index;
		let item = ev.currentTarget.dataset.item;
		let paginationList = this.data.paginationList
		if (this.data.goodItemIndex != index){
			paginationList.map(mapRes => {
				mapRes.isShowLayer = false;
			})
		}
		if (paginationList[index].isShowLayer){
			paginationList[index].isShowLayer = false;
		} else {
			paginationList[index].isShowLayer = true;
		}

		// console.log('paginationList---',paginationList);
		this.setData({
			paginationList: paginationList,
			goodItemIndex: index,
			goodItem: item
		})
	},

	// 点击商品弹层状态
	clickGoodStatus(ev){
		// type：1上架，2下架，3申请直播，4取消直播，5删除
		let type = ev.currentTarget.dataset.type;
		console.log('第一层索引---',this.data.goodItemIndex);
		console.log('选择的商品item数据---',this.data.goodItem);

		console.log('type---',type);

		if (type == 1 || type == 2){
			this.onTheShelfGoods({
				id: this.data.goodItem.id, // 商品id
				status: type, // 传 1下架 2上架
			})
		} else if (type == 3 || type == 4){
			let liveType; // 传 1直播，传0取消直播
			if (type == 3) liveType = 1;
			if (type == 4) liveType = 0;
			this.liveBroadcastStatus({
				id: this.data.goodItem.id, // 商品id
				status: liveType, // 传 1下架 2上架
			})
		} else if (type == 5){ // 删除
			this.goodDel({
				id: this.data.goodItem.id, // 商品id
			})
		}
	},

	// 上架，下架商品
	onTheShelfGoods(param){
		let that = this;
		// console.log('上架---下架')
		let paginationList = that.data.paginationList
		app.http({
			url: app.api.shopShopGoodsEditStatus,
			data: {
				id: param.id, // 商品id
				status: param.status, // 1表示上架 2 下架
			},
			//method: 'get',
		})
			.then((res) => {
				let requestData = res.data;
				// console.log('requestData.code---',requestData.code);
				if (requestData.code == 200){
					// console.log('判断 200 里面---------')
					paginationList.splice(that.data.goodItemIndex,1);
					that.setData({
						paginationList
					})
					wx.showToast({
						title: param.status == 1 ? '上架成功' : '下架成功',
						icon: 'success',
						duration: 2000
					})
				}
			})
			.catch((err) => {});
	},

	// 商品-更改直播状态
	liveBroadcastStatus(param){
		let that = this;
		let paginationList = that.data.paginationList;


		app.http({
			url: app.api.shopShopGoodsEditLives,
			data: {
				id: param.id, // 商品id
				live_status: param.status, // 1 直播 0 取消直播
			},
			//method: 'get',
		})
			.then((res) => {
				let requestData = res.data;
				// console.log('requestData.code---',requestData.code);
				if (requestData.code == 200){
					paginationList[that.data.goodItemIndex].live_status = param.status;
					paginationList[that.data.goodItemIndex].isShowLayer = false;
					that.setData({
						paginationList
					})
					wx.showToast({
						title: param.status == 1 ? '申请直播成功' : '取消直播成功',
						icon: 'success',
						duration: 2000
					})
				}
			})
			.catch((err) => {});
	},

	// 商品-删除
	goodDel(param){
		let that = this;
		let paginationList = that.data.paginationList
		wx.showModal({
			title: '提示',
			content: '确定要删除该商品吗？',
			showCancel: true, // 是否显示取消按钮，默认为 true
			success (res) {
				if (res.confirm) {
					console.log('用户点击确定')
					app.http({
						url: app.api.shopShopgoodsGoodsDel,
						data: {
							id: param.id, // 商品id
						},
						//method: 'get',
					})
						.then((res) => {
							let requestData = res.data;
							console.log('requestData---',requestData)
							if (requestData.code == 200){
								// console.log('判断 200 里面---------')
								paginationList.splice(that.data.goodItemIndex,1);
								that.setData({
									paginationList
								})
								wx.showToast({
									title: '删除成功',
									icon: 'success',
									duration: 2000
								})
							}
						})
						.catch((err) => {});
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})
	},


	// 重置分页
	resetPaging(){
		this.setData({
			pageNum: 1, // 第几页
			limit: 10, // 每页数量
			isMax: false, // 是否已显示最大数量
			paginationList: [], // 清空分页数据
			// topInputValue: '', // 清空输入框
			// selectTypeItem: '', // 清空选择的分类
		})
	},

	//监听搜索输入
	bindBlurInput(e) {
		console.log(e.detail.value);
		this.setData({
			topInputValue: e.detail.value,
		});
	},

	//搜索
	inputBtn() {
		let that = this;
		console.log(that.data.topInputValue);
		this.setData({
			selectTypeItem: '', // 清空选择的分类
		})
		this.resetPaging(); // 重置分页
		this.getGoodsList(); // 搜索商品列表
	},


	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.getGoodsList({
			type: '', // 商品分类，传分类id
			name: this.data.topInputValue, // 搜索名称
			status: 1, // 是否上架 1 上架 2 下架
		});
	},
});
