/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 13:49:52
 * @LastEditTime: 2020-08-04 19:19:26
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\exchangeHst\exchangeHst.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区

		pickerDateValue: '', // 顶部时间选择器的值
		requestListArray: [], // 请求到的列表数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;

		let myDate = new Date();
		let realYear = myDate.getFullYear();
		let myMonth =
			myDate.getMonth() + 1 < 10
				? '0' + (myDate.getMonth() + 1)
				: myDate.getMonth() + 1;
		this.setData({
			pickerDateValue: realYear + '-' + myMonth,
		});

		this.requestList();
	},

	// 获取列表
	requestList() {
		let that = this;
		app.http({
			url: app.api.mb_getMiaoList,
			data: {
				time: that.data.pickerDateValue,
			},
		})
			.then((res) => {
				that.setData({
					requestListArray: res.data.data.data,
				});
			})
			.catch((err) => {});
	},

	// 选择时间
	bindDateChange: function (e) {
		this.setData({
			pickerDateValue: e.detail.value,
		});
		this.requestList();
	},
	// 滚动到顶部
	upper(e) {},
	//滚动到底部
	lower(e) {},
});
