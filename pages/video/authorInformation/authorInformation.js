/*
 * @Author: 红尘痴子
 * @Date: 2020-07-09 18:10:34
 * @LastEditTime: 2020-08-13 11:09:44
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\video\authorInformation\authorInformation.js
 * @https://www.jiangcan95.com/
 */

// pages/video/authorInformation/authorInformation.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.setData({ options: options });
		that.post_my_videoHome(options.id);
	},
	// 获取详情
	post_my_videoHome(id) {
		let that = this;
		app.http({
			url: app.api.post_my_videoHome,
			data: {
				uid: id,
				limit: that.data.limit,
				page: that.data.page,
			},
		})
			.then((res) => {
				let list = res.data.data.list.data;
				that.setData({
					videoHome: res.data.data,
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.list.current_page <
						res.data.data.list.last_page,
				});
			})
			.catch((err) => {});
	},
	// 点击视频item
	clickVideoItem(e) {
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/video/video?id=${id}`,
		});
		// wx.navigateTo({
		// 	url: '/pages/video/videoRelease/videoRelease',
		// });
	},
	// 关注/取消关注
	isFollow() {
		let that = this,
			id = that.data.videoHome.user.id,
			is_follow = that.data.videoHome.is_follow;
		if (is_follow) {
			wx.showModal({
				title: '提示',
				content: '是否取消关注!',
				success(res) {
					if (res.confirm) {
						app.http({
							url: app.api.post_my_delFollow,
							data: {
								uid: id,
							},
						})
							.then((res) => {
								// that.onLoad();
								that.post_my_videoHome(id);
							})
							.catch((err) => {});
					} else if (res.cancel) {
					}
				},
			});
		} else {
			wx.showModal({
				title: '提示',
				content: '是否添加关注!',
				success(res) {
					if (res.confirm) {
						app.http({
							url: app.api.post_my_addFollow,
							data: {
								uid: id,
							},
						})
							.then((res) => {
								that.post_my_videoHome(id);
							})
							.catch((err) => {});
					} else if (res.cancel) {
						//console.log('用户点击取消') } }
					}
				},
			});
		}
	},
	//
	// // 点击粉丝
	// clickFans(e){
	//   let btntypeindex = e.currentTarget.dataset.btntypeindex;
	//   // console.log('index---',index);
	//
	//   // 0关注，1粉丝
	//   wx.navigateTo({
	//     url: '/pages/video/followList/followList?btntypeindex=' + btntypeindex
	//   })
	// },

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		let that = this;
		that.setData({
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		that.onLoad(that.data.options.id);
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.post_my_videoHome(that.data.options.id);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
	getPhone() {
		app.store.setState({
			getPhoneNumber: true,
		});
	},
});
