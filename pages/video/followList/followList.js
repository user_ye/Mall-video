// pages/video/followList/followList.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		testTab: ['关注', '粉丝'],
		vanTabIndex: 0, // tabs索引
		isShowTabs: true, // 是否显示tabs

		inputValue: '', // 搜索内容

		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		wx.showLoading({
			title: '加载中',
			mask: true,
		});
		let btntypeindex;
		if (options) {
			btntypeindex = options.btntypeindex || 0;
		} else {
			btntypeindex = 0;
		}
		if (btntypeindex) {
			that.setData({
				vanTabIndex: options.btntypeindex * 1, // 0关注，1粉丝
			});
		}
		if (btntypeindex == 0) {
			that.post_my_myFollow();
		} else {
			that.post_my_myFans();
		}
	},

	// 搜索按钮
	inputBtn() {
		let that = this;
		that.post_my_myFollow(that.data.inputValue);
	},

	// 监听input输入，顶部搜索
	bindInputValue(e) {
		this.setData({
			inputValue: e.detail.value,
		});
	},

	// 点击tabs
	clickVanTabs(e) {
		let that = this;
		let index = e.detail.index;
		this.setData({
			vanTabIndex: index,
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		if (index == 0) {
			that.post_my_myFollow();
		} else {
			that.post_my_myFans();
		}
		// 微信自有api，页面置顶
		wx.pageScrollTo({
			scrollTop: 0,
			duration: 0,
		});
	},
	// 获取关注列表
	post_my_myFollow(name) {
		let that = this;
		let data = {};
		if (name) {
			data = {
				limit: that.data.limit,
				page: that.data.page,
				nickname: name,
			};
		} else {
			data = {
				limit: that.data.limit,
				page: that.data.page,
			};
		}
		app.http({
			url: app.api.post_my_myFollow,
			data: data,
		})
			.then((res) => {
				let list = res.data.data.list.data;
				let testTab = that.data.testTab;
				testTab[0] = '关注 ' + res.data.data.follow_number;
				testTab[1] = '粉丝 ' + res.data.data.fans_number;
				that.setData({
					myFollow:
						that.data.page == 1
							? list
							: that.data.myFollow.concat(list),
					control:
						res.data.data.list.current_page <
						res.data.data.list.last_page,
					testTab: testTab,
				});
				wx.hideLoading();
			})
			.catch((err) => {});
	},
	// 获取粉丝列表
	post_my_myFans() {
		let that = this;
		app.http({
			url: app.api.post_my_myFans,
			data: {
				limit: that.data.limit,
				page: that.data.page,
			},
		})
			.then((res) => {
				let list = res.data.data.list.data;
				that.setData({
					myFans:
						that.data.page == 1
							? list
							: that.data.myFans.concat(list),
					control:
						res.data.data.list.current_page <
						res.data.data.list.last_page,
				});
				wx.hideLoading();
			})
			.catch((err) => {});
	},
	// 取消关注
	post_my_delFollow(e) {
		let that = this;
		wx.showModal({
			title: '提示',
			content: '是否取消关注!',
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.post_my_delFollow,
						data: {
							uid: e.currentTarget.dataset.id,
						},
					})
						.then((res) => {
							// that.onLoad();
							that.post_my_myFollow();
							that.post_my_myFans();
						})
						.catch((err) => {});
				} else if (res.cancel) {
					//console.log('用户点击取消') } }
				}
			},
		});
	},
	// 添加关注
	post_my_addFollow(e) {
		let that = this;
		wx.showModal({
			title: '提示',
			content: '是否添加关注!',
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.post_my_addFollow,
						data: {
							uid: e.currentTarget.dataset.id,
						},
					})
						.then((res) => {
							that.post_my_myFans();
						})
						.catch((err) => {});
				} else if (res.cancel) {
					//console.log('用户点击取消') } }
				}
			},
		});
	},
	// 跳到用户页面
	jump(e) {
		let that = this;
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/video/authorInformation/authorInformation?id=${id}`,
		});
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		let that = this;
		that.setData({
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		that.onLoad();
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.post_my_myFollow();
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
