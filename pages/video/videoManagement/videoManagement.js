// pages/video/videoManagement/videoManagement.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		// testTab: ['作品 32', '喜欢 56', '评论'],
		testTab: ['作品', '喜欢', '评论'],
		vanTabIndex: 0,
		username: '', //名字
		avatarUrl: '', //头像
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值

		myVideoList: [], // 视频列表
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.post_my_myVideo();
		that.post_my_myVideoList();
	},
	// 获取头部数据
	post_my_myVideo() {
		let that = this;
		app.http({
			url: app.api.post_my_myVideo,
			data: {},
		})
			.then((res) => {
				let testTab = that.data.testTab;
				testTab[0] = '作品 ' + res.data.data.my_video_number;
				testTab[1] = '喜欢 ' + res.data.data.video_follow_number;
				that.setData({
					myVideo: res.data.data,
					testTab: testTab,
				});
			})
			.catch((err) => {});
	},
	// 获取作品列表
	post_my_myVideoList() {
		let that = this;
		app.http({
			url: app.api.post_my_myVideoList,
			data: {
				limit: that.data.limit,
				page: that.data.page,
			},
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					myVideoList:
						that.data.page == 1
							? list
							: that.data.myVideoList.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 获取喜欢列表
	post_my_myLikeVideo() {
		let that = this;
		app.http({
			url: app.api.post_my_myLikeVideo,
			data: {
				limit: that.data.limit,
				page: that.data.page,
			},
		})
			.then((res) => {
				let list = res.data.data.data;
				list.map((item) => {
					if (item.nickname.length > 4) {
						item.user_nickname = item.nickname.slice(0, 4) + '...';
					} else {
						item.user_nickname = item.nickname;
					}
					item.user_avatarurl = item.avatarurl;
					item.id = item.vid;
					return item;
				});
				console.log(list);
				that.setData({
					myLikeVideo:
						that.data.page == 1
							? list
							: that.data.myLikeVideo.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 获取评论列表
	post_my_myVideoComment() {
		let that = this;
		app.http({
			url: app.api.post_my_myVideoComment,
			data: {
				limit: that.data.limit,
				page: that.data.page,
			},
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					myVideoComment:
						that.data.page == 1
							? list
							: that.data.myVideoComment.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 点击tabs
	clickVanTabs(e) {
		let that = this;
		let index = e.detail.index;
		this.setData({
			vanTabIndex: index,
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		if (index == 0) {
			that.post_my_myVideoList();
		} else if (index == 1) {
			that.post_my_myLikeVideo();
		} else {
			that.post_my_myVideoComment();
		}
		// 微信自有api，页面置顶
		wx.pageScrollTo({
			scrollTop: 0,
			duration: 0,
		});
	},

	// 点击关注/粉丝
	clickFollowBtn(e) {
		let btntypeindex = e.currentTarget.dataset.btntypeindex;
		// console.log('index---',index);

		// 0关注，1粉丝
		wx.navigateTo({
			url:
				'/pages/video/followList/followList?btntypeindex=' +
				btntypeindex,
		});
	},

	// 点击视频item
	clickVideoItem(e) {
		let that = this;
		// let id = e.currentTarget.dataset.id;
		let item = e.currentTarget.dataset.item;
		// console.log('点击视频e---',item.id);
		wx.navigateTo({
			url: `/pages/video/video?id=${item.id}`,
		});
	},

	// 删除或分享
	delOrShareBtn(e) {
		let that = this;
		let btntype = e.target.dataset.btntype;
		// console.log('btntype--',btntype);
		// 0 分享，1 删除
		if (btntype == 0) {
			wx.navigateTo({
				url: '/pages/InviteFriends/InviteFriends',
			});
		} else if (btntype == 1) {
			app.http({
				url: app.api.post_my_delVideo,
				data: { vid: e.currentTarget.dataset.id },
			})
				.then((res) => {
					that.post_my_myVideoList();
				})
				.catch((err) => {});
		}
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		let that = this;
		that.setData({
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		that.onLoad();
		wx.stopPullDownRefresh();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			if (that.data.vanTabIndex == 0) {
				that.post_my_myVideoList();
			} else if (that.data.vanTabIndex == 1) {
				that.post_my_myLikeVideo();
			} else {
				that.post_my_myVideoComment();
			}
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
