// pages/video/addCommodityOrActivity/addCommodityOrActivity.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		testTab: ['商品', '活动'],
		vanTabIndex: 0, // tabs索引

		inputValue: '', // 搜索内容

		typeSelectArray: ['精选', '销量', '佣金', '价格'], // 类型数据
		typeSelectIndex: 0, // 类型选择索引

		isSales: true, // 是否对销量进行排序
		salest: true, // 是否第一次点击销量

		isCommission: true, // 是否对佣金进行排序
		commissiont: true, // 是否第一次点击佣金

		isSelectPriceSort: true, // 是否对价格进行排序
		isOneSelectPriceSort: true, // 是否第一次点击价格

		gid: 0, //如果已选择商品重新选择时把已选择的商品id传过来
		title: '', //搜索商品标题
		hot: 1, //1-精选商品，4个排序都不传则默认就是精选
		sales: '', //1-销量降序 2-销量升序
		commission: '', //1-佣金降序 2-佣金升序
		price: '', //1-价格降序 2-价格升序
		limit: 10, //分页默认值
		page: 1, //分页默认值
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页

		aid: 0, //活动id
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		if (options.aid != 0) {
			that.setData({
				aid: options.aid,
			});
		} else {
			that.setData({
				gid: options.id,
			});
		}
		that.post_video_getGoods();
	},

	// 类型选项卡
	clickTypeSelect(e) {
		let that = this;
		let index = e.currentTarget.dataset.index;
		that.setData({
			typeSelectIndex: index,
		});
		//销量 salest
		if (index == 1) {
			that.setData({
				isCommission: true, // 是否对佣金进行排序
				commissiont: true, // 是否第一次点击佣金

				isSelectPriceSort: true, // 是否对价格进行排序
				isOneSelectPriceSort: true, // 是否第一次点击价格
			});
			// 是否第一次点击销量
			if (that.data.salest == true) {
				// 如果是，则设置为false
				that.setData({
					salest: false,
					hot: '', //1-精选商品，4个排序都不传则默认就是精选
					sales: 2, //1-销量降序 2-销量升序
					commission: '', //1-佣金降序 2-佣金升序
					price: '', //1-价格降序 2-价格升序
					limit: 10, //分页默认值
					page: 1, //分页默认值
					current_page: 0, //当前页
					last_page: 0, //总页数
					control: false, //是否还有下一页
				});
			} else {
				// 不是第一次点击则更改销量状态进行排序
				that.setData({
					isSales: !that.data.isSales,
					hot: '', //1-精选商品，4个排序都不传则默认就是精选
					sales: 1, //1-销量降序 2-销量升序
					commission: '', //1-佣金降序 2-佣金升序
					price: '', //1-价格降序 2-价格升序
					limit: 10, //分页默认值
					page: 1, //分页默认值
					current_page: 0, //当前页
					last_page: 0, //总页数
					control: false, //是否还有下一页
				});
			}
		}
		//佣金 commission
		else if (index == 2) {
			that.setData({
				isSales: true, // 是否对销量进行排序
				salest: true, // 是否第一次点击销量

				isSelectPriceSort: true, // 是否对价格进行排序
				isOneSelectPriceSort: true, // 是否第一次点击价格
			});
			// 是否第一次点击佣金
			if (that.data.commissiont == true) {
				// 如果是，则设置为false
				that.setData({
					commissiont: false,
					hot: '', //1-精选商品，4个排序都不传则默认就是精选
					sales: '', //1-销量降序 2-销量升序
					commission: 2, //1-佣金降序 2-佣金升序
					price: '', //1-价格降序 2-价格升序
					limit: 10, //分页默认值
					page: 1, //分页默认值
					current_page: 0, //当前页
					last_page: 0, //总页数
					control: false, //是否还有下一页
				});
			} else {
				// 不是第一次点击则更改佣金状态进行排序
				that.setData({
					isCommission: !that.data.isCommission,
					hot: '', //1-精选商品，4个排序都不传则默认就是精选
					sales: '', //1-销量降序 2-销量升序
					commission: 1, //1-佣金降序 2-佣金升序
					price: '', //1-价格降序 2-价格升序
					limit: 10, //分页默认值
					page: 1, //分页默认值
					current_page: 0, //当前页
					last_page: 0, //总页数
					control: false, //是否还有下一页
				});
			}
		}
		// 价格
		else if (index == 3) {
			that.setData({
				isSales: true, // 是否对销量进行排序
				salest: true, // 是否第一次点击销量

				isCommission: true, // 是否对佣金进行排序
				commissiont: true, // 是否第一次点击佣金
			});
			// 是否第一次点击价格
			if (that.data.isOneSelectPriceSort == true) {
				// 如果是，则设置为false
				that.setData({
					isOneSelectPriceSort: false,
					hot: '', //1-精选商品，4个排序都不传则默认就是精选
					sales: '', //1-销量降序 2-销量升序
					commission: '', //1-佣金降序 2-佣金升序
					price: 2, //1-价格降序 2-价格升序
					limit: 10, //分页默认值
					page: 1, //分页默认值
					current_page: 0, //当前页
					last_page: 0, //总页数
					control: false, //是否还有下一页
				});
			} else {
				// 不是第一次点击则更改价格状态进行排序
				that.setData({
					isSelectPriceSort: !that.data.isSelectPriceSort,
					hot: '', //1-精选商品，4个排序都不传则默认就是精选
					sales: '', //1-销量降序 2-销量升序
					commission: '', //1-佣金降序 2-佣金升序
					price: 1, //1-价格降序 2-价格升序
					limit: 10, //分页默认值
					page: 1, //分页默认值
					current_page: 0, //当前页
					last_page: 0, //总页数
					control: false, //是否还有下一页
				});
			}
		} else {
			that.setData({
				isSales: true, // 是否对销量进行排序
				salest: true, // 是否第一次点击销量

				isCommission: true, // 是否对佣金进行排序
				commissiont: true, // 是否第一次点击佣金

				isSelectPriceSort: true, // 是否对价格进行排序
				isOneSelectPriceSort: true, // 是否第一次点击价格

				title: '', //搜索商品标题
				hot: 1, //1-精选商品，4个排序都不传则默认就是精选
				sales: '', //1-销量降序 2-销量升序
				commission: '', //1-佣金降序 2-佣金升序
				price: '', //1-价格降序 2-价格升序
				limit: 10, //分页默认值
				page: 1, //分页默认值
				current_page: 0, //当前页
				last_page: 0, //总页数
				control: false, //是否还有下一页
			});
		}
		that.post_video_getGoods();
	},

	// 搜索按钮
	inputBtn() {
		let that = this;
		if (this.data.inputValue != '') {
			if (that.data.vanTabIndex == 0) {
				that.post_video_getGoods();
			} else {
				that.post_video_getActivityLog();
			}
		} else {
			this.setData({
				inputValue: '',
				title: '',
			});
			if (that.data.vanTabIndex == 0) {
				that.post_video_getGoods();
			} else {
				that.post_video_getActivityLog();
			}
		}
	},

	// 监听input输入，顶部搜索
	bindInputValue(e) {
		this.setData({
			inputValue: e.detail.value,
			title: e.detail.value,
		});
	},

	// 点击tabs
	clickVanTabs(e) {
		let index = e.detail.index;
		let that = this;
		this.setData({
			vanTabIndex: index,
			title: '', //搜索商品标题
			hot: 1, //1-精选商品，4个排序都不传则默认就是精选
			sales: '', //1-销量降序 2-销量升序
			commission: '', //1-佣金降序 2-佣金升序
			price: '', //1-价格降序 2-价格升序
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页

			errorTips: '',
		});
		if (index == 0) {
			if (that.data.aid != 0.1 && that.data.aid != 0) {
				that.setData({
					gid: 0.1,
				});
			}
			that.post_video_getGoods();
		} else {
			if (that.data.gid != 0.1 && that.data.gid != 0) {
				that.setData({
					aid: 0.1,
				});
			}
			that.post_video_getActivityLog();
		}
		// 微信自有api，页面置顶
		wx.pageScrollTo({
			scrollTop: 0,
			duration: 0,
		});
	},
	// 点击添加,移出,替换
	addTo(e) {
		let that = this;
		if (e.currentTarget.dataset.type == 1) {
			that.setData({
				gid: '',
			});
		} else {
			that.setData({
				gid: e.currentTarget.dataset.item.id,
			});
		}
		// that.post_video_getGoods();
		let pages = getCurrentPages(); //获取小程序页面栈
		let beforePage = pages[pages.length - 2]; //获取上个页面的实例对象
		beforePage.setData({
			//直接修改上个页面的数据（可通过这种方式直接传递参数）
			item: e.currentTarget.dataset.item,
		});

		wx.navigateBack({
			delta: 1,
		});
	},
	// 点击添加,移出,替换
	addTot(e) {
		let that = this;
		if (e.currentTarget.dataset.type == 1) {
			that.setData({
				aid: '',
			});
		} else {
			that.setData({
				aid: e.currentTarget.dataset.item.log_id,
			});
		}
		that.post_video_getActivityLog();
		let pages = getCurrentPages(); //获取小程序页面栈
		let beforePage = pages[pages.length - 2]; //获取上个页面的实例对象
		beforePage.setData({
			//直接修改上个页面的数据（可通过这种方式直接传递参数）
			item: e.currentTarget.dataset.item,
		});

		wx.navigateBack({
			delta: 1,
		});
	},
	//获取商品
	post_video_getGoods() {
		let that = this;
		let gid = that.data.gid,
			title = that.data.title,
			hot = that.data.hot,
			sales = that.data.sales,
			commission = that.data.commission,
			price = that.data.price,
			page = that.data.page,
			limit = that.data.limit;
		let data = {
			gid: gid, //如果已选择商品重新选择时把已选择的商品id传过来
			title: title, //搜索商品标题
			hot: hot, //1-精选商品，4个排序都不传则默认就是精选
			sales: sales, //1-销量降序 2-销量升序
			commission: commission, //1-佣金降序 2-佣金升序
			price: price, //1-价格降序 2-价格升序
			page: page, //分页默认值
			limit: limit, //分页默认值
		};
		app.http({
			url: app.api.post_video_getGoods,
			data: data,
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {
				this.setData({
					errorTips: err.data.msg,
				});
			});
	},
	//获取活动
	post_video_getActivityLog() {
		let that = this;
		let data = {
			aid: that.data.aid,
			title: that.data.title,
		};
		app.http({
			url: app.api.post_video_getActivityLog,
			data: data,
		})
			.then((res) => {
				let list = res.data.data;
				that.setData({
					activity: list,
				});
			})
			.catch((err) => {});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.post_video_getGoods();
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
