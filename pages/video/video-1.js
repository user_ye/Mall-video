// pages/video/video.js
let app = getApp();

var time;

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区

		username: '', // 用户名称
		avatarUrl: '', // 用户头像

		videoDataOptions: {}, // 进入这个页面时的请求视频需要的信息

		videoList: [],
		videoId: null,

		lyTopPlNum: 0, // 留言框里的评论数
		plCurrentIndex: 1, // 点击评论按钮时当前的视频索引

		videoSwiperCurrent: 1, // 指定的轮播位置，改初始视频长度时这个要改
		videoSlideIndex: 1, // 初始索引，视频滑动索引，改初始视频长度时这个要改

		isRequestSlide: false, // 是否请求上一条或下一条数据，如果视频列表长度小于等于 3 条，则为 true

		// videoUpNum:0, // 向上滑动次数，每到 2 则重置为 0
		// videoDownNum:0, // 向下滑动次数，每到 2 则重置为 0

		playVideoId: null, // 当前播放的视频的id，当别的页面跳转过来时，需把当前播放的视频id赋值到这里

		isShowVideoRightBox: true, // 是否显示视频右边盒子，解决滑动视频出现的box丢失后问题
		// isShowVideoRightBox:false, // 是否显示视频右边盒子，解决滑动视频出现的box丢失后问题
		isShowVideoCommodityBox: true, // 是否视频下方商品的盒子，解决滑动视频出现的box丢失后问题
		// isShowVideoCommodityBox:false, // 是否视频下方商品的盒子，解决滑动视频出现的box丢失后问题
		commodityBoxLeft: '-600rpx', // 底部盒子默认偏移距离

		commentTextareaValue: null, // 输入的评论内容默认值
		isFocusTextarea: false, // 是否聚焦textarea输入框
		isShowCommentLayer: false, // 是否显示评论弹层
		//当textarea获取焦点时自适应高度，失去焦点时不自适应高度
		//自适应高度时，style中的height无效
		auto_height: true,

		// 顶部关注
		isFollow: false, // 是否关注

		// 收藏
		isCollectionBtn: false, // 是否收藏

		// 分享
		isShowShareLayer: false, // 是否显示分享弹层

		// 视频评论
		oneCommentList: {}, // 一级评论数据
		isShowCommentBtn: [], // 是否显示加载更多按钮

		paginationList: [], // 一级评论分页列表数据
		pageNum: 1, // 第几页
		limit: 10, // 每页数量
		isMax: false, // 是否已显示最大数量

		messagePlaceholder: '留下你的精彩评论吧',

		isShowZzcBox: false, // 是否显示遮罩层

		isClickPlItem: false, // 是否点击评论列表
		plItemE: {}, // 点击的评论列表的 e 数据

		isKyClickXyb: true, // 是否可以点击加载更多
		pl_one_index: null, // 当前点击的一级评论索引

		isPauseVideo: false, // 是否暂停视频

		infoData: '',
		// 临时数据
		replyItem: ['回复 人生若如初见：感谢支持❤️'],

		isLoading: false, // 是否显示加载框
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		let data = {
			id: options.id ? options.id : 1,
			type: options.type ? options.type : null,
			is_hot: options.is_hot ? options.is_hot : '', //  // 首页推荐进来的需要添加这个 值为 1
			shareIt: app.store.$state.shareIt ? app.store.$state.shareIt : 0,
		};
		// 解析
		if (options.scene) {
			let scene = decodeURIComponent(options.scene);
			var arrPara = scene.split('&');
			var arr = [];
			for (var i in arrPara) {
				arr = arrPara[i].split('=');
				if (i == 0) {
					app.store.setState({
						invite_code: arr[1],
					});
				} else if (i == 1) {
					app.store.setState({
						uid: arr[1] ? arr[1] : '',
					});
					that.setData({
						uid: arr[1] ? arr[1] : '',
					});
				} else {
					that.setData({
						productId: arr[1] ? arr[1] : '',
					});
					app.store.setState({
						id: arr[1] ? arr[1] : '',
					});
				}
			}
		}
		if (options.uid || that.data.uid) {
			that.setData({
				shareIt: 1,
			});
		}
		this.setData({
			videoDataOptions: data,
		});

		let infoData = wx.getStorageSync('infoData');
		this.setData({
			infoData: infoData,
		});

		// this.get_index_videoGet(data);
		// this.videoWatchRecord(data.id);

		let login = wx.getStorageSync('login');
		if (login) {
			// that.get_my_getInfo();
			that.getUserInfo();
		}
		//判断onLaunch是否执行完毕
		if (app.globalData.checkLogin) {
			that.reqIntfc(data);
		} else {
			app.checkLoginReadyCallback = (res) => {
				that.reqIntfc(data);
			};
		}
	},
	// 请求接口
	reqIntfc(data) {
		let that = this;
		this.get_index_videoGet(data);
		this.videoWatchRecord(data.id);
		if (app.store.$state.invite_code) {
			that.bindInvitationCd();
			that.setData({
				shareIt: 1,
			});
		}
	},
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline'],
		});
	},

	// 页面初次渲染完成
	onReady(res) {
		let that = this;
		// 滑动显示下方商品盒子
		time = setTimeout(function () {
			that.setData({
				commodityBoxLeft: '0rpx',
			});
		}, 1000);
	},

	// 获取视频详情
	get_index_videoGet(data) {
		let that = this;
		that.setData({
			isShowZzcBox: true,
		});

		wx.showLoading({
			title: '加载中...',
		});

		app.http({
			url: app.api.video_index_newList,
			data: {
				id: data.id,
				is_hot: data.is_hot,
				type: data.type,
			},
			//method: 'get',
		})
			.then((res) => {
				let resVideoList = res.data.data.list;

				that.setData({
					isRequestSlide: res.data.data.no_loop,
				});

				let newVideoList = that.data.videoList;
				newVideoList.push(...resVideoList);

				if (res.data.data.no_loop == true) {
					newVideoList.map((resMap, indexMap) => {
						if (resMap.id == that.data.videoDataOptions.id) {
							that.setData({
								videoSwiperCurrent: indexMap, // 视频小于等于三条时，轮播索引改为 0
								videoSlideIndex: indexMap, // 视频小于等于三条时，轮播索引改为 0
								plCurrentIndex: indexMap, // 视频小于等于三条时，轮播索引改为 0
							});
						}
					});
				}

				that.setData({
					videoList: newVideoList,
					playVideoId: newVideoList[that.data.videoSwiperCurrent].id,
					isPauseVideo: false,
				});

				time = setTimeout(() => {
					let bb = wx.createVideoContext(
						that.data.videoSwiperCurrent + ''
					);
					bb.play();
					that.setData({
						isShowZzcBox: false,
						isPauseVideo: false,
					});
				}, 200);

				this.setData({
					lyTopPlNum:
						that.data.videoList[that.data.videoSwiperCurrent]
							.comments,
				});

				wx.hideLoading();
			})
			.catch((err) => {
				time = setTimeout(() => {
					that.setData({
						isShowZzcBox: false,
					});
				}, 300);
			});
	},

	// 视频观看记录
	videoWatchRecord(vid) {
		let that = this;
		app.http({
			url: app.api.user_video_addWatchRecord,
			data: {
				vid: vid,
			},
			method: 'POST',
		}).then((res) => {});
	},

	// 视频滑动动画完成时触发
	bindanimationfinish(e) {
		let that = this;

		this.setData({
			isPauseVideo: false,
		});

		// setTimeout(() => {
		//   this.setData({
		//     isPauseVideo: false,
		//   });
		// }, 500);

		let currentIndex = e.detail.current;
		let videoSlideIndex = this.data.videoSlideIndex;

		this.setData({
			lyTopPlNum: this.data.videoList[currentIndex].comments,
		});

		this.videoWatchRecord(this.data.videoList[currentIndex].id);

		// 如果视频列表长度 不 小于等于 3 条
		// 视频滑动的索引 == 当前索引时，表示用户没有滑动到上一屏，做到防抖
		if (this.data.videoSlideIndex == currentIndex) {
			return;
		} else {
			let data = {
				id: that.data.videoList[currentIndex].id,
				currentIndex: currentIndex,
				is_hot: this.data.videoDataOptions.is_hot,
				type: this.data.videoDataOptions.type,
			};

			if (
				(videoSlideIndex > currentIndex &&
					videoSlideIndex == 1 &&
					currentIndex == 0) ||
				(videoSlideIndex > currentIndex &&
					videoSlideIndex == 2 &&
					currentIndex == 1) ||
				(videoSlideIndex < currentIndex &&
					videoSlideIndex == 0 &&
					currentIndex == 2)
			) {
				// this.data.isRequestSlide == true

				data.order = '<';
				this.videoSlideRequest(data);
			} else {
				data.order = '>';
				this.videoSlideRequest(data);
			}

			this.setData({
				videoSlideIndex: currentIndex,
				// isPauseVideo: false,
			});

			this.videoTopBoxIsShow(); // 视频上方的盒子是否显示

			let bb = wx.createVideoContext(currentIndex + '');
			bb.play();
		}

		this.setData({
			pageNum: 1, // 第几页
			limit: 10, // 每页数量
			isMax: false, // 是否已显示最大数量
			paginationList: [],
		});
	},

	// 视频滑动时请求
	videoSlideRequest(data) {
		let that = this;
		if (this.data.isRequestSlide != true) {
			this.setData({
				isShowZzcBox: true,
			});

			wx.showLoading({
				title: '加载中...',
			});

			app.http({
				url: app.api.get_index_getSlide,
				data: {
					id: data.id,
					order: data.order,
					type: data.type,
					is_hot: data.is_hot,

					// val: data.val,
				},
				//method: 'get',
			})
				.then((res) => {
					let newList = that.data.videoList;
					let currentIndex = data.currentIndex;

					let spliceData = res.data.data.list;

					if (data.order == '<') {
						// 向下替换
						if (currentIndex == 0) {
							newList.splice(2, 1, spliceData);
						} else if (currentIndex == 1) {
							newList.splice(0, 1, spliceData);
						} else if (currentIndex == 2) {
							newList.splice(1, 1, spliceData);
						}
					} else if (data.order == '>') {
						// 向上替换
						if (currentIndex == 0) {
							newList.splice(1, 1, spliceData);
						} else if (currentIndex == 1) {
							newList.splice(2, 1, spliceData);
						} else if (currentIndex == 2) {
							newList.splice(0, 1, spliceData);
						}
					}

					time = setTimeout(() => {
						that.setData({
							videoList: newList,
						});
					}, 50);

					time = setTimeout(() => {
						that.setData({
							isShowZzcBox: false,
						});
					}, 300);

					wx.hideLoading();
				})
				.catch((err) => {
					time = setTimeout(() => {
						that.setData({
							isShowZzcBox: false,
						});
					}, 300);
				});
		}
	},

	// 视频滑动时触发
	bindchange(e) {
		let that = this;
		let currentIndex = e.detail.current;

		that.setData({
			isShowZzcBox: true,
			// isPauseVideo: false,
		});

		clearTimeout(time); // 清除定时器

		// 先隐藏视频上方盒子
		this.setData({
			isShowVideoRightBox: false,
			isShowVideoCommodityBox: false,
			commodityBoxLeft: '-600rpx',
		});

		// 每次滑动停止上一个播放的视频
		let aa = wx.createVideoContext(this.data.playVideoId + '');
		aa.stop();
	},

	// 视频播放时触发
	onPlay(e) {
		let that = this;
		let currentIndex = e.detail.current;
		let index = e.currentTarget.dataset.index;
		wx.hideLoading();
		// 防止快速点击
		this.setData({
			playVideoId: index,
			isShowZzcBox: true,
			isPauseVideo: true,
		});
		setTimeout(() => {
			that.setData({
				isShowZzcBox: false,
				isPauseVideo: false,
			});
			wx.hideLoading();
		}, 250);

		// setTimeout(() => {
		// 	wx.hideLoading();
		//   this.setData({
		//     isPauseVideo: false,
		//   });
		// }, 500);
	},

	// 视频出现缓冲时触发
	videoBindWaiting() {
		// console.log('视频出现缓冲时触发----')
		wx.showLoading({
			title: '加载中...',
		});
		this.setData({
			isLoading: true,
		});
	},

	// 播放进度变化时
	videoBindtimeupdate() {
		// console.log('播放进度变化时--')
		if (this.data.isLoading == true) {
			// console.log('播放进度变化时---隐藏')
			wx.hideLoading();
			this.setData({
				isLoading: false,
			});
		}
	},

	// 视频元数据加载完成时触发
	videoBindloadeDmetadata() {
		// console.log('视频元数据加载完成时触发----')
		wx.hideLoading();
		setTimeout(() => {
			wx.hideLoading();
		}, 500);
	},

	// 视频上方的盒子是否显示
	videoTopBoxIsShow() {
		let that = this;
		// 解决滑动视频时视频上方盒子被隐藏的bug
		// 每次滑动先隐藏box，然后再显示

		// time = setTimeout(function () {
		//
		// },100)

		that.setData({
			isShowVideoRightBox: true, // 显示右边盒子
		});
		// 滑动显示下方商品盒子
		time = setTimeout(function () {
			that.setData({
				isShowVideoCommodityBox: true, // 先显示
			});
			time = setTimeout(function () {
				that.setData({
					commodityBoxLeft: '0rpx', // 再偏移
				});
			}, 100);
		}, 150);
	},

	// 打开评论弹层
	openCommentLayer(e) {
		let index = e.currentTarget.dataset.index;
		this.setData({
			plCurrentIndex: index,
		});
		this.commentList(); // 请求视频评论列表
	},
	// 关闭评论弹层
	onCloseCommentLayer() {
		this.setData({
			isClickPlItem: false,

			isShowCommentLayer: false,
			commentTextareaValue: null, // 清空评论输入框输入的内容
			messagePlaceholder: '留下你的精彩评论吧',
			pageNum: 1, // 第几页
			limit: 10, // 每页数量
			isMax: false, // 是否已显示最大数量
			paginationList: [],
		});
	},

	// 评论列表滑动到底部触发
	bindScrollToLowerComment() {
		this.commentList();
	},

	// 视频评论列表
	commentList() {
		let that = this;

		let paginationList = this.data.paginationList; // 分页列表数据
		let pageNum = this.data.pageNum; // 第几页
		let limit = this.data.limit; // 每页数量
		let isMax = this.data.isMax; // 是否已显示最大数量

		if (!isMax) {
			wx.showLoading({
				title: '加载中...',
			});

			app.http({
				url: app.api.video_comment_list,
				data: {
					relation_id:
						that.data.videoList[that.data.videoSlideIndex].id,
					page: pageNum, // 第几页
					limit: limit, // 每页数量
				},
				//method: 'get',
			})
				.then((res) => {
					let requestData = res.data.data;

					requestData.data.map((mapRes) => {
						mapRes.childObj = [];
						// mapRes.isClickChildObj = false; // 是否已点击过
					});

					requestData.data.map((mapRes) => {
						mapRes.add_time = app.util.formatTimeMDHM(
							mapRes.add_time
						);

						if (mapRes.child_comment) {
							mapRes.child_comment.add_time = app.util.formatTimeMDHM(
								mapRes.child_comment.add_time
							);
							mapRes.qc = mapRes.child_comment.id + ',';
						}
					});

					paginationList.push(...requestData.data);

					let total = res.data.data.total; // 列表的总数量
					if (paginationList.length >= total) {
						isMax = true;
					}

					pageNum += 1;
					that.setData({
						oneCommentList: requestData,
						paginationList: paginationList,
						isMax: isMax,
						pageNum: pageNum,
					});

					that.setData({
						isShowCommentLayer: true,
					});

					wx.hideLoading();
				})
				.catch((err) => {});
		}
	},

	// 收起评论
	closeTheComments(e) {
		let that = this;
		let index = e.currentTarget.dataset.index;
		let paginationList = that.data.paginationList;
		paginationList[index].isMax = false;
		paginationList[index].childObj = [];
		paginationList[index].qc = '';

		that.setData({
			paginationList: paginationList,
			isFocusTextarea: false, // 取消聚焦textarea输入框
			isKyClickXyb: true, // 是否可以点击加载更多
		});
	},

	// 点击评论弹层里的展开更多，二级评论
	onExpandMore(e) {
		let that = this;
		let id = e.currentTarget.dataset.id;
		let index = e.currentTarget.dataset.index;
		let item = e.currentTarget.dataset.item;
		let child_comment_id = e.currentTarget.dataset.child_comment_id;

		let paginationList = that.data.paginationList;

		that.setData({
			isFocusTextarea: false, // 取消聚焦textarea输入框
		});

		// let qc = that.data.qc;
		let qc = null;
		if (paginationList[index].qc) {
			if (paginationList[index].qc.indexOf(child_comment_id) == -1) {
				paginationList[index].qc =
					paginationList[index].qc + child_comment_id + ',';
			}
			qc = paginationList[index].qc;
			qc = qc.substring(0, qc.lastIndexOf(','));
		}

		// console.log('id------', id);
		// console.log('展开更多回复index--------', index);
		// console.log('child_comment_id--------', child_comment_id);
		// console.log('item--------', item);
		// console.log('---paginationList---', paginationList[index]);

		// return;

		let str = qc;
		let reg = new RegExp('undefined');
		qc = str.replace(reg, '');

		if (!paginationList[index].isMax) {
			app.http({
				url: app.api.video_comment_getChildList,
				data: {
					fid: id,
					page: 1, // 第几页
					limit: 3, // 每页数量
					qc: qc,
				},
				//method: 'get',
			})
				.then((res) => {
					let requestData = res.data.data;

					if (requestData.data.length == 0) {
						that.setData({
							isKyClickXyb: false, // 是否可以点击加载更多
						});
					}
					requestData.data.map((mapRes) => {
						// console.log('mapRes----787878----',mapRes);
						mapRes.add_time = app.util.formatTimeMDHM(
							mapRes.add_time
						);

						if (paginationList[index].qc) {
							if (
								paginationList[index].qc.indexOf(mapRes.id) ==
								-1
							) {
								paginationList[index].qc =
									paginationList[index].qc + mapRes.id + ',';
							}
						}
					});

					paginationList[index].childObj.push(...requestData.data);

					if (
						paginationList[index].childObj.length >=
						requestData.total
					) {
						// isShowCommentBtn[index] = true;
						paginationList[index].isMax = true;

						wx.showToast({
							title: '已显示全部评论',
							icon: 'none',
							duration: 1500,
						});
					} else {
						// isShowCommentBtn[index] = false;
						paginationList[index].isMax = false;
					}
					// that.setData({
					//   isShowCommentBtn:isShowCommentBtn
					// })

					that.setData({
						paginationList: paginationList,
					});

					// console.log('视频一级评论列表oneCommentList---*-----',that.data.oneCommentList)
				})
				.catch((err) => {});
		}
	},

	// 已授权登录---查询用户信息
	getUserInfo() {
		let that = this;
		if (wx.getStorageSync('login')) {
			wx.getUserInfo({
				success: function (res) {
					that.setData({
						username: res.userInfo.nickName,
						avatarUrl: res.userInfo.avatarUrl,
					});
				},
			});
		} else {
		}
	},

	// 评论框发送按钮
	onCommentSendBtn(e) {
		// this.bindGetUserInfo(e);// 用户点击授权
		// return true;
		let that = this;
		if (this.data.username != '' && this.data.avatarUrl != '') {
			// console.log('用户名称*---',this.data.username)
			// console.log('用户头像*---',this.data.avatarUrl)

			if (this.data.isClickPlItem) {
				if (!this.data.commentTextareaValue) {
					wx.showToast({
						title: '评论不能为空',
						icon: 'none',
						duration: 1500,
					});
				} else {
					this.clickCommentItemRequest(); // 发送二级评论
				}
			} else {
				if (!this.data.commentTextareaValue) {
					wx.showToast({
						title: '评论不能为空',
						icon: 'none',
						duration: 1500,
					});
				} else {
					this.sendCommentRequest(); // 发送一级评论
				}
			}
		} else {
			this.userinfo(e); // 授权用户信息
		}
	},

	// 用户点击授权
	userinfo(e) {
		let that = this;

		that.setData({
			username: e.detail.userInfo.nickName,
			avatarUrl: e.detail.userInfo.avatarUrl,
		});

		app.http({
			url: app.api.post_my_updateUserInfo,
			data: {
				encryptedData: e.detail.encryptedData,
				iv: e.detail.iv,
				rawData: e.detail.rawData,
				signature: e.detail.signature,
			},
		})
			.then((res) => {
				// that.onLoad();
			})
			.catch((err) => {});
		//用户按了允许授权按钮
		if (e.detail.userInfo) {
			wx.setStorageSync('login', true);
		} else {
			// wx.showToast({
			// 	title: '您取消了分享',
			// 	icon: 'none',
			// 	duration: 1000,
			// });
		}
	},

	// 发送评论
	sendCommentRequest() {
		let that = this;

		let videoSlideIndex = this.data.videoSlideIndex;
		let videoList = this.data.videoList;
		let videoId = videoList[videoSlideIndex].id;
		app.http({
			url: app.api.user_video_addComment,
			data: {
				id: videoId,
				content: that.data.commentTextareaValue,
			},
			method: 'POST',
		})
			.then((res) => {
				let addOneComment = {
					add_time: app.util.formatTimeMDHM(res.data.data.add_time),
					childObj: [],
					child_count: 0,
					content: that.data.commentTextareaValue,
					id: res.data.data.fid,
					uid: res.data.data.uid,
					user_nickname: that.data.username,
					user_avatarurl: that.data.avatarUrl,
				};

				that.data.paginationList.unshift(addOneComment);
				let videoListS = that.data.videoList;
				videoListS[that.data.plCurrentIndex].comments += 1;
				that.setData({
					videoList: videoListS,

					paginationList: that.data.paginationList,
					commentTextareaValue: '', // 清空评论框
					messagePlaceholder: '留下你的精彩评论吧',
					isFocusTextarea: false,
				});
			})
			.catch((err) => {});
	},

	// 点击评论列表item
	clickCommentItem(e) {
		this.setData({
			plItemE: e,
			isClickPlItem: true,
			isFocusTextarea: true, // 聚焦textarea输入框
		});

		let hierarchy = e.currentTarget.dataset.hierarchy; // 点击了第几级
		let item = e.currentTarget.dataset.item; // 第一级的数据
		let three_item = e.currentTarget.dataset.three_item; // 第三级的数据
		let one_index = e.currentTarget.dataset.one_index; // 第三级的数据

		if (hierarchy == 1) {
			this.setData({
				messagePlaceholder: '回复 ' + item.user_nickname,
			});
		} else if (hierarchy == 2) {
			this.setData({
				messagePlaceholder: '回复 ' + item.child_comment.user_nickname,
				pl_one_index: one_index,
			});
		} else if (hierarchy == 3) {
			this.setData({
				messagePlaceholder: '回复 ' + three_item.user_nickname,
				pl_one_index: one_index,
			});
		}

		// this.clickCommentItemRequest();
	},

	// 点击评论列表 item 请求
	clickCommentItemRequest() {
		let e = this.data.plItemE;
		let that = this;

		let commentTextareaValue = this.data.commentTextareaValue;

		let hierarchy = e.currentTarget.dataset.hierarchy; // 点击了第几级
		let one_index = e.currentTarget.dataset.one_index; // 第一级的索引

		let item = e.currentTarget.dataset.item; // 第一级的数据

		let three_index = e.currentTarget.dataset.three_index; // 第三级的索引
		let three_item = e.currentTarget.dataset.three_item; // 第三级的数据

		let paginationList = this.data.paginationList; // 整个评论列表

		// this.setData({
		//   isFocusTextarea:true, // 聚焦textarea输入框
		// })

		// -------------------------
		let paramData = {
			// id: item.id,
			// uid: item.uid,
			content: commentTextareaValue,
			// content: 2323,
		};

		if (hierarchy == 1) {
			paramData.id = item.id;
			paramData.uid = item.uid;
			// console.log('id----------------------id',item.id)
			// console.log('uid----------------------uid',item.uid)
		} else if (hierarchy == 2) {
			paramData.id = item.id;
			paramData.fid = item.child_comment.id;
			paramData.uid = item.child_comment.uid;
		} else if (hierarchy == 3) {
			paramData.id = item.id;
			paramData.fid = three_item.id;
			paramData.uid = three_item.uid;
		}

		// return;

		app.http({
			url: app.api.user_video_addCommentChild,
			data: paramData,
			method: 'POST',
		})
			.then((res) => {
				that.setData({
					isFocusTextarea: false, // 取消聚焦textarea输入框
				});

				let addComment = {
					content: commentTextareaValue, // 评论的内容
					add_time: app.util.formatTimeMDHM(res.data.data.add_time),
					id: res.data.data.fid,
					uid: res.data.data.uid,
					// add_time: 999, // 添加成功时返回
					// id: 666,  // 添加成功时返回
					// uid: 777,  // 添加成功时返回
					user_nickname: that.data.username,
					user_avatarurl: that.data.avatarUrl,

					// p_user_nickname:item.user_nickname,
				};

				if (hierarchy == 1) {
					addComment.p_user_nickname = item.user_nickname;

					if (paginationList[one_index].child_count == 0) {
						paginationList[one_index].child_count =
							paginationList[one_index].child_count + 1; // 数量加1
						paginationList[one_index].child_comment = addComment;
					} else {
						// 如果 child_count != 0;

						paginationList[one_index].child_count =
							paginationList[one_index].child_count + 1; // 数量加1

						// 先将已存在的第一条 json 评论数据添加到第三级的数组的前面
						paginationList[one_index].childObj.unshift(
							paginationList[one_index].child_comment
						);
						// 再将新的评论数据覆盖到第一条 json 里面
						paginationList[one_index].child_comment = addComment;
					}
				} else if (hierarchy == 2) {
					addComment.p_user_nickname =
						item.child_comment.user_nickname;

					paginationList[one_index].child_count =
						paginationList[one_index].child_count + 1; // 数量加1

					paginationList[one_index].childObj.unshift(
						paginationList[one_index].child_comment
					);
					paginationList[one_index].child_comment = addComment;
				} else if (hierarchy == 3) {
					addComment.p_user_nickname = three_item.user_nickname;

					paginationList[one_index].child_count =
						paginationList[one_index].child_count + 1; // 数量加1

					paginationList[one_index].childObj.unshift(
						paginationList[one_index].child_comment
					);
					paginationList[one_index].child_comment = addComment;
				}
				let videoListS = that.data.videoList;
				videoListS[that.data.plCurrentIndex].comments += 1;

				if (res.data.data.id) {
					paginationList[one_index].qc =
						paginationList[one_index].qc + res.data.data.id + ',';
				}

				that.setData({
					videoList: videoListS,
					paginationList: paginationList,
					commentTextareaValue: '', // 清空评论框
					messagePlaceholder: '留下你的精彩评论吧',
					isFocusTextarea: false,
				});
			})
			.catch((err) => {});

		// this.setData({
		//   messagePlaceholder: '回复 ' + name
		// })
	},

	// 评论区键盘输入时事件
	commentbindinput(e) {
		this.setData({
			commentTextareaValue: e.detail.value,
		});
	},

	// 点击视频下方商品的去参与
	toParticipate(e) {
		let id = e.currentTarget.dataset.id; // 视频id
		let relation_id = e.currentTarget.dataset.relation_id; // 关联的商品或者活动id
		let log_id = e.currentTarget.dataset.log_id; // 关联的商品或者活动id
		let activity = e.currentTarget.dataset.activity;

		if (activity == true) {
			// 如果是活动商品
			wx.navigateTo({
				url: `/pages/lstOfGrpDetails/lstOfGrpDetails?id=${relation_id}&video_id=${id}&log_id=${log_id}`,
			});
		} else {
			// 如果不是
			wx.navigateTo({
				url: `/pages/productDetails/productDetails?id=${relation_id}&video_id=${id}&type=${1}`,
			});
		}
	},

	// 收藏
	collectionBtn(e) {
		let that = this;

		let id = e.currentTarget.dataset.id;
		let index = e.currentTarget.dataset.index;
		let is_likes = e.currentTarget.dataset.is_likes;
		let videoList = this.data.videoList;

		let type = null;

		// type 1 表示喜欢，0 取消喜欢
		if (is_likes) {
			// 如果为 true 则为喜欢
			type = 0;
		} else {
			type = 1;
		}

		app.http({
			url: app.api.user_video_videoChecklike,
			data: {
				vid: id,
				like: type,
			},
			//method: 'get',
		})
			.then((res) => {
				if (res.data.code == 200) {
					// type 1 表示喜欢，0 取消喜欢
					if (type == 1) {
						videoList[index].is_likes = true;
						videoList[index].likes = videoList[index].likes + 1;
					} else {
						videoList[index].is_likes = false;
						videoList[index].likes = videoList[index].likes - 1;
					}
					that.setData({
						videoList: videoList,
					});
				}
			})
			.catch((err) => {});
	},

	// 用户授权信息传给后台
	bindGetUserInfo(e) {
		let that = this;
		app.http({
			url: app.api.post_my_updateUserInfo,
			data: {
				encryptedData: e.detail.encryptedData,
				iv: e.detail.iv,
				rawData: e.detail.rawData,
				signature: e.detail.signature,
			},
		})
			.then((res) => {
				that.onLoad();
			})
			.catch((err) => {});
		//用户按了允许授权按钮
		if (e.detail.userInfo) {
			that.setData({
				username: e.detail.userInfo.nickName,
				profPic: e.detail.userInfo.avatarUrl,
				login: true,
				qqqq: true,
			});
			wx.setStorageSync('login', true);
		} else {
			// wx.showToast({
			// 	title: '您取消了授权',
			// 	icon: 'none',
			// 	duration: 1500,
			// });
		}
	},

	// 顶部关注
	onFollow(e) {
		let that = this;
		let uid = e.currentTarget.dataset.uid;
		let index = e.currentTarget.dataset.index;
		let is_follow = e.currentTarget.dataset.is_follow;
		let videoList = this.data.videoList;

		if (is_follow) {
			// 取消关注
			app.http({
				url: app.api.post_my_delFollow,
				data: {
					uid: uid,
				},
				method: 'POST',
			})
				.then((res) => {
					let requestData = res.data.data;
					if (requestData == true) {
						videoList[index].is_follow = false;
						that.setData({
							videoList: videoList,
						});
					}
				})
				.catch((err) => {});
		} else {
			app.http({
				url: app.api.post_my_addFollow,
				data: {
					uid: uid,
				},
				method: 'POST',
			})
				.then((res) => {
					let requestData = res.data.data;
					if (requestData == true) {
						videoList[index].is_follow = true;
						that.setData({
							videoList: videoList,
						});
					}
				})
				.catch((err) => {});
		}
	},

	// 分享的方法
	// 关闭分享弹层
	onCloseShareLayer() {
		this.setData({
			isShowShareLayer: false,
		});
	},
	// 打开分享弹层
	openShareLayer() {
		this.setData({
			isShowShareLayer: true,
		});
	},

	// 顶部返回按钮
	topReturnBtn() {
		if (this.data.shareIt == 1) {
			wx.switchTab({
				url: '/pages/tabBar/index/index',
			});
		} else {
			wx.navigateBack({
				delta: 1,
			});
		}
	},

	// 点击视频
	clickVideo(e) {
		let index = e.currentTarget.dataset.index;
		let bb = wx.createVideoContext(index + '');
		bb.pause();
	},

	// 播放视频
	playVideoZzcBtn(e) {
		let that = this;
		let index = e.currentTarget.dataset.index;
		let bb = wx.createVideoContext(index + '');
		bb.play();
		this.setData({
			isPauseVideo: false,
		});

		wx.hideLoading();

		setTimeout(() => {
			wx.hideLoading();
		}, 500);
	},

	// 视频暂停时触触发
	bindpauseVideo(e) {
		let id = e.currentTarget.offsetLeft;
		this.setData({
			isPauseVideo: true,
		});
		wx.hideLoading();

		setTimeout(() => {
			wx.hideLoading();
		}, 500);
	},
	// 跳到用户页面
	jump(e) {
		let that = this;
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/video/authorInformation/authorInformation?id=${id}`,
		});
	},
	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function (res) {
		let that = this;
		let item = res.target.dataset.item;
		let invite_code = app.store.getState().infoData.invite_code;
		// let uid = app.store.getState().infoData.id;
		let id = that.data.videoDataOptions.id;
		// let bg = 'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/bg/invite-friends-bg.png';
		let bg = item.cover;
		if (res.from === 'button') {
			app.http({
				url: app.api.get_video_shareVideo,
				data: {
					id: id,
				},
				//method: 'get',
			})
				.then((res) => {
					that.get_index_videoGet(that.data.videoDataOptions);
				})
				.catch((err) => {});
			return {
				title: item.title,
				path: `/pages/video/video?code=${invite_code}&id=${id}`,
				imageUrl: bg,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
	getPhone() {
		app.store.setState({
			getPhoneNumber: true,
		});
	},
	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {
		app.store.setState({
			shareIt: 0,
		});
	},
});
