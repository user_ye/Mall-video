// pages/video/videoRelease/videoRelease.js

let app = getApp();

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		videoCover: ' ', // 视频封面
		videoTitleValue: '', // 输入的视频标题
		videoSrc: '',
		// 视频分类选择器
		VideoArray: [
			'请选择',
			'分类1',
			'分类2',
			'分类3',
			'分类4',
			'分类5',
			'分类6',
		],
		VideoIndex: 0, // 视频分类选择器索引

		location: '', // 位置信息

		item: {}, // 下级页面传过来的

		type_id: '',
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		// let aa = wx.createVideoContext(res.id);
		let that = this;
		that.videoClassification();
		that.setData({
			id: options.id,
			videoSrc: options.video,
			videoCover: options.videoImg,
		});
		let qm = app.qm({ type: 'image', id: options.id });
		wx.uploadFile({
			url: app.api_url + '/user/video/publishVideoImg',
			filePath: options.videoImg,
			name: 'img',
			header: {
				Authorization: app.store.$state.token,
				'content-type': 'application/x-www-form-urlencoded',
			},
			formData: qm,
			success: (res) => {
				let codeJson = JSON.parse(res.data);
				this.setData({
					videoCover: codeJson.data.cover,
				});
			},
		});
	},
	//提交
	submitBtn() {
		let that = this;

		// let data = {
		// 	id: that.data.id, // 视频id
		// 	title: that.data.videoTitleValue, // 视频标题
		// 	type_id: that.data.type_id
		// 		? that.data.type_id
		// 		: that.data.VideoArray[that.data.VideoIndex].id, // 视频分类id
		// 	type: '', // 1-商品 2-活动
		// 	aid: '', // 活动id
		// 	log_id: '', // 活动log_id
		// 	gid: '', // 商品id
		// 	position: that.data.location ? that.data.location : '', // 位置信息
		// };
		// if (that.data.item.log_id) {
		// 	data.aid = that.data.item.id;
		// 	data.log_id = that.data.item.log_id;
		// 	data.type = 2;
		// } else {
		// 	data.gid = that.data.item.id;
		// 	data.type = 1;
		// }

		let data = {
			id: that.data.id, // 视频id
			title: that.data.videoTitleValue, // 视频标题
			type_id: that.data.type_id
				? that.data.type_id
				: that.data.VideoArray[that.data.VideoIndex].id, // 视频分类id
			type: '', // 1-商品 2-活动
			aid: that.data.item.id ? that.data.item.id : 0, // 活动id
			log_id: that.data.item.log_id ? that.data.item.log_id : 0, // 活动log_id
			gid: that.data.item.id ? that.data.item.id : 0, // 商品id
			position: that.data.location ? that.data.location : '', // 位置信息
		};

		if (that.data.item.log_id) {
			data.type = 2;
		} else {
			data.type = 1;
		}

		if (data.title == '') {
			wx.showToast({
				title: '请输入视频标题',
				icon: 'none',
				duration: 1500,
			});
			return;
		} else if (!data.aid || !data.gid) {
			data.type = 0;
		}

		app.http({
			url: app.api.post_video_publishVideoLog,
			data: data,
		})
			.then((res) => {
				wx.showToast({
					title: '发布成功',
					icon: 'success',
					duration: 2000,
				});
				setTimeout(() => {
					wx.navigateBack({
						data: 1,
					});
				}, 2000);
			})
			.catch((err) => {});
	},

	// 删除位置按钮
	delLocationBtn() {
		this.setData({
			location: '',
		});
	},

	// 上传视频封面图片
	uploadVideoCover() {
		let that = this;
		wx.chooseImage({
			count: 1,
			sizeType: ['original', 'compressed'],
			sourceType: ['album', 'camera'],
			success: (res) => {
				const tempFilePaths = res.tempFilePaths;
				// 将本地资源上传到开发者服务器
				let qm = app.qm({
					type: 'image',
					id: that.data.id,
				});
				wx.uploadFile({
					url: app.api_url + '/user/video/publishVideoImg',
					filePath: tempFilePaths[0],
					name: 'img',
					header: {
						Authorization: app.store.$state.token,
						'content-type': 'application/x-www-form-urlencoded',
					},
					formData: qm,
					success: (res) => {
						let codeJson = JSON.parse(res.data);
						this.setData({
							// videoCover: tempFilePaths[0]
							videoCover: codeJson.data.cover,
						});
					},
				});
			},
		});
	},

	// 添加商品或活动
	addCommodityBtn() {
		let that = this;
		let id = that.data.item.id ? that.data.item.id : 0;
		let aid = that.data.item.log_id ? that.data.item.log_id : 0;
		wx.navigateTo({
			url: `/pages/video/addCommodityOrActivity/addCommodityOrActivity?id=${id}&aid=${aid}`,
		});
	},

	// 监听input输入，输入的视频标题
	bindInputvideoTitle(e) {
		let that = this;
		// console.log(e.detail.value);
		that.setData({
			videoTitleValue: e.detail.value,
		});
	},

	// 视频分类选择器
	bindPickerVideo: function (e) {
		let that = this;
		that.setData({
			type_id: that.data.VideoArray[e.detail.value].id,
		});
		this.setData({
			VideoIndex: e.detail.value,
		});
	},

	// 调用微信地图api
	location: function () {
		// 获取所在城市
		let that = this;

		//// 可以通过 wx.getSetting 先查询一下用户是否授权了 "scope.userLocation" 这个 scope
		wx.getSetting({
			// 接口调用成功的回调函数
			success: (res) => {
				//authSetting用户授权设置信息
				if (!res.authSetting['scope.userLocation']) {
					// wx.authorize 在调用需授权 API 之前，提前向用户发起授权请求。
					wx.authorize({
						//scope需要获取权限的 scope，详见scope 列表
						scope: 'scope.userLocation',
						success: (res) => {
							// 用户已经同意小程序使用地理位置功能，后续调用 wx.chooseLocation 接口不会弹窗询问
							wx.chooseLocation({
								success: (res) => {
									//设置位置名称到location
									this.setData({
										location: res.address,
									});
								},
							});
						},
						fail() {
							wx.showModal({
								title: '提示',
								content: '若点击不授权，将无法使用定位功能',
								cancelText: '不授权',
								cancelColor: '#999',
								confirmText: '授权',
								confirmColor: '#f94218',
								success(res) {
									if (res.confirm) {
										wx.openSetting({
											success(res) {},
										});
									} else if (res.cancel) {
									}
								},
							});
						},
					});
				} else {
					// 有则直接调用
					wx.chooseLocation({
						success: (res) => {
							this.setData({
								location: res.address,
							});
						},
						fail: (res) => {
							// this.setData({
							//   location: '',
							//   nullLocation:'所在位置'
							// });
						},
					});
				}
			},
			// 接口调用失败的回调函数
			fail: function (res) {},
		});
	},

	// videoBindtimeupdate(e){
	//   console.log('e---',e.detail);
	// },
	// 获取视频分类
	videoClassification() {
		let that = this;
		app.http({
			url: app.api.post_video_getType,
			data: {},
		})
			.then((res) => {
				that.setData({
					VideoArray: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 移除
	del() {
		let that = this;
		that.setData({
			item: {},
		});
	},
});
