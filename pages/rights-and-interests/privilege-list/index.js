// pages/rights-and-interests/test/index.js
let app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[], // 请求的活动列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.volumeIndexGetList(); // 购物卷活动列表
  },

  volumeIndexGetList(){
    let that = this;
    app.http({
      url: app.api.volumeIndexGetList,
      data: {},
      //method: 'get',
    })
      .then((res) => {
        console.log('res-----222--',res.data.data);
        let requestData = res.data.data;
        that.setData({
          list:requestData
        })
      })
      .catch((err) => {});
  },

  clickItem(ev){
    let item = ev.currentTarget.dataset.item;
    // console.log("item---", item);
    wx.navigateTo({
      url: '/pages/rights-and-interests/1999-2-2/index?privilegeListItem=' + JSON.stringify(item)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
