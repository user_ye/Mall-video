// pages/rights-and-interests/privilege-details/privilege-details.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    status: app.globalData.statusHeight, // 头部占位高度
    navHeight: app.globalData.navHeight, // 头部内容区

    imgArray:[
      'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/rightsAndInterests/1999/zp-2020-9-12-17-42.png',
      'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/rightsAndInterests/1999/zp-2020-9-12-17-42.png',
      'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/rightsAndInterests/1999/zp-2020-9-12-17-42.png',
      'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/rightsAndInterests/1999/zp-2020-9-12-17-42.png',
      // 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1581257685408&di=df0d57d3fb505c4fd50a50f43811c2cd&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fwallpaper%2F1410%2F14%2Fc0%2F39651560_1413274349352_320x480.jpg',
    ],

    result:"",
    requestUrl:'',
    requestImg:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log('options---',options)
    let privilegeItem = JSON.parse(options.privilegeItem);
    console.log('privilegeItem---',privilegeItem)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.index_getInfo();
  },

  index_getInfo() {
    let that = this;
    app.http({
      url: app.api.index_getInfo,
      data: {
        // type: that.data.type,
        type: 2,
      },
    })
      .then((res) => {
        let requestData = res.data.data;
        console.log("requestData---", requestData.desc);
        var re = /&lt;apk&gt;(.*?)&lt;apk&gt;/;
        var result = requestData.desc.match(re);
        console.log("result--000--", result[0]);
        console.log("result--111--", result[1]);
        that.setData({
          result:result
        })
      })
      .catch((err) => {});
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
