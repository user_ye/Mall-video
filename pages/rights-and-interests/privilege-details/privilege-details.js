// pages/rights-and-interests/privilege-details/privilege-details.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    status: app.globalData.statusHeight, // 头部占位高度
    navHeight: app.globalData.navHeight, // 头部内容区

    backType: 'back', // 返回类型，1上一页，2首页

    result:"",
    requestUrl:'',
    requestImg:'',
    requestData:{}, // 请求的详情数据
    desc:'', // 富文本
    privilegeItem:{},
    pageType:'', // 后台配置的类型
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('options---',options.type)
    if (options.type){
      this.setData({
        pageType: options.type
      })
    }
    let privilegeItem = JSON.parse(options.privilegeItem);
    if (privilegeItem){
      this.setData({
        privilegeItem: privilegeItem
      })
    }

    console.log('privilegeItem--999----',privilegeItem)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log('-------判断顶部按钮返回类型------')
    this.topReturnBtn(); // 判断顶部按钮返回类型


    if (app.globalData.checkLogin) {

      console.log('-------222222------------')
      console.log('getApp().store.$state.token---111111111---------',getApp().store.$state.token)
      console.log('---------999999999-----111-----------',getApp().store.$state.invite_code)

      // 跳转到这个页面已经没有code了
      // if (app.store.$state.invite_code) {
      //   that.bindInvitationCd();
      //   that.setData({
      //     shareIt: 2,
      //   });
      //
      // }
      this.index_getInfo();
    } else {
      app.checkLoginReadyCallback = (res) => {

        console.log('getApp().store.$state.token---222222222---------',getApp().store.$state.token)
        console.log('---------999999999------222----------',getApp().store.$state.invite_code)
        // if (app.store.$state.invite_code) {
        //   that.bindInvitationCd();
        //   that.setData({
        //     shareIt: 2,
        //   });
        //
        // }
        this.index_getInfo();
        console.log('-------3333333333------------')
      };
    }




    // this.index_getInfo();

    // 禁用分享
    wx.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  // 返回首页
  backHome(){
    wx.switchTab({
      url: '/pages/tabBar/index/index'
    })
  },

  index_getInfo() {
    console.log('index_getInfo---888---',this.data.privilegeItem)
    let that = this;
    app.http({
      url: app.api.index_getInfo,
      data: {
        // type: that.data.type,
        type: that.data.pageType ? that.data.pageType : that.data.privilegeItem.id,
      },
    })
      .then((res) => {
        let requestData = res.data.data;
        console.log("requestData---", requestData);

        if (that.data.pageType){
          that.setData({
            privilegeItem: requestData
          })
        }

        that.setData({
          requestData: requestData,
          // result:result,
          desc:requestData.desc
        })
      })
      .catch((err) => {});
  },

  bindTapPhoneNumber(){
    app.store.setState({
      login: true,
      getPhoneNumber: false,
      loginType: 2,
    });
  },

  // 购买按钮
  btn(ev) {
    let that = this;
    // console.log("item---", item);
    console.log('购买按钮---------------------',ev)
    // console.log('购买按钮-------listItem--------------',this.data.listItem)

    // let item = ev.currentTarget.dataset.item;
    app.http({
      url: app.api.post_my_rechargeVolume,
      data: {
        // id: that.data.listItem.id ? that.data.listItem.id : item.id,
        id: that.data.pageType ? that.data.pageType : that.data.privilegeItem.id,
        code: that.data.invite_code,
      },
    })
      .then((res) => {
        console.log('调起微信支付前*-----------------')
        wx.requestPayment({
          //调起支付
          //下边参数具体看微信小程序官方文档
          timeStamp: res.data.data.timeStamp,
          nonceStr: res.data.data.nonceStr,
          package: res.data.data.package,
          signType: res.data.data.signType,
          paySign: res.data.data.paySign,
          success(res) {
            // wx.requestSubscribeMessage({
            // 	tmplIds: [
            // 		'FddT6-1bVu2lfiggUR7FVmS0mwrvTlGIgksy6xh94XE',
            // 	],
            // 	success(res) {},
            // });
            // 微信支付ok 直接跳转到我的订单列表
            if (res.errMsg == 'requestPayment:ok') {
              wx.navigateTo({
                url:
                  '/pages/shoppingVolume/recordsOfConsumption/recordsOfConsumption',
              });
            }
          },
          fail(res) {},
        });
      })
      .catch((err) => {});
  },

  // bindTapPhoneNumber(){
  //   app.store.setState({
  //     login: true,
  //     getPhoneNumber: false,
  //     loginType: 2,
  //   });
  // },

  bindGetPhoneNumber(e) {
    console.log('e----',e);
    if (e.detail.iv) {
      app.store.setState({
        login: true,
        getPhoneNumber: false,
        loginType: 2,
      });
    } else {
      wx.showToast({
        title: '您取消了授权',
        icon: 'none',
        duration: 1500,
      });
    }
  },

  bindGetUserInfo(ev){
    let item = ev.currentTarget.dataset.item;
    console.log('item---99---',item)
  },

  PhoneNumber(e) {
    console.log('PhoneNumber--eee--',e)
    let that = this;
    // 登录
    wx.login({
      success(res) {
        let header = {};
        if (res.code) {
          app.http({
            url: '/user/login/wxLogin',
            data: {
              code: res.code,
            },
            header: header,
          })
            .then((res) => {
              wx.setStorageSync('token', res.data.data.token);
              if (e.detail.iv) {
                app.http({
                  url: app.api.post_index_bindingPhone,
                  data: {
                    encryptedData: e.detail.encryptedData,
                    iv: e.detail.iv,
                  },
                })
                  .then((res) => {
                    app.store.setState({
                      login: true,
                      getPhoneNumber: false,
                      loginType: 2,
                    });
                    console.log('that.btn();------222--')
                    that.btn(e);
                  })
                  .catch((err) => {});
              } else {
                wx.showToast({
                  title: '您取消了授权',
                  icon: 'none',
                  duration: 1500,
                });
              }
            })
            .catch((err) => {});
        }
      },
      fail: (err) => {},
    });
  },

  // 顶部返回按钮
  topReturnBtn() {
    let pages = getCurrentPages(); //获取所有页面
    console.log('pages---',pages);
    console.log('pages.length---',pages.length);
    if (pages.length <= 1){
      // 返回首页
      console.log('返回首页---')
      this.setData({
        backType: 'home'
      })
    } else {
      console.log('返回上一页---')
      // 返回上一页
      this.setData({
        backType: 'back'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
