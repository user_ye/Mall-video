/*
 * @Author: 红尘痴子
 * @Date: 2020-07-07 18:47:52
 * @LastEditTime: 2020-07-15 11:23:02
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\ownershipRights\ownershipRights.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		background: ['1', '2', '3', '4'],
		numOfSliders: 0, //滑块数
		zuoimg:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Ownership%20rights/%E7%BC%96%E7%BB%84%204%402x.png', //标题左边菱形
		youimg:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Ownership%20rights/%E7%BC%96%E7%BB%84%205%402x.png', //标题右边菱形
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.post_my_userViewLevel();
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {},
	// 滑块监听
	sliderMonitoring(e) {
		let that = this;
		that.setData({
			numOfSliders: e.detail.current,
		});
	},
	// 获取首页直播广告图
	post_my_userViewLevel() {
		let that = this;
		app.http({
			url: app.api.post_my_userViewLevel,
			data: {},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					background: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 跳到合成海报页面
	generatePoster() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/syntheticPoster/syntheticPoster?id=${index}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
