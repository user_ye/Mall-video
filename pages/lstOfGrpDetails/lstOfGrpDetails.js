/*
 * @Author: 红尘痴子
 * @Date: 2020-07-02 14:01:16
 * @LastEditTime: 2020-09-03 18:14:04
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\lstOfGrpDetails\lstOfGrpDetails.js
 * @https://www.jiangcan95.com/
 */

// pages/lstOfGrpDetails/lstOfGrpDetails.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区// 精品活动数据
		boutiqueEvntData: [],
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		// 获取上页url地址
		let pages = getCurrentPages(); //页面对象
		let prevpage = pages[pages.length - 2]; //上一个页面对象
		if (prevpage) {
			if ('pages/video/video' == prevpage.route) {
				that.setData({
					log_id: options.log_id,
					video_id: options.video_id,
				});
			}
		}
		that.setData({
			productId: options.id ? options.id : app.store.$state.id,
			shareIt: app.store.$state.shareIt ? app.store.$state.shareIt : 0,
			uid: options.uid ? options.uid : '',
		});
		console.log('options.id---',options.id)
		app.store.setState({
			uid: options.uid ? options.uid : '',
			invite_code: options.code ? options.code : '',
			activity_id: options.id ? options.id : '',
		});
		// 解析
		if (options.scene) {
			let scene = decodeURIComponent(options.scene);
			var arrPara = scene.split('&');
			var arr = [];
			for (var i in arrPara) {
				arr = arrPara[i].split('=');
				if (i == 0) {
					app.store.setState({
						invite_code: arr[1],
					});
				} else if (i == 1) {
					app.store.setState({
						uid: arr[1] ? arr[1] : '',
					});
					that.setData({
						uid: arr[1] ? arr[1] : '',
					});
				} else {
					that.setData({
						productId: arr[1] ? arr[1] : '',
					});
					app.store.setState({
						id: arr[1] ? arr[1] : '',
					});
				}
			}
		}
		if (options.uid || that.data.uid) {
			that.setData({
				shareIt: 1,
			});
		}
		//判断onLaunch是否执行完毕
		if (app.globalData.checkLogin) {
			that.reqIntfc();
		} else {
			app.checkLoginReadyCallback = (res) => {
				that.reqIntfc();
			};
		}
	},
	// 请求接口
	reqIntfc() {
		let that = this;
		that.get_mall_get(this.data.productId);
		if (app.store.$state.invite_code) {
			// that.bindInvitationCd();
			app.store.setState({
				shareIt: 1,
				invite_code: '',
			});
			that.setData({
				shareIt: 1,
			});
			that.post_lookActivity();
		}
	},
	post_lookActivity() {
		let that = this;
		app.http({
			url: app.api.post_lookActivity,
			data: {
				aid: that.data.productId,
				uid: that.data.uid,
			},
		})
			.then((res) => {
				let list = res.data.data.goods;
				that.setData({
					activity: res.data.data.activity,
					boutiqueEvntData: list,
				});
			})
			.catch((err) => {});
	},
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline'],
		});
	},
	// 跳转到海报合成页面
	poster() {
		let that = this;
		let id = that.data.productId;
		wx.navigateTo({
			url: `/pages/syntheticPoster/syntheticPoster?id=${id}&type=2&page=pages/lstOfGrpDetails/lstOfGrpDetails`,
		});
	},
	// 获取拼团活动列表
	get_mall_get(id) {
		let that = this;
		app.http({
			url: app.api.get_mall_get,
			data: { id: id, limit: that.data.limit, page: that.data.page },
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.goods;
				that.setData({
					activity: res.data.data.activity,
					boutiqueEvntData: list,
				});
			})
			.catch((err) => {});
	},
	// 跳到规则页面
	ruleOfActivity() {
		let that = this;
		let id = that.data.productId;
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?id=${id}`,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		// let that = this;
		// let control = that.data.control;
		// let id = that.data.productId; //商品id
		// if (control) {
		// 	that.setData({
		// 		page: that.data.page + 1,
		// 	});
		// 	that.get_mall_get(id);
		// } else {
		// 	wx.showToast({
		// 		title: '没有更多了',
		// 		icon: 'none',
		// 	});
		// }
	},
	// 返回上一页
	back() {
		if (this.data.shareIt == 1) {
			wx.switchTab({
				url: '/pages/tabBar/index/index',
			});
		} else {
			wx.navigateBack({
				delta: 1,
			});
		}
	},
	//转发
	onShareAppMessage: function (res) {
		let that = this;
		let invite_code = app.store.getState().infoData.invite_code;
		let uid = app.store.getState().infoData.id;
		let id = that.data.productId;
		let bg = that.data.activity.share;
		if (res.from === 'button') {
			return {
				title: that.data.activity.title,
				path: `/pages/lstOfGrpDetails/lstOfGrpDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}`,
				imageUrl: bg,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
	/**
	 * 生命周期函数--监听页面卸载
	 */ onUnload: function () {
		app.store.setState({
			shareIt: 0,
		});
	},
});
