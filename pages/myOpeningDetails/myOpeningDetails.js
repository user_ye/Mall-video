/*
 * @Author: 红尘痴子
 * @Date: 2020-07-07 14:12:19
 * @LastEditTime: 2020-08-29 17:47:28
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\myOpeningDetails\myOpeningDetails.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		imgUrl: '/resource/image/tx1.gif',
		timeData: {},
		show: false,
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值

		myActivityInfo:{}, // 请求到的活动数据
		backType:'', // 返回类型
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		console.log('options----111--',options)
		let paramId = options.log_id ? options.log_id : options.id
		that.post_branch_myActivityInfo(paramId);
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.getHeightDynamically();
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline'],
		});
		this.topReturnBtn();
	},


	// 顶部返回按钮
	topReturnBtn() {
		let pages = getCurrentPages(); //获取所有页面
		// console.log('pages---',pages);
		// console.log('pages.length---',pages.length);
		if (pages.length <= 1){ // 返回类型，back,home
			this.setData({
				backType:'home'
			})
			// 返回首页
			// wx.switchTab({
			//   url: '/pages/index/index',
			// });
		} else {
			this.setData({
				backType:'back'
			})
			// 返回上一页
			// wx.navigateBack({
			//   delta: 1,
			// });
		}
	},

	// 倒计时
	onChange(e) {
		this.setData({
			timeData: e.detail,
		});
	},
	// 打开弹窗
	onClose() {
		let that = this;
		that.setData({ show: !that.data.show });
	},
	// 跳到合成海报页面
	generatePoster() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/syntheticPoster/syntheticPoster?id=${index}`,
		});
	},
	// 获取数据
	post_branch_myActivityInfo(id) {
		let that = this;
		app.http({
			url: app.api.post_branch_myActivityInfo,
			data: { log_id: id },
		})
			.then((res) => {
				that.setData({
					myActivityInfo: res.data.data,
					users: res.data.data.users.reverse(),
					good_attr: res.data.data.json,
				});
				if (res.data.data.title.length > 12) {
					that.setData({
						title: res.data.data.title.slice(0, 4) + '...',
					});
				} else {
					that.setData({
						title: res.data.data.title,
					});
				}
				that.post_getActivityList(res.data.data.aid);
			})
			.catch((err) => {});
	},
	// 获取数据
	post_getActivityList(id) {
		let that = this;
		app.http({
			url: app.api.post_getActivityList,
			data: {
				aid: id, limit: that.data.limit, page: that.data.page
			},
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					getActivityList:
						that.data.page == 1
							? list
							: that.data.getActivityList.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
				console.log(res);
			})
			.catch((err) => {});
	},
	lower(e) {
		console.log(e);
		let that = this;
		let control = that.data.control;
		let id = that.data.myActivityInfo.aid; //商品id
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.post_getActivityList(id);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
	// 跳到订单页面
	jump() {
		let id = this.data.myActivityInfo.oid;
		wx.navigateTo({
			url: `/pages/order/orderDetail/orderDetail?id=${id}`,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {},
	// 返回上一页
	back() {
		if (this.data.backType == 'home'){
			wx.switchTab({
				url: '/pages/tabBar/index/index',
			});
		} else {
			wx.navigateBack({
				delta: 1,
			});
		}
	},
	//转发
	onShareAppMessage: function (res) {
		let that = this;
		let invite_code = app.store.getState().infoData.invite_code;
		let uid = app.store.getState().infoData.id;
		let id = that.data.myActivityInfo.aid;
		// let id = that.data.myActivityInfo.goods_id;
		let bg = that.data.myActivityInfo.share;
		if (res.from === 'button') {
			return {
				title: that.data.myActivityInfo.title,
				path: `/pages/lstOfGrpDetails/lstOfGrpDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}`,
				// path: `/pages/myOpeningDetails/myOpeningDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}&log_id=${that.data.myActivityInfo.log_id}`,
				// path: `/pages/productDetails/productDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}&log_id=${that.data.myActivityInfo.log_id}}&typeTwo=1`,
				imageUrl: bg,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
	// 动态获取高度
	getHeightDynamically() {
		let that = this;
		// 先取出页面高度 windowHeight
		wx.getSystemInfo({
			success: function (res) {
				that.setData({
					windowHeight: res.windowHeight,
				});
			},
		});
		// 然后取出navbar和header的高度
		// 根据文档，先创建一个SelectorQuery对象实例
		let query = wx.createSelectorQuery().in(this);
		// 然后逐个取出navbar和header的节点信息
		// 选择器的语法与jQuery语法相同
		query.select('#head').boundingClientRect();
		query.select('#contentArea').boundingClientRect();
		query.select('#visitors').boundingClientRect();

		// 执行上面所指定的请求，结果会按照顺序存放于一个数组中，在callback的第一个参数中返回
		query.exec((res) => {
			console.log(res);
			// 分别取出navbar和header的高度
			let headHeight = res[0].height;
			let myBalHeight = res[1].height;
			let visiHeight = res[2].height;

			// 然后就是做个减法
			let scrollViewHeight =
				that.data.windowHeight -
				headHeight -
				myBalHeight -
				visiHeight -
				86 -
				19;

			// 算出来之后存到data对象里面
			that.setData({
				scrollViewHeight: scrollViewHeight,
			});
		});
	},
});
