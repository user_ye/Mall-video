/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 13:55:03
 * @LastEditTime: 2020-08-04 19:20:53
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\meowCurrExchange\meowCurrExchange.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: { miao: 100, value: '' },

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.get_my_myWallet();
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {},
	// 获取喵呗
	get_my_myWallet() {
		let that = this;
		app.http({
			url: app.api.get_my_myWallet,
			data: {},
		})
			.then((res) => {
				that.setData({
					miao: res.data.data.miao - 0,
				});
			})
			.catch((err) => {});
	},
	// 全部兑换
	redeemAll() {
		let that = this;
		that.setData({
			value: that.data.miao,
		});
	},
	inputValue(e) {
		this.setData({
			value: e.detail.value,
		});
	},
	// 兑换按钮
	btn() {
		let that = this;
		let miao = that.data.value;
		if (miao % 100 == 0) {
			// console.log('是');
		} else {
			// console.log('否');
		}
		app.http({
			url: app.api.post_branch_exchangeMiao,
			data: { miao: miao },
		})
			.then((res) => {
				wx.showToast({
					title: '兑换成功!',
					icon: 'none',
					duration: 1500,
				});
				that.get_my_myWallet();
			})
			.catch((err) => {});
	},
	// 兑现说明
	explain() {
		let key = 'meow_coin_exchange';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},
});
