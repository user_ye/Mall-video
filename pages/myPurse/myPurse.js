/*
 * @Author: 红尘痴子
 * @Date: 2020-07-08 14:25:34
 * @LastEditTime: 2020-09-04 15:41:41
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\myPurse\myPurse.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区

		requestMyWallet: {}, // 我的钱包
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		// 获取余额和喵呗
		let that = this;
		that.get_my_myWallet();
	},

	// 请求我的钱包数据
	get_my_myWallet() {
		app.http({
			url: app.api.get_my_myWallet,
			data: {},
		})
			.then((res) => {
				this.setData({
					requestMyWallet: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 跳到余额页面
	bal() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/bal/bal?id=${index}`,
		});
	},
	// 跳转喵呗
	clickMyWallet() {
		wx.navigateTo({
			url: '/pages/catCoin/catCoin',
		});
	},

	// 点击权限按钮
	clickJurisdictionBtn(e) {
		let index = e.currentTarget.dataset.index;

		// 0 视频收益，1 拼团奖励，2 分享奖励，3 团队奖励
		if (index == 0) {
			wx.navigateTo({
				url: '/pages/videoRev/videoRev',
			});
		} else if (index == 1) {
			wx.navigateTo({
				url: '/pages/grpReward/grpReward',
			});
		} else if (index == 2) {
			wx.navigateTo({
				url: '/pages/shareRewards/shareRewards',
			});
		} else if (index == 3) {
			wx.navigateTo({
				url: '/pages/teamRewards/teamRewards',
			});
		} else if (index == 4) {
			wx.navigateTo({
				url: '/pages/liveCommission/liveCommission',
			});
		}
	},

	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
