/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 11:40:07
 * @LastEditTime: 2020-08-05 11:12:19
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\withdrawalsRec\withdrawalsRec.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    status: app.globalData.statusHeight, //头部占位高度
    navHeight: app.globalData.navHeight, //头部内容区
    active: 0,
    vanTitle: [
      {id: 1, title: '全部'},
      {id: 2, title: '申请中'},
      {id: 3, title: '待打款'},
      {id: 4, title: '已打款'},
      {id: 5, title: '已驳回'},
    ],
    pickerDateValue: '', // 顶部时间选择器的值
    paginationList: [], // 分页列表数据
    pageNum: 1, // 第几页
    limit: 10, // 每页数量
    isMax: false, // 是否已显示最大数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;

    let myDate = new Date();
    let realYear = myDate.getFullYear();
    let myMonth =
      myDate.getMonth() + 1 < 10
        ? '0' + (myDate.getMonth() + 1)
        : myDate.getMonth() + 1;
    this.setData({
      pickerDateValue: realYear + '-' + myMonth,
    });

    this.requestList(0);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  // 请求列表
  requestList() {
    let that = this;
    let paginationList = this.data.paginationList; // 分页列表数据
    let pageNum = this.data.pageNum; // 第几页
    let limit = this.data.limit; // 每页数量
    let isMax = this.data.isMax; // 是否已显示最大数量

    let typeNumber = this.data.active;
    // console.log('type---',type)
    if (typeNumber == 4) {
      typeNumber = 5;
    } else {
      typeNumber = typeNumber;
    }

    if (!isMax) {
      app.http({
        url: app.api.my_withdrawalList,
        data: {
          time: this.data.pickerDateValue,
          type: typeNumber,
          page: pageNum, // 第几页
          limit: limit, // 每页数量
        },
      })
        .then((res) => {
          // console.log('res--------',res);
          // console.log('res--------',res.data.data.data);

          let listData = res.data.data.data; // 请求到的列表数据
          paginationList.push(...listData);
          that.setData({
            paginationList: paginationList,
          });

          let total = res.data.data.total; // 列表的总数量
          if (paginationList.length >= total) {
            isMax = true;
          }

          pageNum += 1;
          that.setData({
            paginationList: paginationList,
            isMax: isMax,
            pageNum: pageNum,
          });
        })
        .catch((err) => {
        });
    }
  },

  // 失败时的状态提示框
  statusTipsLayer(e) {
    let faildetail = e.currentTarget.dataset.faildetail;
    // console.log('typeindex---',faildetail);
    wx.showModal({
      content: faildetail,
      showCancel: false,
      success(res) {
        if (res.confirm) {
        } else if (res.cancel) {}
      },
    });
  },

  // 选择时间
  bindDateChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      pickerDateValue: e.detail.value,
    });
    this.setData({
      pageNum: 1,
      paginationList: [],
      isMax: false,
    });
    this.requestList(this.data.active);
    // console.log('this.data.active---',this.data.active)
  },
  //切换标签
  onChange(event) {
    let that = this;
    this.setData({
      pageNum: 1,
      paginationList: [],
      isMax: false,
      active: event.detail.name,
    });
    that.requestList(event.detail.name);
  },
  // 滚动到顶部
  upper(e) {
  },
  //滚动到底部
  lower(event) {
    // console.log(e.detail.direction);
    this.requestList(this.data.active);
  },
});
