/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 18:08:23
 * @LastEditTime: 2020-08-04 19:18:33
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\brandIntroduction\brandIntroduction.js
 * @https://www.jiangcan95.com/
 */

// pages/brandIntroduction/brandIntroduction.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		limit: 10, //分页默认值
		page: 1, //分页默认值
		control: false, //是否还有下一页
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.setData({
			id: options.id,
		});
		that.get_mall_getBrand(options.id);
		that.get_mall_malls(options.id);
	},
	// 获取数据
	// 详情
	get_mall_getBrand(id) {
		let that = this;
		app.http({
			url: app.api.get_mall_getBrand,
			data: {
				id: id,
			},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					getBrand: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 列表
	get_mall_malls(id) {
		let that = this;
		app.http({
			url: app.api.get_mall_malls,
			data: {
				bid: id,
				limit: that.data.limit,
				page: that.data.page,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 分享按钮
	shareBtn(e) {
		let that = this;
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/productDetails/productDetails?id=${id}&type=0`,
		});
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.get_mall_malls(that.data.id);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
