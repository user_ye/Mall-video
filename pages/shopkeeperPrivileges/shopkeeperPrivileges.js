/*
 * @Author: 红尘痴子
 * @Date: 2020-07-08 10:46:39
 * @LastEditTime: 2020-07-29 11:03:06
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\shopkeeperPrivileges\shopkeeperPrivileges.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		shopkeeperPrivilegeBg:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Ownership%20rights/%E6%8E%8C%E6%9F%9C%E7%AD%89%E7%BA%A7%E8%83%8C%E6%99%AF%402x.png', //掌柜特权背景
		width: 80, //进度条
		zuoimg:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Ownership%20rights/%E7%BC%96%E7%BB%84%204%402x.png', //标题左边菱形
		youimg:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Ownership%20rights/%E7%BC%96%E7%BB%84%205%402x.png', //标题右边菱形
		show: false, //提示弹窗判断
		text:
			'弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容弹窗内容',
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.post_index_getLevelPower();
	},
	// 获取数据
	post_index_getLevelPower() {
		let that = this;
		app.http({
			url: app.api.post_index_getLevelPower,
			data: {},
		})
			.then((res) => {
				let num =
					Number(res.data.data.consume) + Number(res.data.data.teams);
				that.setData({
					LevelPower: res.data.data,
					num: num.toFixed(2),
					width: Math.round((num / res.data.data.level) * 100),
				});
			})
			.catch((err) => {});
	},
	// 查看掌柜权益
	viewTheOwnersRights() {
		let that = this;
		wx.navigateTo({
			url: '/pages/ownershipRights/ownershipRights',
		});
	},
	// 跳到说明页面
	explanation() {
		let key = 'hierarchical_rules';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},
	// 弹窗
	enterInvitationCdPopup(e) {
		let that = this;
		let key = '';
		if (e.currentTarget.dataset.type == 1) {
			key = 'personal';
		} else {
			key = 'team_consumption';
		}
		app.http({
			url: app.api.ruleSpecifications,
			data: {
				key: key,
			},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					key: res.data.data.val,
				});
			})
			.catch((err) => {});
		that.setData({
			show: !that.data.show,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
