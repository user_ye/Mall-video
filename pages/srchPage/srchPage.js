/*
 * @Author: 红尘痴子
 * @Date: 2020-07-02 09:21:10
 * @LastEditTime: 2020-08-04 19:35:01
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\srchPage\srchPage.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		invitationCdInptBox: '', //监听搜索input框
		active: 0,
		// 分类标签
		vanTan: [
			{
				id: 1,
				text: '商品',
			},
			// {
			// 	id: 2,
			// 	text: '视频',
			// },
			// {
			// 	id: 3,
			// 	text: '直播',
			// },
			// {
			// 	id: 4,
			// 	text: '活动',
			// },
		],
		videoList: [],
		// 精品活动数据
		boutiqueEvntData: [],
		name: '',
		limit: 10, //分页默认值
		page: 1, //分页默认值
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.setData({ active: parseInt(options.index) });
		that.get_index_videos('');
		that.get_mall_malls('');
		that.get_mall_list('');
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
	},
	// 搜索按钮
	srchFor() {
		let that = this;
		that.setData({
			name: that.data.invitationCdInptBox,
		});
		if (that.data.active == 0) {
			that.get_mall_malls(that.data.invitationCdInptBox);
		} else if (that.data.active == 3) {
			that.get_mall_list(that.data.invitationCdInptBox);
		} else {
			that.get_index_videos(that.data.invitationCdInptBox);
		}
	},
	// 分类标签切换
	onChange(event) {
		let that = this;
		that.setData({
			active: event.detail.name,
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
	},
	// 监听input输入
	invitationCd(e) {
		let that = this;
		that.setData({
			invitationCdInptBox: e.detail.value,
		});
	},
	// 搜索视频列表
	get_index_videos(name) {
		let that = this;
		app.http({
			url: app.api.get_index_videos,
			data: {
				type: 0,
				limit: that.data.limit,
				page: that.data.page,
				title: name,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					videoList:
						that.data.page == 1
							? list
							: that.data.videoList.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},

	// 获取商品列表
	get_mall_malls(name) {
		let that = this;
		app.http({
			url: app.api.get_mall_malls,
			data: {
				type: 0,
				limit: that.data.limit,
				page: that.data.page,
				name: name,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					boutiqueEvntData:
						that.data.page == 1
							? list
							: that.data.boutiqueEvntData.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 获取活动列表
	get_mall_list(name) {
		let that = this;
		app.http({
			url: app.api.get_mall_list,
			data: {
				limit: that.data.limit,
				page: that.data.page,
				name: name,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					get_mall_list:
						that.data.page == 1
							? list
							: that.data.get_mall_list.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 跳到拼团列表
	skpToGrpLst(e) {
		let that = this;
		let id = e.currentTarget.dataset.item.id;
		wx.navigateTo({
			url: `/pages/lstOfGrpDetails/lstOfGrpDetails?id=${id}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			if (that.data.active == 0) {
				that.get_mall_malls(that.data.name);
			} else if (that.data.active == 3) {
				that.get_mall_list(that.data.name);
			} else {
				that.get_index_videos(that.data.name);
			}
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
