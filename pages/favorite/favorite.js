/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 18:08:23
 * @LastEditTime: 2020-08-04 19:19:37
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\favorite\favorite.js
 * @https://www.jiangcan95.com/
 */

// pages/favorite/favorite.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.post_index_myCollect();
	},
	//获取收藏列表
	post_index_myCollect() {
		let that = this;
		app.http({
			url: app.api.post_index_myCollect,
			data: { limit: that.data.limit, page: that.data.page },
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 删除按钮
	delBtn(e) {
		let that = this;
		app.http({
			url: app.api.post_index_delCollect,
			data: {
				gid: e.currentTarget.dataset.id,
			},
		})
			.then((res) => {
				that.post_index_myCollect();
			})
			.catch((err) => {});
	},
	// 跳到详情页
	jump(e) {
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/productDetails/productDetails?id=${id}`,
		});
	},
	// 分享按钮
	shareBtn() {
		wx.navigateTo({
			url: '/pages/InviteFriends/InviteFriends',
		});
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.post_index_myCollect();
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
