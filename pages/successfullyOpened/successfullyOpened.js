/*
 * @Author: 红尘痴子
 * @Date: 2020-07-06 16:05:28
 * @LastEditTime: 2020-07-31 11:27:32
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\successfullyOpened\successfullyOpened.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		imgUrl: '/resource/image/tx1.gif',
		timeData: {},
		show: false,

		myActivityInfo:{}, // 请求到的活动数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		let paramId = options.log_id ? options.log_id : options.id
		that.post_branch_myActivityInfo(paramId);
		that.setData({
			options: options,
		});
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline'],
		});
	},
	// 倒计时
	onChange(e) {
		this.setData({
			timeData: e.detail,
		});
	},
	// 跳到合成海报页面
	generatePoster() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/syntheticPoster/syntheticPoster?id=${index}`,
		});
	},
	// 获取数据
	post_branch_myActivityInfo(id) {
		let that = this;
		app.http({
			url: app.api.post_branch_myActivityInfo,
			data: { log_id: id },
		})
			.then((res) => {
				that.setData({
					myActivityInfo: res.data.data,
					users: res.data.data.users.reverse(),
					good_attr: res.data.data.json,
				});
				if (res.data.data.title.length > 12) {
					that.setData({
						title: res.data.data.title.slice(0, 4) + '...',
					});
				} else {
					that.setData({
						title: res.data.data.title,
					});
				}
			})
			.catch((err) => {});
	},
	// 跳到订单详情
	jump() {
		let id = this.data.myActivityInfo.oid;
		wx.navigateTo({
			url: `/pages/order/orderDetail/orderDetail?id=${id}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
	//转发
	onShareAppMessage: function (res) {
		let that = this;
		let invite_code = app.store.getState().infoData.invite_code;
		let uid = app.store.getState().infoData.id;
		let id = that.data.myActivityInfo.aid;
		// let id = that.data.myActivityInfo.goods_id;
		let bg = that.data.myActivityInfo.share;
		if (res.from === 'button') {
			return {
				title: that.data.myActivityInfo.title,
				path: `/pages/lstOfGrpDetails/lstOfGrpDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}`,
				// path: `/pages/myOpeningDetails/myOpeningDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}&log_id=${that.data.myActivityInfo.log_id}`,
				// path: `/pages/productDetails/productDetails?code=${invite_code}&uid=${uid}&id=${id}&activity_id=${id}&log_id=${that.data.myActivityInfo.log_id}}&typeTwo=1`,
				imageUrl: bg,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
});
