/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 14:49:03
 * @LastEditTime: 2020-08-04 19:20:01
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\historicalOverview\historicalOverview.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		vanTitle1: [
			{ type: 1, title: '余额提现' },
			{ type: 2, title: '余额提现' },
			{ type: 3, title: '余额提现' },
			{ type: 4, title: '余额提现' },
		],

		pickerDateValue: '', // 顶部时间选择器的值
		requestList: {}, // 请求到的列表数据

		typeNumber: null, // 上个页面传过来的类型
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		// 	type：1-视频任务 2-拼团奖励 3-团队奖励 4-分享奖励 默认获取全部类型
		this.setData({
			typeNumber: options.type,
		});
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;

		let myDate = new Date();
		let realYear = myDate.getFullYear();
		this.setData({
			pickerDateValue: realYear,
		});

		this.requestList();
	},

	// 请求列表
	requestList(type) {
		let that = this;
		app.http({
			url: app.api.mb_historyList,
			data: {
				time: this.data.pickerDateValue,
				type: this.data.typeNumber,
			},
		})
			.then((res) => {
				this.setData({
					requestList: res.data.data,
				});
			})
			.catch((err) => {});
	},

	// 选择时间
	bindDateChange: function (e) {
		this.setData({
			pickerDateValue: e.detail.value,
		});
		this.requestList(this.data.active);
	},
	// 滚动到顶部
	upper(e) {},
	//滚动到底部
	lower(e) {},
});
