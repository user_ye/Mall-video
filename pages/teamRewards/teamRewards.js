/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 17:11:01
 * @LastEditTime: 2020-09-08 16:51:58
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\teamRewards\teamRewards.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		balPic:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E5%96%B5%E5%B8%81%402x.png', //余额图片
		active: 0,

		requestData: {},
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		that.post_my_getBalances();
	},

	// 点击我的客户
	clickMyClient() {
		wx.navigateTo({
			url: `/pages/myClient/myClient`,
		});
	},

	// 请求数据
	post_my_getBalances() {
		let that = this;
		app.http({
			url: app.api.wallet_teamReward,
			data: {},
		})
			.then((res) => {
				that.setData({
					requestData: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 跳到历史概况页面
	historicalOverview() {
		let type = 3;
		wx.navigateTo({
			url: `/pages/historicalOverview/historicalOverview?type=${type}`,
		});
	},
	// 跳到说明页面
	explanation() {
		let key = 'team';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
