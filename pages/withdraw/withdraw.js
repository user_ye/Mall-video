let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		img:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E4%BD%99%E9%A2%9D%402x.png',
		input: '', //金额输入框
		is100: false, // 是否为50的倍数

		requestData: {}, // 请求到的数据
		selectBankCardData: '', // 选中的银行卡数据
		isSelectBankCardPage: false, // 是否从选择银行卡页面跳转过来
		withdrawalType: 2, // 提现类型：1银行卡，2微信零钱，默认微信零钱
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		// 如果不是从选择银行卡页面跳转过来，则请求数据
		if (!this.data.isSelectBankCardPage) {
			console.log("如果不是从选择银行卡页面跳转过来，则请求数据---");
			this.requestList();
		}
	},

	// 选择银行卡列表
	requestList() {
		let that = this;
		app.http({
			url: app.api.withdrawal_getMyCard,
			data: {},
		})
			.then((res) => {
				let requestData = res.data.data;

				that.setData({
					requestData: requestData,
				});

				requestData.list.map((mapRes) => {
					if (mapRes.is_default == true) {
						that.setData({
							selectBankCardData: mapRes,
						});
					}
				});
			})
			.catch((err) => {});
	},

	// 监听输入框
	bindInput(e) {
		let that = this;
		// 计算是否为50的倍数
		let value = e.detail.value;
		// console.log('num-=num%50;',value % 50);
		if (value % 50 == 0 && value != 0) {
			this.setData({
				is100: true,
				input: value,
			});
		} else {
			this.setData({
				is100: false,
			});
		}
	},
	// 全部提现按钮
	withdrawAll(e) {
		let that = this;
		let allwithdrawal = e.currentTarget.dataset.allwithdrawal;
		// console.log('e----',e.currentTarget.dataset.allwithdrawal)
		that.setData({
			input: allwithdrawal - 0,
		});
		if (allwithdrawal % 50 == 0 && allwithdrawal != 0) {
			this.setData({
				is100: true,
			});
		} else {
			this.setData({
				is100: false,
			});
		}
	},
	// 申请提现按钮
	btn(e) {
		// console.log('e---',e)
		let that = this;

		if (!that.data.is100) {
			wx.showToast({
				title: '请输入金额为50的倍数',
				icon: 'none',
				duration: 2000,
			});
			return;
		}

		console.log('that.data.selectBankCardData---',that.data.selectBankCardData)
		app.http({
			url: app.api.branch_addWithdrawal,
			data: {
				type: that.data.withdrawalType,  // 提现类型：1银行卡，2微信零钱
				amount: that.data.input,
				bank_id: that.data.selectBankCardData ? that.data.selectBankCardData.id : '',
			},
		})
			.then((res) => {
				// console.log('------------666666666666----------')

				if (res.data.data == true) {
					wx.showToast({
						title: res.data.msg,
						icon: 'success',
						duration: 1500,
					});

					let newBalance =
						this.data.requestData.balance * 1 - that.data.input * 1;
					let toFixedTwo = parseFloat(newBalance).toFixed(2);
					this.setData({
						['requestData.balance']: toFixedTwo,
						input: '',
					});
				}
			})
			.catch((err) => {
				// console.log('err------',err.data.msg);
				wx.showModal({
					content: err.data.msg,
					showCancel: false,
					success(res) {
						if (res.confirm) {
							// console.log('用户点击确定')
						} else if (res.cancel) {
							// console.log('用户点击取消')
						}
					},
				});
			});
	},

	// 提现说明
	explain() {
		let key = 'withdraw';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},

	// // 添加账户
	// addAccount(){
	//   wx.navigateTo({
	//     url: `/pages/verified/verified`,
	//   });
	// },
	// 选择银行卡
	selectAccount() {
		wx.navigateTo({
			url: '/pages/chooseBnkCrd/chooseBnkCrd?bankCardList=' + JSON.stringify(this.data.requestData) + '&withdrawalType=' + this.data.withdrawalType
		});
	},
});
