/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 17:50:16
 * @LastEditTime: 2020-08-05 11:11:45
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\videoRev\videoRev.js
 * @https://www.jiangcan95.com/
 */

import store from "../../resource/lib/mystore";

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		balPic:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E5%96%B5%E5%B8%81%402x.png', //余额图片
		active: 0,

		infoData:{}, // 用户信息
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		// that.post_my_getBalances(1); //1为收入 2为支出，默认返回收入
		that.post_index_videoReward();
		that.updateUserInfo();

		let infoData = wx.getStorageSync('infoData')
		this.setData({
			infoData:infoData
		})
	},
	// 获取余额列表
	post_my_getBalances(type) {
		let that = this;
		app.http({
			url: app.api.post_my_getBalances,
			data: { type: type },
		})
			.then((res) => {
				that.setData({
					getBalances: res.data.data.wallet,
				});
			})
			.catch((err) => {});
	},

	// 刷新用户信息
	updateUserInfo(){
		let that = this;
		wx.login({
			success(res) {
				let token = wx.getStorageSync('token');
				let header;
				if (token) {
					header = {
						Authorization: token,
					};
				} else {
					header = {};
				}
				if (res.code) {
					app.http({
						url: '/user/login/wxLogin',
						data: {
							code: res.code,
						},
						header: header,
					})
						.then((res) => {
							that.setData({
								infoData: res.data.data
							})
							store.setState({
								infoData: res.data.data,
								token: res.data.data.token,
							});
							if (res.data.data.phone == '0') {
								store.setState({ loginType: 1 });
							} else {
								store.setState({ loginType: 0 });
							}
							wx.setStorageSync('infoData', res.data.data);
							wx.setStorageSync('token', res.data.data.token);
							that.globalData.checkLogin = true;

							//由于这里是网络请求，可能会在 Page.onLoad 之后才返回
							// 所以此处加入 callback 以防止这种情况
							if (that.checkLoginReadyCallback) {
								that.checkLoginReadyCallback(res);
							}
						})
						.catch((err) => {});
				}
			},
			fail: (err) => {},
		});
	},

	// 获取视频收益
	post_index_videoReward() {
		let that = this;
		app.http({
			url: app.api.post_index_videoReward,
			data: {},
		})
			.then((res) => {
				that.setData({
					videoReward: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 跳到历史概况页面
	historicalOverview() {
		let type = 1;
		wx.navigateTo({
			url: `/pages/historicalOverview/historicalOverview?type=${type}`,
		});
	},
	// 跳到说明页面
	explanation() {
		let key = 'video';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},
	// 去赚喵呗
	jump(e) {
		// console.log('去赚喵呗---')
		let index = e.currentTarget.dataset.index;

		console.log("this.data.infoData---", this.data.infoData);
		// if (this.data.infoData.is_proceeds == 0 && index == 0){
		if (this.data.infoData.is_proceeds == 0){ // 如果是普通用户

			// 传空code
			wx.navigateTo({
				url: '/pages/rights-and-interests/opening-privilege/opening-privilege?isKCode=' + true
			})
		} else if (index == 0) {
			wx.switchTab({
				url: '/pages/tabBar/index/index',
			});
		} else {
			// wx.navigateBack({
			// 	delta: 1
			// })
			wx.switchTab({
				url: '/pages/tabBar/index/index',
			});
		}
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
