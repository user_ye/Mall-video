
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    status: app.globalData.statusHeight, //头部占位高度
    navHeight: app.globalData.navHeight, //头部内容区
    invitationCdInptBox: '', //监听搜索input框

    paginationList:[], // 分页列表数据
    pageNum: 1, // 第几页
    limit: 10, // 每页数量
    isMax:false, // 是否已显示最大数量

    goodsType: 0, // 商品类型，welfare 福利专区，new 新品专区
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log('options--222--',options.goodsType);
    if (options.goodsType){
      this.setData({
        goodsType: options.goodsType
      })

    }

    // 商品类型，welfare 福利专区，new 新品专区
    if (options.goodsType == 'new'){
      wx.setNavigationBarTitle({
        title: '新品专区',
      });
    } else if (options.goodsType == 'welfare'){
      wx.setNavigationBarTitle({
        title: '福利专区',
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    that.get_mall_malls({
      name: that.data.invitationCdInptBox
    });
  },
  // 搜索按钮
  srchFor() {
    let that = this;
    that.setData({
      paginationList:[], // 分页列表数据
      pageNum: 1, // 第几页
      limit: 10, // 每页数量
      isMax:false, // 是否已显示最大数量
    })
    that.get_mall_malls({
      name: that.data.invitationCdInptBox
    });
  },

  // 监听input输入
  invitationCd(e) {
    let that = this;
    that.setData({
      invitationCdInptBox: e.detail.value,
    });
  },

  // 获取商品列表
  get_mall_malls(param) {
    // console.log('param---',param)
    let that = this;
    let paginationList = this.data.paginationList; // 分页列表数据
    let pageNum = this.data.pageNum; // 第几页
    let limit = this.data.limit;  // 每页数量
    let isMax = this.data.isMax; // 是否已显示最大数量

    let requestParam = {
      type: this.data.goodsType,
      limit: limit,
      page: pageNum,
      name: param.name ? param.name : '',
    };

    // 商品类型，welfare 福利专区，new 新品专区
    if (this.data.goodsType == 'welfare'){
      requestParam.is_welfare = 1;
    } else if (this.data.goodsType == 'new'){
      requestParam.is_new = 1;
    }


    if (!isMax) {
      app.http({
        url: app.api.get_mall_malls,
        data: requestParam,
        //method: 'get',
      })
        .then((res) => {
          let listData = res.data.data.data; // 请求到的列表数据
          paginationList.push(...listData);
          let total = res.data.data.total; // 列表的总数量
          if (paginationList.length >= total) {
            isMax = true;
            // console.log('---------------最大啦---------------')
          }
          pageNum += 1;
          that.setData({
            paginationList: paginationList,
            isMax: isMax,
            pageNum: pageNum
          })
        })
        .catch((err) => {});
    }
  },

  // 跳到拼团列表
  skpToGrpLst(e) {
    let that = this;
    let id = e.currentTarget.dataset.item.id;
    wx.navigateTo({
      url: `/pages/lstOfGrpDetails/lstOfGrpDetails?id=${id}`,
    });
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    this.get_mall_malls(this.data.invitationCdInptBox);
    // let control = that.data.control;
    // if (control) {
    //   that.setData({
    //     page: that.data.page + 1,
    //   });
    //   if (that.data.active == 0) {
    //     that.get_mall_malls(that.data.name);
    //   }
    // } else {
    //   wx.showToast({
    //     title: '没有更多了',
    //     icon: 'none',
    //   });
    // }
  },
});
