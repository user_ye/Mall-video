/*
 * @Author: 红尘痴子
 * @Date: 2020-07-11 10:07:35
 * @LastEditTime: 2020-09-04 17:16:21
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\myClient\myClient.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		balPic:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E5%96%B5%E5%B8%81%402x.png', //余额图片
		srchFor: '', //搜索
		active: 0,
		vanTitle: [
			{ id: 0, title: '全部' },
			{ id: 1, title: '普通会员' },
			{ id: 3, title: '资深掌柜' },
			{ id: 4, title: '精英掌柜' },
			{ id: 5, title: '超级掌柜' },
		],

		requestData: {}, // 请求的数据

		paginationList: [],
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.getHeightDynamically();
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		that.requestList(0);
	},
	// 请求列表
	requestList(type, name) {
		let that = this;
		app.http({
			url: app.api.my_myCustomer,
			data: {
				level: type,
				limit: that.data.limit,
				page: that.data.page,
				nickname: name || '', //非必填搜索
			},
		})
			.then((res) => {
				let list = res.data.data.list.data;
				that.setData({
					paginationList:
						that.data.page == 1
							? list
							: that.data.paginationList.concat(list),
					control:
						res.data.data.list.current_page <
						res.data.data.list.last_page,
					requestData: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 搜索
	srchFor(e) {
		let that = this;
		that.setData({
			nickname: e.detail.value,
		});
		this.requestList(this.data.active, e.detail.value);
	},
	// 滚动到顶部
	upper(e) {},
	//滚动到底部
	lower(e) {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.requestList(that.data.active);
		} else {
			that.setData({ isMax: true });
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
	//切换标签
	onChange(event) {
		let that = this;
		this.setData({
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
			limit: 10, //分页默认值
			page: 1, //分页默认值
			paginationList: [],
			isMax: false,
			active: this.data.vanTitle[event.detail.name].id,
		});
		console.log("this.data.vanTitle[index]---", this.data.vanTitle[event.detail.name].id);
		console.log('event.detail.name---',event.detail.name)
		that.requestList(this.data.vanTitle[event.detail.name].id, that.data.nickname);
	},
	// 跳到历史概况页面
	historicalOverview() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/historicalOverview/historicalOverview?id=${index}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
	// 动态获取高度
	getHeightDynamically() {
		let that = this;
		// 先取出页面高度 windowHeight
		wx.getSystemInfo({
			success: function (res) {
				that.setData({
					windowHeight: res.windowHeight,
				});
			},
		});
		// 然后取出navbar和header的高度
		// 根据文档，先创建一个SelectorQuery对象实例
		let query = wx.createSelectorQuery().in(this);
		// 然后逐个取出navbar和header的节点信息
		// 选择器的语法与jQuery语法相同
		query.select('#head').boundingClientRect();
		query.select('#myBal').boundingClientRect();

		// 执行上面所指定的请求，结果会按照顺序存放于一个数组中，在callback的第一个参数中返回
		query.exec((res) => {
			// 分别取出navbar和header的高度
			let headHeight = res[0].height;
			let myBalHeight = res[1].height;

			// 然后就是做个减法
			let scrollViewHeight =
				that.data.windowHeight -
				headHeight -
				myBalHeight +
				that.data.status;

			// 算出来之后存到data对象里面
			that.setData({
				scrollViewHeight: scrollViewHeight,
			});
		});
	},
});
