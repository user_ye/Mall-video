/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 09:10:29
 * @LastEditTime: 2020-08-04 19:38:17
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\verified\verified.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
var util = require('../../utils/util');
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		nameValue: '', //姓名
		idCrdValue: '', //身份证

		bnkCrdValue: '', // 银行卡号
		bank_name: '', // 银行卡名称
		bankCardType: '', // 银行卡类型
		bankCardImg: '', // 银行卡icon

		phnReservedValue: '', //手机号

		bankCardData: {}, // 上一页传过来的银行卡数据

		token: '',
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		this.setData({
			bankCardData: JSON.parse(options.bankCardList),
		});
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		let token = wx.getStorageSync('token');
		this.setData({
			token: token,
		});
	},
	// 监听姓名输入
	nameInput(e) {
		let that = this;
		that.setData({
			nameValue: e.detail.value,
		});
	},
	// 监听身份证输入
	idCrdInput(e) {
		let that = this;
		that.setData({
			idCrdValue: e.detail.value,
		});
	},
	// 监听银行卡输入
	bnkCrdInput(e) {
		let that = this;
		that.setData({
			bnkCrdValue: e.detail.value,
		});
	},

	// 监听手机号输入
	phnReservedInput(e) {
		let that = this;
		that.setData({
			phnReservedValue: e.detail.value,
		});
	},

	// 银行卡输入框失去焦点
	blurBankNumber() {
		let that = this;

		app.http({
			url: app.api.branch_checkCard,
			data: {
				card: this.data.bnkCrdValue,
			},
			method: 'POST',
		})
			.then((res) => {
				let resData = res.data.data;

				that.setData({
					bnkCrdValue: resData.card,
					bank_name: resData.bank_name,
					bankCardType: resData.type,
				});
			})
			.catch((err) => {});
	},

	// 拍照上传银行卡
	photoUpload(e) {
		let that = this;

		// 选择图片
		wx.chooseImage({
			count: 1,
			success: function (res) {
				wx.showLoading({
					title: '识别中',
				});

				let qm = app.qm({ type: 'image' });
				wx.uploadFile({
					url: app.api_url + app.api.orcBank,
					filePath: res.tempFilePaths[0],
					name: 'img',
					header: {
						Authorization: that.data.token,
						'content-type': 'application/x-www-form-urlencoded',
					},
					formData: qm,
					success: (res) => {
						wx.hideLoading();

						let codeJson = JSON.parse(res.data);
						if (codeJson.code == 400) {
							wx.showModal({
								title: '提示',
								content: codeJson.msg,
								showCancel: false, // 是否显示取消按钮，默认为 true
								success(res) {
									if (res.confirm) {
									} else if (res.cancel) {
									}
								},
							});
						} else {
							let resData = JSON.parse(res.data);

							if (resData.code == 200) {
								wx.showToast({
									title: '识别成功',
									icon: 'success',
									duration: 1000,
								});
							}

							that.setData({
								bnkCrdValue: resData.data.card,
								bank_name: resData.data.bank_name,
								bankCardType: resData.data.type,
								bankCardImg: resData.data.img,
							});
						}
					},
					fail: (err) => {
						wx.showModal({
							title: '提示',
							content: err.msg,
							showCancel: false, // 是否显示取消按钮，默认为 true
							success(res) {
								if (res.confirm) {
								} else if (res.cancel) {
								}
							},
						});
					},
				});
			},
			fail: function (err) {
				console.error(err);
			},
		});
	},

	// 确认
	btn() {
		let that = this;

		let pages = getCurrentPages(); //获取所有页面
		let currentPage = null; //当前页面
		let prevPage = null; //上一个页面

		if (pages.length >= 2) {
			currentPage = pages[pages.length - 1]; //获取当前页面，将其赋值
			prevPage = pages[pages.length - 2]; //获取上一个页面，将其赋值
		}

		// 判断身份证
		if (!util.getBirthAndSex(that.data.idCrdValue)) {
		} else if (!util.verifyMobilePhone(that.data.phnReservedValue)) {
			return true;
		} else if (this.data.nameValue == '') {
			wx.showToast({
				title: '请输入真实姓名',
				icon: 'none',
				duration: 1500,
			});
		} else if (this.data.nameValue == '') {
			wx.showToast({
				title: '请输入银行卡号',
				icon: 'none',
				duration: 1500,
			});
		} else {
			app.http({
				url: app.api.branch_addCard,
				data: {
					identity: that.data.idCrdValue, // 身份证
					card: that.data.bnkCrdValue, // 银行卡
					name: that.data.nameValue, // 持卡人姓名
					phone: that.data.phnReservedValue, // 预留手机号
				},
				method: 'POST',
			})
				.then((res) => {
					let resData = res.data;

					if (prevPage) {
						let addCardData = resData.data;

						let newBanckCardData = this.data.bankCardData;
						newBanckCardData.list.push(addCardData);
						that.setData({
							bankCardData: newBanckCardData,
						});

						//将想要传的信息赋值给上一个页面data中的值
						prevPage.setData({
							bankCardData: newBanckCardData,
						});
					}

					wx.showToast({
						title: '添加成功',
						icon: 'true',
						duration: 1500,
					});

					wx.navigateBack({
						//返回上一个页面
						delta: 1,
					});
				})
				.catch((err) => {
					wx.showModal({
						content: err.data.msg,
						showCancel: false,
						success(res) {
							if (res.confirm) {
							} else if (res.cancel) {
							}
						},
					});
				});
		}
	},
});
