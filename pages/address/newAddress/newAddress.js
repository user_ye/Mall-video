// pages/address/newAddress/newAddress.js
let app = getApp();
import util from '../../../utils/util';
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		info: {
			username: '', // 收货人
			phone: '', // 收货人
			address: [], // 地址选择器选择的地址
			addressDetail: '', // 详细地址
			isDefaultAddress: false, // 是否默认地址
		},
		addressCode: [],
		isNewAddress: true, // 是否是新建地址状态，false为编辑地址
	},

	// 保存提交
	submitBtn() {
		let that = this;
		let info = this.data.info;
		if (info.username == '') {
			wx.showToast({
				title: '收货人姓名不能为空',
				icon: 'none',
				duration: 1500,
			});
		} else if (!util.verifyMobilePhone(info.phone)) {
			// wx.showToast({
			// 	title: '收货人手机号不能为空',
			// 	icon: 'none',
			// 	duration: 1500,
			// });
		} else if (info.address.length == 0) {
			wx.showToast({
				title: '请选择所在地',
				icon: 'none',
				duration: 1500,
			});
		} else if (info.addressDetail == '') {
			wx.showToast({
				title: '请填写详细地址',
				icon: 'none',
				duration: 1500,
			});
		} else {
			let code = that.data.addressCode;
			let id = that.data.id;
			let data = {
				name: info.username, //姓名
				tel: info.phone, //手机号
				address: info.addressDetail, //详细地址
				is_default: info.isDefaultAddress ? 1 : 0, //是否默认
				province: info.address[0],
				city: info.address[1],
				area: info.address[2],
				province_sn: code[0] ? code[0] : '',
				city_sn: code[1] ? code[1] : '',
				area_sn: code[2] ? code[2] : '',
			};
			if (id) {
				data.id = id;
			}
			app.http({
				url: app.api.post_branch_addAddress,
				data: data,
			})
				.then((res) => {
					wx.showToast({
						title: '保存成功',
						icon: 'none',
						duration: 1500,
					});
					setTimeout(() => {
						wx.navigateBack({
							delta: 1,
						});
					}, 1500);
				})
				.catch((err) => {});
		}
	},
	// 删除
	del() {
		let that = this;
		app.http({
			url: app.api.post_branch_delAddress,
			data: {
				id: that.data.id,
			},
		})
			.then((res) => {
				wx.showToast({
					title: '删除成功',
					icon: 'none',
					duration: 1000,
				});
				setTimeout(() => {
					wx.navigateBack({
						delta: 1,
					});
				}, 1000);
			})
			.catch((err) => {});
	},
	// 所在地选择器触发
	bindRegionChange(e) {
		this.setData({
			['info.address']: e.detail.value,
			addressCode: e.detail.code,
		});
	},

	// 输入用户名时触发
	userNameBindInput(e) {
		this.setData({
			['info.username']: e.detail.value,
		});
	},
	// 输入手机号时触发
	phoneBindInput(e) {
		this.setData({
			['info.phone']: e.detail.value,
		});
	},
	// 输入详细地址时触发
	addressDetailBindInput(e) {
		this.setData({
			['info.addressDetail']: e.detail.value,
		});
	},

	// 默认地址switch开关触发
	defaultAddressBindchange(e) {
		this.setData({
			['info.isDefaultAddress']: e.detail.value,
		});
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		if (options.id) {
			this.setData({
				isNewAddress: false,
				id: options.id,
			});
			wx.setNavigationBarTitle({
				title: '编辑收货地址',
			});
			this.post_branch_myAddrList(options.id);
		} else {
			this.setData({
				isNewAddress: true,
			});
			wx.setNavigationBarTitle({
				title: '新建收货地址',
			});
		}
	},
	// 获取地址信息
	post_branch_myAddrList(id) {
		let that = this;
		app.http({
			url: app.api.post_branch_myAddrList,
			data: {
				id: id,
			},
		})
			.then((res) => {
				that.setData({
					['info.username']: res.data.data[0].name, //姓名
					['info.phone']: res.data.data[0].tel, //手机号
					['info.addressDetail']: res.data.data[0].address, //详细地址
					['info.isDefaultAddress']:
						res.data.data[0].is_default == 1 ? true : false, //是否默认
					['info.address']: [
						res.data.data[0].province,
						res.data.data[0].city,
						res.data.data[0].area,
					],
				});
			})
			.catch((err) => {});
	},
});
