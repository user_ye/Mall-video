
// pages/address/addressList/addressList.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		isReturnPage:true, // 是否点击地址返回
		myAddrList:[], // 地址列表
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
	  // console.log('options----',options.isReturnPage);
    if (options.isReturnPage){
      this.setData({
        isReturnPage:options.isReturnPage
      })
    }
  },
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		that.post_branch_myAddrList();
	},
	// 获取地址列表
	post_branch_myAddrList() {
		let that = this;
		app.http({
			url: app.api.post_branch_myAddrList,
			data: {},
		})
			.then((res) => {
				let requestData = res.data.data;
				that.setData({
					myAddrList: requestData,
				});


				if (!requestData.length){
					let pages = getCurrentPages(); //获取所有页面
					// let currentPage = null; //当前页面
					let prevPage = null; //上一个页面
					if (pages.length >= 2) {
						// currentPage = pages[pages.length - 1]; //获取当前页面，将其赋值
						prevPage = pages[pages.length - 2]; //获取上一个页面，将其赋值
					}
					if (prevPage) {
						let adders = prevPage.data.adders; //这是拿到了上一页js中data里面定义的 adders
						// let myAddrList = prevPage.data.myAddrList; //这是拿到了上一页js中data里面定义的 myAddrList
						// adders = item;
						prevPage.setData({
							adders: '', //将想要传的信息赋值给上一个页面data中的值
						});
					}
				}

				// let pages = getCurrentPages(); //获取小程序页面栈
				// let beforePage = pages[pages.length - 2]; //获取上个页面的实例对象
				// if (beforePage.adders == ''){
				// 	beforePage.setData({
				// 		//直接修改上个页面的数据（可通过这种方式直接传递参数）
				// 		adders: a[0] ? a[0] : '',
				// 	});
				// }

			})
			.catch((err) => {});
	},
	// 点击地址时向上个页面传值
	clickAddressItem(e) {
    if (this.data.isReturnPage == true){
      let item = e.currentTarget.dataset.item;
      let pages = getCurrentPages(); //获取所有页面
      let currentPage = null; //当前页面
      let prevPage = null; //上一个页面
      if (pages.length >= 2) {
        currentPage = pages[pages.length - 1]; //获取当前页面，将其赋值
        prevPage = pages[pages.length - 2]; //获取上一个页面，将其赋值
      }
      if (prevPage) {
        let adders = prevPage.data.adders; //这是拿到了上一页js中data里面定义的 adders
        let myAddrList = prevPage.data.myAddrList; //这是拿到了上一页js中data里面定义的 myAddrList
        adders = item;
        prevPage.setData({
          adders: adders, //将想要传的信息赋值给上一个页面data中的值
        });
        console.log('adders---',adders)
      }
      wx.navigateBack({
        //返回上一个页面
        delta: 1,
      });
    }
	},
	// 编辑地址按钮
	editAddress(e) {
		let iseditaddress = e.target.dataset.iseditaddress;
		let id = e.currentTarget.dataset.id ? e.currentTarget.dataset.id : '';
		wx.navigateTo({
			url: `/pages/address/newAddress/newAddress?isEditAddress=${iseditaddress}&id=${id}`,
		});
	},
	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},
});
