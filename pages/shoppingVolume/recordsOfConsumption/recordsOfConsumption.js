/*
 * @Author: 红尘痴子
 * @Date: 2020-07-11 18:08:04
 * @LastEditTime: 2020-08-04 19:34:46
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\shoppingVolume\recordsOfConsumption\recordsOfConsumption.js
 * @https://www.jiangcan95.com/
 */

// pages/shoppingVolume/recordsOfConsumption/recordsOfConsumption.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
		show: false, //隐藏弹窗
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.post_my_getVolume();
	},
	// 获取优惠卷详情
	post_my_getVolume() {
		let that = this;
		app.http({
			url: app.api.post_my_getVolume,
			data: {
				limit: that.data.limit,
				page: that.data.page,
			},
		})
			.then((res) => {
				let list = res.data.data.list.data;
				that.setData({
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.list.current_page <
						res.data.data.list.last_page,
					rechargeVolume: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 购买
	btn() {
		let that = this;
		app.http({
			url: app.api.post_branch_rechargeVolume,
			data: {},
		})
			.then((res) => {
				wx.requestPayment({
					//调起支付
					//下边参数具体看微信小程序官方文档
					timeStamp: res.data.data.timeStamp,
					nonceStr: res.data.data.nonceStr,
					package: res.data.data.package,
					signType: res.data.data.signType,
					paySign: res.data.data.paySign,
					success(res) {
						// 微信支付ok 直接跳转到我的订单列表
						if (res.errMsg == 'requestPayment:ok') {
						}
					},
					fail(res) {},
				});
			})
			.catch((err) => {});
	},
	// 弹窗
	enterInvitationCdPopup() {
		let that = this;
		app.http({
			url: app.api.ruleSpecifications,
			data: {
				key: 'volume',
			},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					key: res.data.data.val,
				});
			})
			.catch((err) => {});
		that.setData({
			show: !that.data.show,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.post_my_getVolume();
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
