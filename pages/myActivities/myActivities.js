/*
 * @Author: 红尘痴子
 * @Date: 2020-07-11 14:04:28
 * @LastEditTime: 2020-08-05 17:16:27
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\myActivities\myActivities.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		vanTitle: [
			{ id: -1, title: '全部' },
			{ id: 0, title: '活动中' },
			{ id: 1, title: '已完成' },
			{ id: 2, title: '活动结束' },
		],
		active: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.post_branch_myActivity(-1);
	},
	// 获取列表
	post_branch_myActivity(astatus) {
		let that = this;
		app.http({
			url: app.api.post_branch_myActivity,
			data: { astatus: astatus },
		})
			.then((res) => {
				that.setData({
					myActivity: res.data.data.data,
				});
				// that.time();
			})
			.catch((err) => {});
	},
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {},
	// tabs标签切换
	onChange(event) {
		let that = this;
		let a = that.data.vanTitle;
		let d = event.detail.name;
		that.setData({
			myActivity: [],
			active: d,
		});
		that.post_branch_myActivity(a[d].id);
	},
	// 跳到活动详情
	activityDetails(e) {
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/myOpeningDetails/myOpeningDetails?id=${id}`,
		});
	},
	// 跳到订单详情
	jump(e) {
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/order/orderDetail/orderDetail?id=${id}`,
		});
	},
});
