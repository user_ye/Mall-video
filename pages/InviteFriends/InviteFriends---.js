/*
 * @Author: 红尘痴子
 * @Date: 2020-07-11 18:08:04
 * @LastEditTime: 2020-09-09 09:13:43
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\InviteFriends\InviteFriends.js
 * @https://www.jiangcan95.com/
 */

// pages/InviteFriends/InviteFriends.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline'],
		});
		that.setData({
			invite_code: app.store.getState().infoData.invite_code,
		});
		that.fx();
	},
	// 获取分享参数
	fx() {
		let that = this;
		let nickName = app.store.$state.infoData.nickName;
		app.http({
			url: app.api.myCardShare,
			data: {},
		}).then((res) => {
			console.log(res.data.data);
			that.setData({
				title: nickName + res.data.data.title,
				bg: res.data.data.share_img,
			});
		});
	},
	// 复制按钮
	copyBtn() {
		let that = this;
		wx.setClipboardData({
			data: that.data.invite_code,
			success(res) {
				wx.getClipboardData({
					success(res) {},
				});
			},
		});
	},

	//跳到合成海报页面
	generatePoster() {
		let that = this;
		let id = app.store.$state.infoData.id;
		wx.navigateTo({
			url: `/pages/syntheticPoster/syntheticPoster?id=${id}&type=4&page=pages/shoppingVolume/privilege/privilege`,
		});
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function (res) {
		let that = this;
		let invite_code = that.data.invite_code;
		let title = that.data.title;
		let bg = that.data.bg;
		if (res.from === 'button') {
			return {
				title: title,
				path: `/pages/shoppingVolume/privilege/privilege?code=${invite_code}&type=1`,
				imageUrl: bg,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
});
