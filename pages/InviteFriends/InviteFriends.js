/*
 * @Author: 红尘痴子
 * @Date: 2020-07-11 18:08:04
 * @LastEditTime: 2020-09-09 09:13:43
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\InviteFriends\InviteFriends.js
 * @https://www.jiangcan95.com/
 */

// pages/InviteFriends/InviteFriends.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		// getVolume:'', // 获取优惠卷详情
		share_img:'', // 分享图片
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline'],
		});
		that.setData({
			invite_code: app.store.getState().infoData.invite_code,
		});
		that.fx();
	},
	onShow(){
		// this.index_getInfo();
	},
	// 获取优惠卷详情
	// index_getInfo() {
	// 	let that = this;
	// 	app.http({
	// 		url: app.api.index_getInfo,
	// 		data: {
	// 			type: that.data.type,
	// 		},
	// 	})
	// 		.then((res) => {
	// 			that.setData({
	// 				getVolume: res.data.data,
	// 			});
	// 		})
	// 		.catch((err) => {});
	// },
	// 获取分享参数
	fx() {
		let that = this;
		let nickName = app.store.$state.infoData.nickName;
		app.http({
			url: app.api.myCardShare,
			data: {},
		}).then((res) => {
			console.log(res.data.data);
			that.setData({
				title: nickName + res.data.data.title,
				share_img: res.data.data.share_img,
			});
		});
	},
	// 复制按钮
	copyBtn() {
		let that = this;
		wx.setClipboardData({
			data: that.data.invite_code,
			success(res) {
				wx.getClipboardData({
					success(res) {},
				});
			},
		});
	},

	//跳到合成海报页面
	generatePoster() {
		let that = this;
		let id = app.store.$state.infoData.id;
		wx.navigateTo({
			url: `/pages/syntheticPoster/syntheticPoster?id=${id}&type=4&page=pages/rights-and-interests/opening-privilege/opening-privilege`,
		});
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function (res) {
		let that = this;
		let invite_code = that.data.invite_code;
		let title = that.data.title;
		let share_img = that.data.share_img;
		if (res.from === 'button') {
			return {
				title: title,
				// path: `/pages/rights-and-interests/399/index?code=${invite_code}&type=1`,
				path: `/pages/rights-and-interests/opening-privilege/opening-privilege?code=${invite_code}&type=1`,
				imageUrl: share_img,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
});
