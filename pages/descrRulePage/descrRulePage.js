/*
 * @Author: 红尘痴子
 * @Date: 2020-07-14 13:35:07
 * @LastEditTime: 2020-08-21 15:53:24
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\descrRulePage\descrRulePage.js
 * @https://www.jiangcan95.com/
 */

// pages/descrRulePage/descrRulePage.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		text: '',
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		console.log(options);
		this.setData({
			text: options.text,
		});
		if (options.key) {
			app.http({
				url: app.api.ruleSpecifications,
				data: {
					key: options.key,
				},
				//method: 'get',
			})
				.then((res) => {
					that.setData({
						text: res.data.data.val.replace(
							/\<img/gi,
							'<img style="max-width:100%;height:auto;display:block;"'
						),
					});
				})
				.catch((err) => {});
		} else if (options.id) {
			wx.setNavigationBarTitle({
				title: '活动规则',
			});
			app.http({
				url: app.api.getRule,
				data: {
					id: options.id,
				},
				//method: 'get',
			})
				.then((res) => {
					that.setData({
						text: res.data.data.rule.replace(
							/\<img/gi,
							'<img style="max-width:100%;height:auto;display:block;"'
						),
					});
				})
				.catch((err) => {});
		}
	},
});
