/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 16:36:20
 * @LastEditTime: 2020-07-28 11:58:42
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\shareOrdr\shareOrdr.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		active: 0,
		vanTitle: [
			{ id: 1, title: '全部' },
			{ id: 1, title: '待付款' },
			{ id: 1, title: '待发货' },
			{ id: 1, title: '待收货' },
			{ id: 1, title: '已完成' },
		],
		limit: 10, //分页默认值
		page: 1, //分页默认值
		control: false, //是否还有下一页
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.setData({
			active: parseInt(options.index),
		});
		that.get_share_list(options.index);
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {},
	// 获取数据
	get_share_list(num) {
		let that = this;
		if (num == 4) {
			num = 5;
		}
		app.http({
			url: app.api.get_share_list,
			data: {
				status: num,
				limit: that.data.limit,
				page: that.data.page,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	//切换标签
	onChange(event) {
		let that = this;
		that.setData({
			active: event.detail.name,
		});
		that.get_share_list(event.detail.name);
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.get_share_list(that.data.active);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
