/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 16:56:41
 * @LastEditTime: 2020-07-29 10:27:58
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\grpReward\grpReward.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		balPic:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E5%96%B5%E5%B8%81%402x.png', //余额图片
		active: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		that.sharingRewards(2); //2-拼团奖励 4-分享奖励
	},
	// 获取余额列表
	sharingRewards(type) {
		let that = this;
		app.http({
			url: app.api.wallet_sharingRewards,
			data: { type: type },
		})
			.then((res) => {
				that.setData({
					sharingRewards: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 跳到历史概况页面
	historicalOverview() {
		let type = 2;
		wx.navigateTo({
			url: `/pages/historicalOverview/historicalOverview?type=${type}`,
		});
	},
	// 跳到说明页面
	explanation() {
		let key = 'group';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
