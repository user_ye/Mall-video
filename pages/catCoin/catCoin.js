let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		balPic:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E5%96%B5%E5%B8%81%402x.png', //余额图片
		active: 0,

		requestMiao: {}, // 请求到的余额

		paginationList: [], // 分页列表数据
		pageNum: 1, // 第几页
		limit: 10, // 每页数量
		isMax: false, // 是否已显示最大数量
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		that.post_my_getBalances(1); //1为收入 2为支出，默认返回收入
	},
	// 获取余额列表
	post_my_getBalances(type) {
		let that = this;
		let paginationList = this.data.paginationList; // 分页列表数据
		let pageNum = this.data.pageNum; // 第几页
		let limit = this.data.limit; // 每页数量
		let isMax = this.data.isMax; // 是否已显示最大数量

		if (!isMax) {
			app.http({
				url: app.api.mb_getMiao,
				data: {
					type: type,
					page: pageNum, // 第几页
					limit: limit, // 每页数量
				},
			})
				.then((res) => {
					that.setData({
						requestMiao: res.data.data.miao,
					});

					let listData = res.data.data.list.data; // 请求到的列表数据
					paginationList.push(...listData);
					that.setData({
						paginationList: paginationList,
					});

					let total = res.data.data.list.total; // 列表的总数量
					if (paginationList.length >= total) {
						isMax = true;
					}

					pageNum += 1;
					that.setData({
						paginationList: paginationList,
						isMax: isMax,
						pageNum: pageNum,
					});
				})
				.catch((err) => {});
		}
	},
	//切换标签
	onChange(event) {
		let that = this;
		// wx.showToast({
		// 	title: `切换到标签 ${event.detail.name}`,
		// 	icon: 'none',
		// });

		this.setData({
			pageNum: 1,
			paginationList: [],
			isMax: false,
			active: event.detail.name,
		});

		that.post_my_getBalances(event.detail.name + 1);
	},
	// 滚动到顶部
	upper(e) {},
	//滚动到底部
	lower(e) {
		this.post_my_getBalances(this.data.active + 1);
	},
	// 跳到喵呗兑换
	withdraw() {
		// let index = 0;
		wx.navigateTo({
			url: `/pages/meowCurrExchange/meowCurrExchange`,
		});
	},
	// 跳到兑换记录
	exchangeRecordsBtn() {
		// let index = 0;
		wx.navigateTo({
			url: `/pages/exchangeHst/exchangeHst`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
