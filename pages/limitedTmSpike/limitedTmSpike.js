/*
 * @Author: 红尘痴子
 * @Date: 2020-08-18 09:26:25
 * @LastEditTime: 2020-08-18 16:24:31
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\limitedTmSpike\limitedTmSpike.js
 * @https://www.jiangcan95.com/
 */
//获取应用实例
const app = getApp();

Page({
	data: {
		navData: [
			{
				text: '已抢购',
				time: '09:33',
			},
			{
				text: '抢购中',
				time: '09:33',
			},
			{
				text: '即将开始',
				time: '09:33',
			},
			{
				text: '明日预告',
				time: '09:33',
			},
			{
				text: '明日预告',
				time: '09:33',
			},
			{
				text: '明日预告',
				time: '09:33',
			},
			{
				text: '明日预告',
				time: '09:33',
			},
			{
				text: '明日预告',
				time: '09:33',
			},
			{
				text: '明日预告',
				time: '09:33',
			},
		],
		currentTab: 0,
		navScrollLeft: 0,
		img:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/icon/%E7%A7%92%E6%9D%80%E6%96%87%E5%AD%97.png', //秒杀文字
	},
	//事件处理函数
	onLoad: function () {
		wx.getSystemInfo({
			success: (res) => {
				this.setData({
					pixelRatio: res.pixelRatio,
					windowHeight: res.windowHeight,
					windowWidth: res.windowWidth,
				});
			},
		});
		setTimeout(() => {
			wx.loadFontFace({
				family: 'numFont',
				source:
					'url("https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/DIN%20Alternate%20Bold%281%29.ttf")',
				success: console.log,
			});
		}, 1000);
	},
	switchNav(event) {
		console.log(event);
		var cur = event.currentTarget.dataset.current;
		//每个tab选项宽度占1/5
		var singleNavWidth = this.data.windowWidth / 5;
		//tab选项居中
		this.setData({
			navScrollLeft: (cur - 2) * singleNavWidth,
		});
		if (this.data.currentTab == cur) {
			return false;
		} else {
			this.setData({
				currentTab: cur,
			});
		}
	},
	switchTab(event) {
		console.log(event);
		var cur = event.detail.current;
		var singleNavWidth = this.data.windowWidth / 5;
		this.setData({
			currentTab: cur,
			navScrollLeft: (cur - 2) * singleNavWidth,
		});
	},
});
