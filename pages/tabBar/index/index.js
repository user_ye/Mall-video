/*
 * @Author: 红尘痴子
 * @Date: 2020-06-29 18:35:41
 * @LastEditors: 红尘痴子
 * @LastEditTime: 2020-09-10 09:07:05
 * @Description:
 */

let app = getApp();
var list = [];
import {
	multiArray,
	objectMultiArray,
} from '../../../resource/lib/pickerLinkage';
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		// city: '请授权定位', //城市
		city: '位置', //城市
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		indicatorDots: true, //轮播图是否显示指示点
		autoplay: true, //轮播图是否自动滑动
		interval: 3000, //自动切换时间间隔
		duration: 500, //轮播图滑动动画时长
		// 轮播图数据
		imgUrl: [],
		// 分类标签
		vanTan: [],
		videoList: [],
		multiIndex: ['', ''],
		multiArray: multiArray,
		objectMultiArray: objectMultiArray,
		checkLogin: false, //判断是否请求接口
		limit: 10, //分页默认值
		page: 1, //分页默认值
		control: false, //是否还有下一页
		isHideVideo: {}, // 是否隐藏视频
		getList: [],

		isShowSkeletonScreen: true, // 是否显示骨架屏
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			isShowSkeletonScreen: true, // 显示骨架屏
		})
		wx.hideTabBar(); // 隐藏 tabBar

		let that = this;
		//测试api
		// that.testApi();
		// 是否隐藏视频
		app.http({
			url: app.api.view_index_viewHomeVideoHide,
			data: {},
		})
			.then((res) => {
				this.setData({
					isHideVideo: res.data.data,
				});
				// 设置缓存
				wx.setStorageSync('isHideVideo', res.data.data)
			})
			.catch((err) => {});

		//判断onLaunch是否执行完毕
		if (app.globalData.checkLogin) {
			that.reqIntfc();
		} else {
			app.checkLoginReadyCallback = (res) => {
				that.reqIntfc();
			};
		}

		app.store.setState({
			shareIt: 0,
		});
		that.setData({
			videoId: that.data.videoId,
			limit: 10, //分页默认值
			page: 1, //分页默认值
			control: false, //是否还有下一页
			is_hot: that.data.is_hot,
		});
		// that.get_index_videos(that.data.videoId);
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		// app.store.setState({
		// 	shareIt: 0,
		// });
		// that.setData({
		// 	videoId: that.data.videoId,
		// 	limit: 10, //分页默认值
		// 	page: 1, //分页默认值
		// 	control: false, //是否还有下一页
		// 	is_hot: that.data.is_hot,
		// });
		// that.get_index_videos(that.data.videoId);
	},
	// 请求接口
	reqIntfc() {
		let that = this;

		// 获取所在城市
		// that.get_location();
		//获取首页直播广告图
		// that.get_index_liveBroadcastBanner();

		// 获取首页轮播图
		that.get_Banner();

		// 获取首页视频分类
		that.get_index_videoType();

		//获取直播
		that.getList();
		that.getNoticeList();

		setTimeout(() => {
			// 获取用户数据
			that.get_my_getInfo();
		}, 1500);

		app.store.setState({
			uid: '',
		});
	},
	//直播接口
	getList() {
		let that = this;
		app.http({
			url: app.api.getList,
			data: {
				type: '',
				title: '',
				limit: 5,
				page: 1,
			},
		})
			.then((res) => {
				console.log(res, '这是直播列表');
				let list = res.data.data.data;
				// list.map((item) => {
				// 	item.tiem = app.util
				// 		.formatTimeYMDhms(item.add_time)
				// 		.slice(0, 10);
				// 	return;
				// });
				that.setData({
					getList:
						that.data.page == 1
							? list
							: that.data.getList.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	//直播预告
	getNoticeList() {
		let that = this;
		app.http({
			url: app.api.getNoticeList,
			data: {
				limit: 5,
				page: 1,
			},
		})
			.then((res) => {
				console.log(res);
				that.setData({
					getNoticeList: res.data.data.data,
				});
			})
			.catch((err) => {});
	},
	// 获取用户基本数据
	get_my_getInfo() {
		let that = this;
		app.http({
			url: app.api.get_my_getInfo,
			data: {},
			// //method: 'get',
		})
			.then((res) => {
				if (res.data.data[0].is_visitor != 0) {
					// 判断是不是游客,是游客就立即购买  1是分享  2是开团  3是立即购买
					wx.setStorage({
						key: 'touristIdentity',
						data: 1,
					});
				} else {
					// 判断是不是游客,是游客就立即购买  1是分享  2是开团  3是立即购买
					wx.setStorage({
						key: 'touristIdentity',
						data: 3,
					});
				}
				that.setData({
					getInfo: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 获取首页直播广告图
	// get_index_liveBroadcastBanner() {
	// 	let that = this;
	// 	app.http({
	// 		url: app.api.get_index_liveBroadcastBanner,
	// 		data: {},
	// 		//method: 'get',
	// 	})
	// 		.then((res) => {
	// 			that.setData({
	// 				liveBroadcastBanner: res.data.data,
	// 			});
	// 		})
	// 		.catch((err) => {});
	// },
	// 获取轮播图数据
	get_Banner() {
		let that = this;
		app.http({
			url: app.api.get_index_banner,
			data: {},
		})
			.then((res) => {
				that.setData({ imgUrl: res.data.data });

				that.getOther(); // 获取其它api请求
			})
			.catch((err) => {
				that.getOther(); // 获取其它api请求
			});
	},

	// 在轮播图请求成功后再执行其他api请求
	getOther() {
		let that = this;
		// // 获取首页视频分类
		// that.get_index_videoType();

		// setTimeout(()=>{
		// 	that.get_index_textLog(); // 获取首页通知栏
		// },500)

		// setTimeout(()=>{
		// 	//获取用户数据
		// 	that.get_my_getInfo();
		// },1000);
	},

	// 获取首页视频分类
	get_index_videoType() {
		let that = this;
		app.http({
			url: app.api.get_index_videoType,
			data: {},
		})
			.then((res) => {
				that.setData({ vanTan: res.data.data });
				//获取视频列表
				that.get_index_videos(res.data.data[0].id);
				// console.log('res.data.data[0].id---',res.data.data[0].id);
				wx.showTabBar(); // 显示 tabBar
			})
			.catch((err) => {});
	},
	// 获取首页视频列表
	get_index_videos(id, isRefresh = 0) {
		let that = this;
		let is_hot = 1;
		if (id != 0) {
			is_hot = 2;
		}
		let a = that.data.videoId ? that.data.videoId : id;
		app.http({
			url: app.api.get_index_videos,
			data: {
				type: a ? a : 0,
				limit: that.data.limit,
				page: that.data.page,
				is_hot: is_hot,
				isRefresh: isRefresh,
			},
			// //method: 'get',
		})
			.then((res) => {
				if (isRefresh == true) {
					// 停止当前页面下拉刷新
					wx.stopPullDownRefresh();
				}

				let list = res.data.data.data;
				list.map((item) => {
					if (item.user_nickname.length > 4) {
						item.user_nickname =
							item.user_nickname.slice(0, 4) + '...';
					}
					return item;
				});
				that.setData({
					videoList:
						that.data.page == 1
							? list
							: that.data.videoList.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
					is_hot: is_hot,
					typeId: id,
					videoId: id,

					isShowSkeletonScreen: false, // 是否显示骨架屏
				});
			})
			.catch((err) => {});
	},
	// 点击切换分类
	onChange(event) {
		let that = this;
		let a = that.data.vanTan;
		let id = 0;
		for (let i = 0; i < a.length; i++) {
			if (event.detail.title == a[i].name) {
				id = a[i].id;
			}
		}
		that.setData({
			videoId: id,
			limit: 10, //分页默认值
			page: 1, //分页默认值
			control: false, //是否还有下一页
			is_hot: that.data.is_hot,
		});
		that.get_index_videos(id);
	},
	// 获取所在城市
	// get_location() {
	// 	let that = this;
	// 	wx.getLocation({
	// 		type: 'gcj02',
	// 		success: (res) => {
	// 			let latitude = res.latitude,
	// 				longitude = res.longitude;
	// 			that.setData({
	// 				latitude,
	// 				longitude,
	// 			});
	// 			// 坐标地址解析
	// 			app.qqmapsdk.reverseGeocoder({
	// 				location: { latitude, longitude },
	// 				success: (sdkRes) => {
	// 					// wx.showToast({
	// 					// 	title: sdkRes.result.address_component.city,
	// 					// 	icon: 'none',
	// 					// });
	// 					let city = '';
	// 					if (sdkRes.result.address_component.city.length >= 4) {
	// 						city =
	// 							sdkRes.result.address_component.city.slice(
	// 								0,
	// 								3
	// 							) + '...';
	// 					} else {
	// 						city = sdkRes.result.address_component.city;
	// 					}
	// 					that.setData({
	// 						city: city,
	// 					});
	// 					wx.setStorageSync('city', city);
	// 				},
	// 			});
	// 		},
	// 	});
	// },
	// 跳到搜索页
	srchPage() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/srchPage/srchPage?index=${index}`,
		});
	},
	// 选择城市
	bindMultiPickerColumnChange: function (e) {
		let that = this;
		switch (e.detail.column) {
			case 0:
				list = [];
				for (var i = 0; i < that.data.objectMultiArray.length; i++) {
					if (
						that.data.objectMultiArray[i].parid ==
						that.data.objectMultiArray[e.detail.value].regid
					) {
						list.push(that.data.objectMultiArray[i].regname);
					}
				}
				that.setData({
					'multiArray[1]': list,
					'multiIndex[0]': e.detail.value,
					'multiIndex[1]': 0,
				});
		}
	},
	// 选择城市确定
	bindMultiPickerChange: function (e) {
		let that = this;
		that.setData({
			'multiIndex[0]': e.detail.value[0],
			'multiIndex[1]': e.detail.value[1],
		});
		if (that.data.multiArray[1][that.data.multiIndex[1]].length >= 4) {
			that.setData({
				city:
					that.data.multiArray[1][that.data.multiIndex[1]].slice(
						0,
						3
					) + '...',
			});
		} else {
			that.setData({
				city: that.data.multiArray[1][that.data.multiIndex[1]],
			});
		}
	},
	// 回到顶部
	bkToTheTop() {
		wx.pageScrollTo({
			scrollTop: 0,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		let id = that.data.videoId; //视频id
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.get_index_videos(id);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
	// 图片点击跳转
	switch(e) {
		let that = this;
		// let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/productDetails/productDetails?id=205`,
		});
	},
	swiperImg(e) {
		let that = this;




		console.log(e.currentTarget.dataset.url);
		wx.navigateTo({
			url: e.currentTarget.dataset.url,
		});
	},

	// 下拉刷新
	onPullDownRefresh(e) {
		// console.log('下拉刷新-----')
		// // 停止当前页面下拉刷新
		// wx.stopPullDownRefresh()
		this.setData({
			limit: 15,
			page: 1,
		});
		this.data.videoList = [];
		this.get_index_videos(this.data.videoId, 1);
		//获取直播
		this.getList();
		this.getNoticeList();
		// 获取首页轮播图
		this.get_Banner();
	},
	//跳转到直播广场
	zbbox() {
		wx.navigateTo({
			url: `/pages/LiveBroadcastSquare/LiveBroadcastSquare`,
		});
	},
	// 测试api
	testApi() {
		let that = this;
		wx.request({
			url: 'http://rap2.taobao.org:38080/app/mock/265880/listTest', //仅为示例，并非真实的接口地址
			data: {},
			header: {
				'content-type': 'application/json', // 默认值
			},
			success(res) {
				console.log(res.data);
			},
		});
	},

	// 获取手机授权弹层
	getPhone() {
		app.store.setState({
			getPhoneNumber: true,
		});
	},
	bindGetPhoneNumber(e) {
		console.log('e----',e);
		if (e.detail.iv) {
			app.store.setState({
				login: true,
				getPhoneNumber: false,
				loginType: 2,
			});
		} else {
			wx.showToast({
				title: '您取消了授权',
				icon: 'none',
				duration: 1500,
			});
		}
	}
});
