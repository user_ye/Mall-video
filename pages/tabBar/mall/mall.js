/*
 * @Author: 红尘痴子
 * @Date: 2020-06-29 19:37:25
 * @LastEditors: 红尘痴子
 * @LastEditTime: 2020-09-04 15:17:26
 * @Description:
 */

let app = getApp();

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		// 分类标签
		vanTan: [], //一级分类
		indicatorDots: true, //轮播图是否显示指示点
		autoplay: true, //轮播图是否自动滑动
		interval: 3000, //自动切换时间间隔
		duration: 500, //轮播图滑动动画时长
		// 轮播图数据
		imgUrl: [],
		// 优惠卷
		vchr:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/mall/680%E8%B4%AD%E7%89%A9%E5%88%B8banner%402x.png',
		// 精品活动数据
		boutiqueEvntData: [],
		// 选中筛选效果
		active: 0,
		iconfont: 0, //加个选中判断
		btnType: 0, //判断用户是不是游客
		limit: 10, //分页默认值
		page: 1, //分页默认值
		orderKey: '', //排序值 add_time表示时间，sales_volume销量,default_discount_price价格
		order: '', //升或降  desc 升 ，asc降
		is_hot: 1, //是否精选 	1表示精选，0表示不是精选
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		noticeDetail: [], // 获取的公共栏
		mallIndexShopHomeList: [], // 商城首页-爆款、福利、新品
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		//判断onLaunch是否执行完毕
		if (app.globalData.checkLogin) {
			this.requestApi();
		} else {
			app.checkLoginReadyCallback = (res) => {
				this.requestApi();
			};
		}

	},
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let timestamp = Date.parse(new Date());
		this.setData({
			timestamp,
		});
	},

	requestApi(){
		this.get_mall_mallType(0);
		// 获取商城轮播图
		this.get_mall_ProductBanner();
		this.get_my_getInfo();
		// 获取首页通知栏
		this.get_index_textLog();
		// 商城首页分类菜单
		this.homeDisplay();
		// 商城首页-爆款、福利、新品
		this.requestMallIndexShopHomeList();

		this.setData({
			btnType: wx.getStorageSync('touristIdentity'),
		});
	},

	// 点击热卖爆款、福利、新品
	clickHostGoods(ev){
		let item = ev.currentTarget.dataset.item;
		// 爆款 host、福利 welfare、新品 newProducts
		let type = ev.currentTarget.dataset.type;
		// console.log('item---',item);
		if (type == 'host'){ // 如果是爆款商品，则跳转商品详情
		// &btnType=1
		// &typeTwo=1
			wx.navigateTo({
				url: `/pages/productDetails/productDetails?id=${item.id}`,
			});
		} else if (type == 'welfare' || type == 'new'){ // 如果是福利或新品，则跳转商品列表
			// 点击福利商品或新品区域
			wx.navigateTo({
				url: `/pages/new-goods/new-goods?goodsType=${type}`,
			});
		}
	},

	// 商城首页-爆款、福利、新品
	requestMallIndexShopHomeList() {
		let that = this;
		app.http({
			url: app.api.mallIndexShopHomeList,
			data: {},
		})
			.then((res) => {
				let requestData = res.data.data;
				console.log('requestData---',requestData);
				that.setData({
					mallIndexShopHomeList:requestData
				})
			})
			.catch((err) => {});
	},

	// 商城首页分类菜单
	homeDisplay() {
		let that = this;
		app.http({
			url: app.api.homeDisplay,
			data: {},
		})
			.then((res) => {
				// console.log(res);
				that.setData({
					homeDisplay: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 获取通知栏
	get_index_textLog() {
		let that = this;
		app.http({
			url: app.api.get_index_textLog,
			data: {},
			// //method: 'get',
		})
			.then((res) => {
				that.setData({
					noticeDetail: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 获取商品分类
	get_mall_mallType(id) {
		let that = this;
		let type = 0;
		if (id == 0) {
			type = 0;
		} else {
			type = id;
		}
		app.http({
			url: app.api.get_mall_mallType,
			data: {
				type: type,
			},
			//method: 'get',
		})
			.then((res) => {
				if (id == 0) {
					let a = { id: 0, name: '全部' };
					let records = that.prepend(res.data.data, a);
					that.setData({ vanTan: records });
				} else {
					that.setData({ vanTan1: res.data.data });
				}
				that.get_mall_malls(id);
			})
			.catch((err) => {});
	},
	// 获取商品列表
	get_mall_malls(id) {
		let that = this;
		let type = 0;
		if (id == 0) {
			type = 0;
			that.setData({
				type,
				productId: type,
			});
		} else {
			type = id;
			that.setData({
				type,
				productId: type,
			});
		}
		app.http({
			url: app.api.get_mall_malls,
			data: {
				type: type,
				limit: that.data.limit,
				page: that.data.page,
				orderKey: that.data.orderKey,
				order: that.data.order,
				is_hot: that.data.is_hot,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					boutiqueEvntData:
						that.data.page == 1
							? list
							: that.data.boutiqueEvntData.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	// 获取商城轮播图
	get_mall_ProductBanner() {
		let that = this;
		app.http({
			url: app.api.get_mall_ProductBanner,
			data: {},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					ProductBanner: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 一级分类标签切换
	onChange(event) {
		let that = this;
		let vanTan = that.data.vanTan;
		// console.log("event.detail.title---",event.detail.title);
		// console.log("event.detail.index---",event.detail.index);
		this.setData({
			active: event.detail.index
		})
		let id = 0;
		for (let i = 0; i < vanTan.length; i++) {
			if (event.detail.title == vanTan[i].name) {
				id = vanTan[i].id;
			}
		}
		if (id == 0) {
			that.setData({
				productId: id,
				limit: 10,
				page: 1,
				orderKey: '',
				order: '',
				is_hot: 1,
			});
		} else {
			that.setData({
				productId: id,
				limit: 10,
				page: 1,
				orderKey: '',
				order: '',
				is_hot: 1,
				// active: 1,
				iconfont: 0,
			});
		}
		// that.get_mall_mallType(id);
		that.get_mall_malls(id);
	},
	// 筛选按钮点击
	filter(e) {
		let that = this;
		let type = that.data.type;
		if (e.currentTarget.dataset.index == 1) {
			that.setData({
				active: e.currentTarget.dataset.index,
				orderKey: '',
				is_hot: 1,
				iconfont: 0,
			});
		}
		if (e.currentTarget.dataset.index == 2) {
			that.setData({
				active: e.currentTarget.dataset.index,
				orderKey: 'add_time',
				is_hot: 0,
				iconfont: 0,
			});
		}
		if (e.currentTarget.dataset.index == 3 && that.data.iconfont != 3) {
			that.setData({
				active: e.currentTarget.dataset.index,
				orderKey: 'sales_volume',
				is_hot: 0,
				order: 'desc',
				iconfont: 3,
			});
		} else if (
			e.currentTarget.dataset.index == 3 &&
			that.data.iconfont == 3
		) {
			that.setData({
				iconfont: 4,
				orderKey: 'sales_volume',
				is_hot: 0,
				order: 'asc',
				active: e.currentTarget.dataset.index,
			});
		}
		if (e.currentTarget.dataset.index == 4 && that.data.iconfont != 1) {
			that.setData({
				iconfont: 1,
				orderKey: 'default_discount_price',
				is_hot: 0,
				order: 'desc',
				active: e.currentTarget.dataset.index,
			});
		} else if (
			e.currentTarget.dataset.index == 4 &&
			that.data.iconfont == 1
		) {
			that.setData({
				iconfont: 2,
				orderKey: 'default_discount_price',
				is_hot: 0,
				order: 'asc',
				active: e.currentTarget.dataset.index,
			});
		}
		that.get_mall_malls(type);
	},
	// 跳到商品详情
	_goCoupon(e) {
		let that = this;
		let type = 0;
		let id = e.currentTarget.dataset.item.id;
		wx.navigateTo({
			url: `/pages/productDetails/productDetails?id=${id}&type=${type}`,
		});
	},
	// 回到顶部
	bkToTheTop() {
		wx.pageScrollTo({
			scrollTop: 0,
		});
	},
	// 跳到搜索页
	srchPage() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/srchPage/srchPage?index=${index}`,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		let id = that.data.productId; //商品id
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.get_mall_malls(id);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
	// 页面刷新
	onPullDownRefresh: function () {
		let that = this;
		that.setData({
			limit: 10, //分页默认值
			page: 1, //分页默认值
			orderKey: '', //排序值 add_time表示时间，sales_volume销量,default_discount_price价格
			order: '', //升或降  desc 升 ，asc降
			is_hot: 1, //是否精选 	1表示精选，0表示不是精选
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		// that.get_mall_mallType(that.data.productId);
		that.get_mall_malls(that.data.productId);
		wx.stopPullDownRefresh();
	},
	// 开通视频收益
	videoRev() {
		wx.navigateTo({
			url: '/pages/shoppingVolume/privilege/privilege',
		});
	},
	// 跳到购物券记录页面
	shoppingVchr() {
		let that = this;
		let index = 1;
		wx.navigateTo({
			url: `/pages/shoppingVolume/recordsOfConsumption/recordsOfConsumption?id=${index}`,
		});
	},
	// 把数据添加到头部
	prepend(arr, item) {
		var m = arr.slice();
		m.unshift(item);
		return m;
	},
	swiperImg(e) {
		let that = this;
		console.log(e.currentTarget.dataset.url);
		wx.navigateTo({
			url: e.currentTarget.dataset.url,
		});

		// if (that.data.getInfo[0].is_proceeds == 0) {
		// 	wx.navigateTo({
		// 		url: '/pages/shoppingVolume/privilege/privilege',
		// 	});
		// } else {
		// 	wx.navigateTo({
		// 		url: `/pages/shoppingVolume/recordsOfConsumption/recordsOfConsumption`,
		// 	});
		// }
	},
	// 获取用户基本数据
	get_my_getInfo() {
		let that = this;
		app.http({
			url: app.api.get_my_getInfo,
			data: {},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					getInfo: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 商城分类菜单
	row(e) {
		let that = this;
		let page = e.currentTarget.dataset.page;
		console.log(e.currentTarget.dataset.page);
		if (page != 0) {
			wx.navigateTo({
				url: page,
			});
		} else {
			wx.showToast({
				title: '敬请期待!',
				icon: 'none',
				duration: 2000,
			});
		}
	},

	// 获取手机授权弹层
	getPhone() {
		app.store.setState({
			getPhoneNumber: true,
		});
		console.log("获取手机授权弹层---");
		// that.onLoad(); // 刷新
	},
	bindGetPhoneNumber(e) {
		console.log('e----',e);
		if (e.detail.iv) {
			app.store.setState({
				login: true,
				getPhoneNumber: false,
				loginType: 2,
			});
		} else {
			wx.showToast({
				title: '您取消了授权',
				icon: 'none',
				duration: 1500,
			});
		}
	}
});
