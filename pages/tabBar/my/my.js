/*
 * @Author: 红尘痴子
 * @Date: 2020-06-29 19:37:25
 * @LastEditors: 红尘痴子
 * @LastEditTime: 2020-09-07 10:12:35
 * @Description:
 */

// pages/tabBar/my/my.js

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		infoData:null, // 本地缓存的用户信息
		username: '',
		profPic: '',
		login: wx.getStorageSync('login'), //判断用户有没有授权
		show: false, //隐藏弹窗
		enterInvitationCdShow: false, //输入邀请码弹窗
		acceptInvitationShow: false, //是否接受邀请弹窗
		howToGetTheInvitationCd: false, //如何获得邀请码弹窗
		autoShutOffShow: false, //无效的邀请码弹窗
		tutorWechatShow: false, //导师弹窗
		weChatNum: 'meizhuangshuo', //导师微信号
		invitationCdInputBox: '', //邀请码输入框
		autoShutNum: 3, //倒计时
		invitationCd: '', //邀请码
		invitationName: '', //邀请码

		isShowVideo: {}, // 是否显示视频
		imgUrl: [
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/bg/%E6%8E%8C%E6%9F%9C%E7%89%B9%E6%9D%83%E8%83%8C%E6%99%AF.jpg',
		],
		indicatorDots: true, //轮播图是否显示指示点
		autoplay: true, //轮播图是否自动滑动
		interval: 3000, //自动切换时间间隔
		duration: 500, //轮播图滑动动画时长
		getInfo:'', // 用户信息getInfo，level_number 等级 0游客，1普通，2vip，3资深，4精英，5超级

		shopApplicationStatus:'', // 申请店铺的状态数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		// let login = wx.getStorageSync('login');
		// if (login) {
		// 	that.get_my_getInfo();
		// }

		console.log("---我的页面---onLoad---");


		that.post_branch_tutorWechat();

		// 是否显示视频
		// app.http({
		// 	url: app.api.view_index_viewHomeVideoHide,
		// 	data: {},
		// })
		// 	.then((res) => {
		// 		this.setData({
		// 			isShowVideo: res.data.data,
		// 		});
		// 	})
		// 	.catch((err) => {});
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		// this.onLoad();

		console.log("---我的页面---onShow---");

		// 获取指定缓存
		let isHideVideo = wx.getStorageSync('isHideVideo')
		if (isHideVideo){
			this.setData({
				isShowVideo: isHideVideo,
			});
		}

		let infoData = wx.getStorageSync('infoData')
		this.setData({
			infoData:infoData
		})

		this.getMyGetInfo();	// 获取用户基本数据
		this.getShopIndexCheckShop(); // 获取申请店铺状态

		// 禁用分享
		wx.hideShareMenu({
			menus: ['shareAppMessage', 'shareTimeline']
		})
	},

	// 获取用户基本数据
	getMyGetInfo() {
		console.log('获取用户基本数据---');
		let that = this;
		app.http({
			url: app.api.get_my_getInfo,
			data: {},
			//method: 'get',
		})
			.then((res) => {
				console.log('获取用户基本数据---res--',res)
				console.log('获取用户基本数据---res--',res.data.data)
				that.setData({
					getInfo: res.data.data,
					profPic: res.data.data[0].avatarurl, //头像
					invitationCd: res.data.data[0].invite_code, // 邀请码
				});
				app.store.setState({
					infoData: res.data.data[0],
				});
				if (res.data.data[0].phone) {
					app.store.setState({
						getPhoneNumber: false,
					});
				}
			})
			.catch((err) => {});
	},
	// 跳到我的钱包
	myPurse() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/myPurse/myPurse?id=${index}`,
		});
	},
	// 跳到掌柜权益
	ownershipRights() {
		let index = 0;
		wx.navigateTo({
			url: `/pages/shopkeeperPrivileges/shopkeeperPrivileges?id=${index}`,
		});
	},
	// 跳到订单页面
	allOrders(e) {
		let that = this;
		let index = e.currentTarget.dataset.index; //传值判断跳到那个tab下,0是全部,1是待付款,2是待发货,3是待收货,4是待评价,5是售后
		let url = e.currentTarget.dataset.url; //传值判断跳到那个页面
		let id = e.currentTarget.dataset.id || 0; //传值id
		that.navigateTo(url, id, index);
	},
	// 跳order页面
	navigateTo(type, id, index) {
		wx.navigateTo({
			url: `/pages/order/${type}/${type}?id=${id}&index=${index}`,
		});
	},
	// 暂无
	prompt() {
		wx.navigateTo({
			url: `/pages/creALiveBroadcast/creALiveBroadcast`,
			// url: `/pages/liveLst/liveLst`,
		});
		// wx.showToast({
		// 	title: '敬请期待!',
		// 	icon: 'none',
		// 	duration: 2000,
		// });
	},

	// 获取申请店铺状态
	getShopIndexCheckShop(e){
		let that = this;
		app.http({
			url: app.api.shopIndexCheckShop,
			data: {},
		})
			.then((res) => {
				//  -1申请失败，0没申请，1申请中，2已通过
				console.log('获取申请店铺状态res---',res.data.data);
				that.setData({
					shopApplicationStatus: res.data.data
				})
				// let type = e.currentTarget.dataset.type;
				// if (type == 'shop'){
				//
				// 	let shopApplicationStatus = res.data.data
				// 	// apply_status: -1申请失败，0没申请，1申请中，2已通过
				// 	if (shopApplicationStatus.apply_status == 0){
				// 		wx.navigateTo({
				// 			url: '/pagesTwo/shop/shop-apply/shop-apply',
				// 		});
				// 	} else if (shopApplicationStatus.apply_status == -1 || shopApplicationStatus.apply_status == 1){
				// 		wx.navigateTo({
				// 			url: '/pagesTwo/shop/to-examine-tips/to-examine-tips?shopData=' + JSON.stringify(shopApplicationStatus)
				// 		});
				// 	} else if (shopApplicationStatus.apply_status == 2){
				// 		wx.navigateTo({
				// 			url: '/pagesTwo/shop/my-shop/my-shop',
				// 		});
				// 	}
				// 	return ;
				// }
			})
			.catch((err) => {

			});
	},

	// 跳到服务页面
	servJump(e) {
		console.log("跳到服务页面");
		let that = this;
		let type = e.currentTarget.dataset.type;
		let url = e.currentTarget.dataset.url;

		if (type == 'inviteIn'){
			wx.showModal({
				title: '提示',
				content: '请先升级VIP再邀请商家入驻',
				showCancel: false, // 是否显示取消按钮，默认为 true
				success (res) {
					if (res.confirm) {
						console.log('用户点击确定')
					} else if (res.cancel) {
						console.log('用户点击取消')
					}
				}
			})
			return ;
		} else if (type == 'shop'){
			wx.navigateTo({
				url: '/pagesTwo/shop/shop-entrance-application/shop-entrance-application'
			});


			// let shopApplicationStatus = this.data.shopApplicationStatus
			// if (shopApplicationStatus){
			// 	console.log("shop----");
			// 	// apply_status: -1申请失败，0没申请，1申请中，2已通过
			// 	if (shopApplicationStatus.online.apply_status == 0){
			// 		wx.navigateTo({
			// 			url: '/pagesTwo/shop/shop-apply/shop-apply',
			// 		});
			// 	} else if (shopApplicationStatus.online.apply_status == -1 || shopApplicationStatus.online.apply_status == 1){
			// 		wx.navigateTo({
			// 			url: '/pagesTwo/shop/to-examine-tips/to-examine-tips?shopData=' + JSON.stringify(shopApplicationStatus) + '&shopType=online'
			// 		});
			// 	} else if (shopApplicationStatus.online.apply_status == 2){
			// 		wx.navigateTo({
			// 			url: '/pagesTwo/shop/my-shop/my-shop',
			// 		});
			// 	}
			// } else {
			// 	this.getShopIndexCheckShop();
			// }


			return ;
		} if (type == 'coupon'){ // 跳转优惠卷
			wx.navigateTo({
				url: '/pages/coupon/receive-list/receive-list',
			});
		} else if (type) {
			wx.navigateTo({
				url: `/pages/${type}/${url}/${url}`,
			});
		} else if (url != 0) {
			wx.navigateTo({
				url: `/pages/${url}/${url}`,
			});
		} else {
			wx.showToast({
				title: '敬请期待!',
				icon: 'none',
				duration: 2000,
			});
		}
	},
	// 关闭弹窗
	enterInvitationCdPopup() {
		let that = this;
		that.setData({
			show: false,
			enterInvitationCdShow: false, //输入邀请码弹窗
			acceptInvitationShow: false, //是否接受邀请弹窗
			autoShutOffShow: false, //无效的邀请码弹窗
			tutorWechatShow: false, //导师弹窗
			howToGetTheInvitationCd: false, //如何获得邀请码弹窗
		});
	},
	// 输入邀请码弹窗
	enterInvitationCd() {
		let that = this;
		if (that.data.enterInvitationCdShow) {
			that.setData({
				show: !that.data.show,
				enterInvitationCdShow: !that.data.enterInvitationCdShow,
			});
		} else {
			that.setData({
				show: !that.data.show,
				enterInvitationCdShow: !that.data.enterInvitationCdShow,
			});
		}
	},
	// 粘贴邀请码
	paste() {
		let that = this;
		wx.getClipboardData({
			success(res) {
				that.setData({
					invitationCdInputBox: res.data,
				});
			},
		});
	},
	// 监听input输入
	invitationCd(e) {
		let that = this;
		that.setData({
			invitationCdInputBox: e.detail.value,
		});
	},
	// 提交邀请码
	submit() {
		let that = this;
		app.http({
			url: app.api.post_my_checkInviteCode,
			data: {
				code: that.data.invitationCdInputBox,
			},
		})
			.then((res) => {
				that.setData({
					invitationName: res.data.data.name,
					enterInvitationCdShow: !that.data.enterInvitationCdShow,
					acceptInvitationShow: !that.data.acceptInvitationShow,
				});
			})
			.catch((err) => {
				that.setData({
					enterInvitationCdShow: !that.data.enterInvitationCdShow,
					autoShutOffShow: !that.data.autoShutOffShow,
				});
				that.autoShutOff();
				that.setData({
					invitationCdInputBox: '',
				});
			});
	},
	// 自动关闭无效的邀请码弹窗
	autoShutOff() {
		let that = this;
		let autoShut = setInterval(() => {
			if (that.data.autoShutNum == 0) {
				that.setData({
					show: false,
					autoShutOffShow: false,
					autoShutNum: 3,
				});
				clearInterval(autoShut);
			} else {
				that.setData({
					autoShutNum: that.data.autoShutNum - 1,
				});
			}
		}, 1000);
	},
	// 接受邀请
	acceptAnInvitation() {
		let that = this;
		app.http({
			url: app.api.post_my_bindInviteCode,
			data: {
				code: that.data.invitationCdInputBox,
			},
		})
			.then((res) => {
				that.setData({
					invitationName: res.data.data.name,
				});
				wx.showToast({
					title: '成功',
					icon: 'success',
					duration: 2000,
				});
				that.get_my_getInfo();
				that.getMyGetInfo();
			})
			.catch((err) => {
				that.autoShutOff();
			});
		that.setData({
			show: !that.data.show,
			acceptInvitationShow: !that.data.acceptInvitationShow,
			invitationCdInputBox: '',
		});
	},
	// 获取导师微信
	post_branch_tutorWechat() {
		let that = this;
		app.http({
			url: app.api.post_branch_tutorWechat,
			data: {},
		})
			.then((res) => {
				that.setData({
					weChatNum: res.data.data.wechat,
					weChatname: res.data.data.name,
					weChatavatarurl: res.data.data.avatarurl,
				});
			})
			.catch((err) => {});
	},
	// 导师微信弹窗
	tutorWechat() {
		let that = this;
		if (that.data.tutorWechatShow) {
			that.setData({
				show: !that.data.show,
				tutorWechatShow: !that.data.tutorWechatShow,
			});
		} else {
			that.setData({
				show: !that.data.show,
				tutorWechatShow: !that.data.tutorWechatShow,
			});
		}
	},
	// 复制导师微信号
	cpyWechat() {
		let that = this;
		wx.setClipboardData({
			data: that.data.weChatNum,
			success(res) {
				wx.getClipboardData({
					success(res) {
						that.setData({
							tutorWechatShow: !that.data.tutorWechatShow,
							show: !that.data.show,
						});
					},
				});
			},
		});
	},

	// 复制邀请码
	copyInvitationCd() {
		let that = this;
		wx.setClipboardData({
			data: that.data.invitationCd,
			success(res) {
				wx.getClipboardData({
					success(res) {},
				});
			},
		});
	},
	// 跳到购物券记录页面
	shoppingVchr() {
		let that = this;
		let index = 1;
		wx.navigateTo({
			url: `/pages/shoppingVolume/recordsOfConsumption/recordsOfConsumption?id=${index}`,
		});
	},
	// 跳到我的客户页面
	myClient() {
		wx.navigateTo({
			url: '/pages/myClient/myClient',
		});
	},
	// 跳到视频上传
	afterRead(e) {
		let that = this;
		let qm = app.qm({ type: 'video' });
		console.log(qm);
		wx.showLoading({
			title: '上传中...',
		});
		wx.uploadFile({
			url: app.api_url + '/user/video/publishVideo', // 仅为示例，非真实的接口地址
			filePath: e.detail.file.path,
			name: 'video',
			header: {
				Authorization: app.store.$state.token,
				'content-type': 'application/x-www-form-urlencoded',
			},
			formData: qm,
			success(res) {
				wx.hideLoading();
				let codeJson = JSON.parse(res.data);
				let id = codeJson.data.id;
				let video = codeJson.data.video;
				let videoImg = codeJson.data.cover;
				console.log(res, video);
				wx.navigateTo({
					url: `/pages/video/videoRelease/videoRelease?id=${id}&video=${video}&videoImg=${videoImg}`,
				});
			},
			fail(err) {},
		});
	},
	test(){
		wx.navigateTo({
			url: `/pages/rights-and-interests/privilege-list/index`,
		});
	},
	// 客服中心
	handleContact(e) {},
	// 如何获得邀请码
	howToGet() {
		let that = this;
		that.setData({
			howToGetTheInvitationCd: !that.data.howToGetTheInvitationCd,
			enterInvitationCdShow: !that.data.enterInvitationCdShow,
		});
		app.http({
			url: app.api.ruleSpecifications,
			data: {
				key: 'invitation_code',
			},
			//method: 'get',
		})
			.then((res) => {
				that.setData({
					invitation_code: res.data.data.val,
				});
			})
			.catch((err) => {});
	},
	// 开通视频收益
	videoRev() {
		wx.navigateTo({
			// url: '/pages/shoppingVolume/privilege/privilege',
			url: '/pages/videoRev/videoRev',
		});
	},
	getPhone() {
		app.store.setState({
			getPhoneNumber: true,
		});
	},
	// 测试订阅消息
	aaaaa() {
		let that = this;
		app.http({
			url: app.api.del,
			data: { uid: app.store.$state.infoData.id },
		})
			.then((res) => {
				wx.showToast({
					title: '删除成功',
					icon: 'success',
					duration: 2000,
				});
			})
			.catch((err) => {});
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function (res) {
		let that = this;
		let invite_code = app.store.getState().infoData.invite_code;
		let uid = app.store.getState().infoData.id;
		let shareImg = '';
		if (res.from === 'button') {
			return {
				title: '',
				path: `/pagesTwo/shop/shop-entrance-application/shop-entrance-application?code=${invite_code}&uid=${uid}`,
				imageUrl: shareImg,
				success() {},
				fail() {},
				complete() {},
			};
		}
		return;
	},
});
