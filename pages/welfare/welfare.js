// pagesTwo/welfare/welfare.js
import store from "../../resource/lib/mystore";

let app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    requestSignInData: {}, // 请求到的今日签到数据
    videoReward:{}, // 收益
    infoData:{}, // 用户信息
    // pageThis:111,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // app.globalData.pageThis = this;
    app.store.setState({
      pageThis: this
    });

    // 禁用分享
    wx.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline']
    })

    // app.store.setState({
    //   isAuthPhone: false
    // });

    if (app.globalData.checkLogin) {
      this.getRequest();
      this.profit(); // 收益
      this.updateUserInfo(); // 刷新用户信息
    } else {
      app.checkLoginReadyCallback = (res) => {
        this.getRequest();
        this.profit(); // 收益
        this.updateUserInfo(); // 刷新用户信息
      };
    }

    // console.log('app.store---',app.store.$state.isAuthPhone);

  },

  aa(){
    console.log("aa---执行--------aaa");
  },

  // 请求打卡数据
  getRequest(){
    let that = this;
    app.http({
      url: app.api.signIndexUserSignDetail,
      data: {},
      //method: 'get',
    })
      .then((res) => {
        let requestSignInData = res.data.data;
        requestSignInData.rules = requestSignInData.rules.map(mapRes => {
          return JSON.parse(mapRes);
          // console.log("mapRes---", mapRes);
        })
        that.setData({
          requestSignInData
        })
        // console.log("mapRequestData---", requestSignInData);
      })
      .catch((err) => {

      });
  },

  // 打卡
  signInBtn(){
    let that = this;

    if (this.data.requestSignInData.is_sign == 2){
      return ;
    }

    app.http({
      url: app.api.signIndexSignIn,
      data: {},
      //method: 'get',
    })
      .then((res) => {
        let msg = res.data.msg;
        wx.showToast({
          title: msg,
          icon: 'success',
          duration: 1500
        })
        that.getRequest(); // 请求打卡数据
      })
      .catch((err) => {

      });
  },

  // 收益
  profit(){
    let that = this;

    app.http({
      url: app.api.post_index_videoReward,
      data: {},
      //method: 'get',
    })
      .then((res) => {
        let requestData = res.data.data;
        that.setData({
          videoReward: requestData
        })
        // console.log("requestData---", requestData);
      })
      .catch((err) => {

      });
  },

  // 去赚喵呗
  jump(e) {
    if (this.data.infoData.is_proceeds == 0){ // 如果是普通用户
      // 传空code
      wx.navigateTo({
        url: '/pages/rights-and-interests/opening-privilege/opening-privilege?isKCode=' + true
      })
    } else {
      wx.switchTab({
        url: '/pages/tabBar/index/index',
      });
    }
  },

  // 刷新用户信息
  updateUserInfo(){
    let that = this;
    wx.login({
      success(res) {
        let token = wx.getStorageSync('token');
        let header;
        if (token) {
          header = {
            Authorization: token,
          };
        } else {
          header = {};
        }
        if (res.code) {
          app.http({
            url: '/user/login/wxLogin',
            data: {
              code: res.code,
            },
            header: header,
          })
            .then((res) => {
              that.setData({
                infoData: res.data.data
              })
              store.setState({
                infoData: res.data.data,
                token: res.data.data.token,
              });
              if (res.data.data.phone == '0') {
                store.setState({ loginType: 1 });
              } else {
                store.setState({ loginType: 0 });
              }
              wx.setStorageSync('infoData', res.data.data);
              wx.setStorageSync('token', res.data.data.token);
              that.globalData.checkLogin = true;

              //由于这里是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (that.checkLoginReadyCallback) {
                that.checkLoginReadyCallback(res);
              }
            })
            .catch((err) => {});
        }
      },
      fail: (err) => {},
    });
  },

  // 跳转喵币
  withdrawal(){
    wx.navigateTo({
      url: '/pages/catCoin/catCoin'
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
