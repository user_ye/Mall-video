// pages/order/orderAfterSale/orderAfterSale.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		orderId: null, // 订单id
		good_id: null, // 商品的id
		order_goods_id: null, // 订单商品的id
		// price: null, // 商品单价价格
		total: null, // 订单总价
		numberOfApplications: null, // 申请数量

		token: '',

		topTabsOnChangeIndex: 0, // 选择退货退款还是换货索引，0 退货退款，1 换货

		// 退款原因
		reasonForRefundArray: [
			'请选择退款原因',
			'效果不好/不喜欢',
			'质量问题',
			'卖家发错货',
			'商品少件/破损/污渍等',
		],
		reasonForRefundIndex: 0, // 退款原因索引

		// 换货原因
		exchangeGoodsArray: [
			'请选择换货原因',
			'效果不好/不喜欢',
			'质量问题',
			'卖家发错货',
			'商品少件/破损/污渍等',
		],
		exchangeGoodsIndex: 0, // 换货原因索引

		testTab: ['退货退款', '换货'],

		fileList: [], // 图片集合
		uploadFileList: [], // 上传的图片集合
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			orderId: options.id,
			// price: options.price,
			total: options.total,
			numberOfApplications: options.num,
			order_goods_id: options.order_goods_id,
			good_id: options.good_id,
		});
		console.log("options.num---", options.num);
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		let token = wx.getStorageSync('token');
		this.setData({
			token: token,
		});
	},

	// 选择退货退款还是换货索引
	topTabsOnChange(e) {
		let index = e.detail.index;
		this.setData({
			topTabsOnChangeIndex: index,
		});
	},

	// 提交
	submitBtn() {
		let that = this;

		let topTabsOnChangeIndex = this.data.topTabsOnChangeIndex; // 选择 退货退款 还是 换货 索引
		let type = null;
		let apply_reason = ''; // 用户申请理由

		// topTabsOnChangeIndex 0 退货退款，1 换货
		// type	1 换货，2 退货退款

		if (topTabsOnChangeIndex == 0) {
			// 退货退款
			type = 2;
			apply_reason = this.data.reasonForRefundArray[
				this.data.reasonForRefundIndex
			];
			if (this.data.reasonForRefundIndex == 0) {
				wx.showToast({
					title: '请选择退货退款原因',
					icon: 'none',
					duration: 1500,
				});
				return;
			}
		} else if (topTabsOnChangeIndex == 1) {
			// 换货
			type = 1;
			apply_reason = this.data.exchangeGoodsArray[
				this.data.exchangeGoodsIndex
			];
			if (this.data.exchangeGoodsIndex == 0) {
				wx.showToast({
					title: '请选择换货原因',
					icon: 'none',
					duration: 1500,
				});
				return;
			}
		}

		let param = {
			oid: that.data.orderId,
			order_goods_id: that.data.order_goods_id,
			good_id: that.data.good_id,
			type: type, // 1换货，2退货退款
			cover: that.data.uploadFileList.join(','), //	上传的图片，需要使用英文逗号切割
			price: that.data.total * 1, // 用户填写的申请退款金额
			apply_reason: apply_reason, // 用户申请理由
			num: this.data.numberOfApplications, // 申请数量
		};

		app.http({
			url: app.api.service_index_apply,
			data: param,
			//method: 'get',
		})
			.then((res) => {
				// // 跳转订单管理
				// wx.navigateTo({
				//   url: '/pages/order/orderManage/orderManage'
				// })
				wx.showModal({
					title: '提示',
					content: '申请成功',
					showCancel: false, // 是否显示取消按钮，默认为 true
					success(res) {
						let pages = getCurrentPages(); //获取所有页面
						let currentPage = null; //当前页面
						let prevPage = null; //上一个页面
						if (pages.length >= 2) {
							currentPage = pages[pages.length - 1]; //获取当前页面，将其赋值
							prevPage = pages[pages.length - 2]; //获取上一个页面，将其赋值
						}
						if (prevPage) {
							let isRefresh = prevPage.data.isRefresh; //这是拿到了上一页js中data里面定义的 isRefresh
							isRefresh = true;
							prevPage.setData({
								isRefresh: isRefresh, //将想要传的信息赋值给上一个页面data中的值
							});
						}

						if (res.confirm) {
							wx.navigateBack({
								delta: 1,
							});
						} else if (res.cancel) {
						}
					},
				});
			})
			.catch((err) => {});
	},

	// 退款原因普通选择器
	bindPickerChangeEeasonForRefund: function (e) {
		this.setData({
			reasonForRefundIndex: e.detail.value,
		});
	},

	// 换货原因普通选择器
	bindPickerChangeExchangeGoods: function (e) {
		this.setData({
			exchangeGoodsIndex: e.detail.value,
		});
	},

	// 上传图片
	afterRead(event) {
		wx.showLoading({
			title: '上传中...',
		});

		let that = this;
		const { file } = event.detail;
		// 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
		const { fileList = [] } = this.data;
		fileList.push({ ...file, deletable: true });
		this.setData({ fileList });

		let qm = app.qm({ type: 'image' });
		wx.uploadFile({
			url: app.api_url + app.api.api_index_uploads,
			filePath: file.path,
			name: 'img',
			header: {
				Authorization: that.data.token,
				'content-type': 'application/x-www-form-urlencoded',
			},
			formData: qm,
			success: (res) => {
				if (res.data) {
					let requestData = JSON.parse(res.data);
					that.data.uploadFileList.push(requestData.data.result);
					that.setData({
						uploadFileList: that.data.uploadFileList,
					});
				} else {
					wx.showToast({
						title: '服务器报错!',
						icon: 'none',
						duration: 1500,
					});
				}
				wx.hideLoading();
			},
			fail: (err) => {},
		});
	},
	// 删除图片
	imgDel(e) {
		let index = e.detail.index;
		const { fileList = [], uploadFileList = [] } = this.data;
		fileList.splice(index, 1);
		uploadFileList.splice(index, 1);
		this.setData({
			fileList,
			uploadFileList,
		});
	},

	// 金额输入框失去焦点时
	bindBlurPrice(e) {
		let value = e.detail.value;
		this.setData({
			total: value,
		});
	},

	// 数量
	bindBlurNumberOfApplications(e) {
		let value = e.detail.value;
		this.setData({
			numberOfApplications: value,
		});
	},
});
