/*
 * @Author: 红尘痴子
 * @Date: 2020-07-07 11:43:05
 * @LastEditTime: 2020-08-04 19:31:37
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\order\orderManage\orderManage.js
 * @https://www.jiangcan95.com/
 */

// pages/order/orderManage/orderManage.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		testTab: [
			{ id: 0, text: '全部' },
			{ id: 1, text: '待付款' },
			{ id: 2, text: '待发货' },
			{ id: 3, text: '待收货' },
			{ id: 4, text: '待评价' },
		], //tab标签名字
		active: 0, //tab选中标签
		limit: 10, //分页默认值
		page: 1, //分页默认值
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页

		list: [],
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.setData({
			active: parseInt(options.index),
		});
		// that.get_order_list();
	},

	onShow() {
		this.get_order_list();
	},

	// 评价按钮
	evaluateBtn(e) {
		let item = e.currentTarget.dataset.item;
		let index = e.currentTarget.dataset.index;

		wx.navigateTo({
			url:
				'/pages/order/commodityEvaluate/commodityEvaluate?item=' +
				JSON.stringify(item),
		});
	},

	// 确认收货
	confirmReceipt(e) {
		let that = this;

		let item = e.currentTarget.dataset.item;
		let index = e.currentTarget.dataset.index;

		wx.showModal({
			title: '提示',
			content: '是否确认收货？',
			showCancel: true, // 是否显示取消按钮，默认为 true
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.order_index_receipt,
						data: {
							id: item.id,
						},
						//method: 'get',
					})
						.then((res) => {
							that.data.list[index].status = 4;
							that.setData({
								list: that.data.list,
							});
							wx.showToast({
								title: '收货成功',
								icon: 'success',
								duration: 1500,
							});
						})
						.catch((err) => {});
				} else if (res.cancel) {
				}
			},
		});
	},

	// 取消订单
	cancellationOfOrder(e) {
		let that = this;

		let item = e.currentTarget.dataset.item;
		let index = e.currentTarget.dataset.index;

		wx.showModal({
			title: '提示',
			content: '是否取消订单？',
			showCancel: true, // 是否显示取消按钮，默认为 true
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.order_index_cancel,
						data: {
							id: item.id,
						},
						//method: 'get',
					})
						.then((res) => {
							that.data.list[index].status = 0;
							that.setData({
								list: that.data.list,
							});
							wx.showToast({
								title: '取消成功',
								icon: 'success',
								duration: 1500,
							});
						})
						.catch((err) => {});
				} else if (res.cancel) {
				}
			},
		});
	},

	// 付款
	payment(e) {
		let that = this;
		let item = e.currentTarget.dataset.item;
		let index = e.currentTarget.dataset.index;

		app.http({
			url: app.api.order_index_payment,
			data: {
				id: item.id,
			},
			//method: 'get',
		})
			.then((res) => {
				wx.requestPayment({
					//调起支付
					//下边参数具体看微信小程序官方文档
					timeStamp: res.data.data.data.timeStamp,
					nonceStr: res.data.data.data.nonceStr,
					package: res.data.data.data.package,
					signType: res.data.data.data.signType,
					paySign: res.data.data.data.paySign,
					success(res) {
						that.data.list[index].status = 2;
						that.setData({
							list: that.data.list,
						});
						wx.showToast({
							title: '付款成功',
							icon: 'success',
							duration: 1500,
						});
					},
					fail(err) {},
				});
			})
			.catch((err) => {});
	},

	// tab标签切换
	onChange(event) {
		let that = this;
		that.setData({
			active: event.detail.index,
		});
		that.get_order_list();
	},

	// 获取订单列表
	get_order_list() {
		let that = this;
		wx.showLoading({
			title: '加载中',
			mask: true,
		});
		app.http({
			url: app.api.get_order_list,
			data: {
				status: that.data.active,
				limit: that.data.limit,
				page: that.data.page,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				that.setData({
					list:
						that.data.page == 1
							? list
							: that.data.list.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
				wx.hideLoading();
			})
			.catch((err) => {});
	},
	// 跳到详情
	jump(ev) {
		let that = this;
		let id = ev.currentTarget.dataset.id
		console.log("id---", id);
		wx.navigateTo({
			url: `/pages/order/orderDetail/orderDetail?id=${id}`,
		});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.get_order_list();
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
});
