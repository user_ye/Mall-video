// pages/order/paymentOrder/paymentOrder.js
let app = getApp();

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		//当textarea获取焦点时自适应高度，失去焦点时不自适应高度
		//自适应高度时，style中的height无效
		auto_height: true,
		// num: 22,
		selectPaymentType: 1, // 选择的支付类型，1微信支付，2余额支付
		discRollup: false, //优惠卷弹窗
		couponIcon:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/mall/%E5%9B%BE%E6%A0%87%EF%BC%8F%E8%B4%AD%E7%89%A9%E5%88%B8%402x.png', //优惠卷图标
		selectCouponIndex: 1, //选择购物券, 1使用, 0不使用
		// chooseShopping: 1,
		video_id: '',
		goodImg: '', // 商品图片
		adders: '', // 地址
		lives: '', // 直播商品跳转过来的

		requestGoodsData: {}, // 请求的商品信息
		introspectionIsEnough: 0, // 自省购
		showShoppingVolume: 0, // 显示出来的购物卷
		paymentAmount: 0, // 计算后的付款金额
		// options
		goodsArray: [], // 商品信息

		// 通过后端接口计算的各种价格
		requestComputeData:{
			sum_money: 0, // 总支付金额
			money: 0,  // 实际支付金额
			wallet: 0, // 购物券抵扣金额
			wallet_money: 0,  // 购物券余额
			purchase: 0, // 自购省抵扣金额
		},

		collectCouponsLayer: false, // 是否显示领取优惠卷弹窗
		couponList:[], // 优惠卷列表
		couponRadioIndex: -1, // 优惠卷索引
		selectCouponItem:"", // 选择的优惠卷
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		console.log('options---', options);
		wx.showLoading({
			title: '加载中',
		});
		let that = this;
		// let attrs = JSON.parse(options.goodsArray);
		let goodsArray = JSON.parse(options.goodsArray);
		console.log('goodsArray---', goodsArray);
		that.setData({
			goodsArray: goodsArray,
			productId: options.id,
			price_share: options.price_share,
			name: options.name,
			video_id: options.videoid,
			activ: options.activ,
			puid: app.store.getState().uid,
			activity_id: app.store.getState().activity_id,
			lives: options.lives ? options.lives : '',
		});
		setTimeout(() => {
			that.setData({
				num: options.stepper,
			});
		}, 1000);
		that.getCommodityInfo(options.id, options.stepper, goodsArray[0].id);

		// 获取优惠卷列表
		this.getCouponList({
			id: options.id,
		})
	},

	// 立即领劵
	collectCoupons(){
		console.log("立即领劵---");
		this.setData({
			collectCouponsLayer: !this.data.collectCouponsLayer,
		});
	},

	// 显示或关闭立即领劵弹层
	closeCollectCoupons() {
		// this.data.selectCouponIndex == 1  ||
		// if (!this.data.couponList.length){
		// 	console.log("显示或关闭立即领劵弹层---");
		// 	return ;
		// }
		this.setData({
			collectCouponsLayer: !this.data.collectCouponsLayer,
		});
	},

	// 点击优惠卷
	bindChangeRadioCoupon(ev){
		let item = ev.currentTarget.dataset.item;
		let index = ev.currentTarget.dataset.index;
		console.log('item---',item)
		console.log('index---',index)
		if (this.data.couponRadioIndex == index){
			this.setData({
				couponRadioIndex: -1,
				selectCouponItem: ""
			})
		} else {
			this.setData({
				couponRadioIndex: index,
				selectCouponItem: item,
			})
		}
		// 计算价格---请求接口计算
		this.requestCompute();
	},

	// 获取优惠卷列表
	getCouponList(param){
		let that = this;
		app.http({
			url: app.api.mallIndexApply_coupon,
			data: {
				id: param.id, // 商品id
			},
		})
			.then((res) => {
				let requestData = res.data.data;
				requestData.map(mapRes =>{
					mapRes.start_time = app.util.timestampToTime(mapRes.start_time);
					mapRes.end_time = app.util.timestampToTime(mapRes.end_time);
				})
				that.setData({
					couponList: requestData
				})
				console.log('获取优惠卷列表---requestData---',requestData);
			})
			.catch((err) => {});
	},

	// 获取商品信息
	getCommodityInfo(id, num, attr) {
		let that = this;
		let data = {};
		data = {
			id: id,
			num: num,
			attr_id: attr,
			lives: that.data.lives ? that.data.lives : '',
		};
		app.http({
			url: app.api.get_mall_payData,
			data: data,
			//method: 'get',
		})
			.then((res) => {
				let requestData = res.data.data;
				console.log('获取商品信息---',requestData);

				let price = this.data.goodsArray[0].price; // 单个商品价格
				let selectNumber = this.data.goodsArray[0].selectNumber; // 单个商品选择的数量

				// parseFloat(introspectionIsEnough).toFixed(2)
				// 计算后的自购省 = 单个商品自省购 * 单个商品的数量，这里未考虑多个商品的情况
				let introspectionIsEnough = requestData.purchase_money ? requestData.purchase_money * 1 * selectNumber : 0;
				// 显示出来的购物卷 = 单个商品折扣后的购物卷 * 单个商品数量 - 计算后的自购省，这里未考虑多个商品的情况
				let showShoppingVolume = requestData.wallet_money ? requestData.wallet_money * 1 * selectNumber - introspectionIsEnough : 0;

				// 如果折扣后的购物卷 || 计算后的购物卷 >= 账户中购物卷余额
				if (showShoppingVolume >= requestData.wallet ? requestData.wallet : 0){
					showShoppingVolume = requestData.wallet
				}

				// 支付价格 = 商品单价 * 数量 + 运费 - 单个自购省 * 数量 - 单个商品折扣后的购物卷 * 数量
				// if 账户中购物卷余额 >= 商品单价 * 数量 + 运费 - 单个商品自购省 * 数量
				// if 商品单价 * 数量 + 运费 - 自购省 >= 购物卷 * 数量 则 (商品单价 * 数量 + 运费 - 自购省) - 购物卷 * 数量
				// if 购物卷 * 数量 <
				// 支付价格2 = 商品单价 * 数量 + 运费 - 计算后的购物卷

				// 支付价格 = 商品单价 * 数量 + 运费 - 单个自购省 * 数量 - 单个商品折扣后的购物卷 * 数量
				let paymentAmount;
				// 商品单价 * 数量 + 运费 - 计算后的自购省
				let goodPriceSelectNumFreight = price * 1 * selectNumber + requestData.freight * 1 - introspectionIsEnough;
				// console.log('商品单价 requestData.price---',price)
				// console.log('数量 selectNumber---',selectNumber)
				// console.log('运费 requestData.freight---',requestData.freight)
				// console.log('计算后的自购省 introspectionIsEnough---',introspectionIsEnough)
				// console.log('支付价格 goodPriceSelectNumFreight---',goodPriceSelectNumFreight)

				// 如果购物卷余额 >= 支付金额;
				// if (requestData.wallet ? requestData.wallet : 0 >= goodPriceSelectNumFreight){
				// 	paymentAmount = 0
				// } else {
				// 	paymentAmount = goodPriceSelectNumFreight - requestData.wallet * 1;
				// }

				paymentAmount = goodPriceSelectNumFreight - showShoppingVolume
				paymentAmount = paymentAmount < 0 ? paymentAmount * -1 : paymentAmount

				that.setData({
					requestGoodsData: requestData, // 请求的商品信息
					adders: requestData.adders, // 默认收货地址
					introspectionIsEnough: parseFloat(introspectionIsEnough).toFixed(2), // 自省购
					showShoppingVolume: parseFloat(showShoppingVolume).toFixed(2), // 计算后的购物卷
					paymentAmount: parseFloat(paymentAmount).toFixed(2), // 计算后的付款金额
				})

				that.requestCompute({
					num: that.data.goodsArray[0].selectNumber
				});

				wx.hideLoading();
			})
			.catch((err) => {});
	},
	// 选择数量
	onChange(e) {
		let index = e.currentTarget.dataset.index;
		let goodsArray = this.data.goodsArray;

		let num = e.detail; //数量
		goodsArray[index].selectNumber = num;
		this.setData({
			goodsArray: goodsArray
		})

		// // 调用计算价格方法
		// this.calcThePrc({
		// 	goodsArray:goodsArray,
		// 	index:index
		// });

		// 计算价格---请求接口计算
		// this.requestCompute({
		// 	num:num
		// });
		// 计算价格---请求接口计算
		this.requestCompute();
	},

	// 计算价格---请求接口计算
	// requestCompute(param){
	requestCompute(){
		// console.log('请求接口计算---');
		let that = this;
		app.http({
			url: app.api.mallIndexSumTotal,
			data: {
				id: that.data.productId,// 商品id
				attr_id: that.data.requestGoodsData.attr.id, //商品规格id
				// num: param.num, // 数量
				num: that.data.goodsArray[0].selectNumber, // 数量
				lives:that.data.lives ? that.data.lives : '',
				// wallet: param.selectCouponIndex ? param.selectCouponIndex : that.data.selectCouponIndex, // 是否使用购物券 1 使用 2 不使用
				wallet: that.data.selectCouponIndex, // 是否使用购物券 1 使用 2 不使用
				coupon: that.data.selectCouponItem ? that.data.selectCouponItem.id : '', // 选择的优惠卷id
			},
		})
			.then((res) => {
				let requestData = res.data.data;
				// console.log("请求接口计算---res---", requestData);
				that.setData({
					requestComputeData: requestData
				})
			})
			.catch((err) => {});
	},

	// 计算价格
	calcThePrc(param) {
		let goodsArray = param.goodsArray; // 商品信息数组
		let selectNumber = goodsArray[param.index].selectNumber; // 商品信息数组
		let price = this.data.goodsArray[param.index].price; // 单个商品价格

		// 计算后的自购省 = 单个商品自省购 * 单个商品的数量，这里未考虑多个商品的情况
		let introspectionIsEnough = this.data.requestGoodsData.purchase_money ? this.data.requestGoodsData.purchase_money * selectNumber : 0;

		let wallet = this.data.requestGoodsData.wallet ? this.data.requestGoodsData.wallet : 0; // 账户中购物卷余额
		// 计算后的购物卷 = 单个商品折扣后的购物卷 * 单个商品数量 - 计算后的自购省，这里未考虑多个商品的情况
		let showShoppingVolume = this.data.requestGoodsData.wallet_money ? this.data.requestGoodsData.wallet_money * selectNumber - introspectionIsEnough : 0;

		// 如果折扣后的购物卷 || 计算后的购物卷 >= 账户中购物卷余额
		if (showShoppingVolume >= wallet ? wallet : 0){
			showShoppingVolume = this.data.requestGoodsData.wallet
		}


		// 支付价格 = 商品单价 * 数量 + 运费 - 单个自购省 * 数量 - 单个商品折扣后的购物卷 * 数量
		let paymentAmount = 0;
		// 商品单价 * 数量 + 运费 - 计算后的自购省
		let goodPriceSelectNumFreight = price * 1 * selectNumber + this.data.requestGoodsData.freight * 1 - introspectionIsEnough;

		// 如果使用购物卷
		if (this.data.selectCouponIndex == 1){
			// 如果购物卷余额 >= 支付金额
			// if (this.data.requestGoodsData.wallet >= goodPriceSelectNumFreight){
			// 	paymentAmount = 0
			// } else {
			// 	paymentAmount = goodPriceSelectNumFreight - this.data.requestGoodsData.wallet * 1;
			// }

			paymentAmount = goodPriceSelectNumFreight - showShoppingVolume
			paymentAmount = paymentAmount < 0 ? paymentAmount * -1 : paymentAmount
		} else { // 如果不使用购物卷
			paymentAmount = goodPriceSelectNumFreight
		}


		this.setData({
			introspectionIsEnough: parseFloat(introspectionIsEnough).toFixed(2), // 自省购
			showShoppingVolume: parseFloat(showShoppingVolume).toFixed(2), // 显示出来的购物卷
			paymentAmount: parseFloat(paymentAmount).toFixed(2), // 支付金额
		})
	},
	// 支付
	pay() {
		let that = this;

		console.log("支付----adders----",that.data.adders);
		// return ;
		if (!this.data.adders) {
			wx.showToast({
				title: '请选择收货地址',
				icon: 'none',
				duration: 2000,
			});
			return;
		}


		let activity_log_id = that.data.activ || '', //活动拼团列表id
			video_id = that.data.video_id, //视频id
			puid = that.data.puid || '', //分享id
			lives = that.data.lives || ''; //直播商品跳转

		console.log("that.data.adders---", that.data.adders);
		let data = {
			id: that.data.productId,// 商品id
			num: that.data.goodsArray[0].selectNumber, // 购买数量
			pay_type: that.data.selectPaymentType, // 支付类型  1  微信 ，  2  余额
			attr_id: that.data.requestGoodsData.attr.id, //商品规格id
			wallet: that.data.selectCouponIndex, // 用户是否使用购物券 0 不使用优惠券 1 使用
			phone: that.data.adders.tel, // 收货手机
			address: that.data.adders.province + that.data.adders.city + that.data.adders.area + ' ' + that.data.adders.address, // 收货地址
			name: that.data.adders.name, // 收货名
			adders_id: that.data.adders.id, //地址id, // 用户当前所选的支付地址 id 支付详情可以拿取到
			desc: that.data.orderNotesTextareaValue || '', //订单备注
			coupon: that.data.selectCouponItem.id, // 选择的优惠卷id
		};
		//判断支付类型
		//活动拼团
		if (activity_log_id || video_id) {
			// 支付页面  请求参数  类型  3 - 活动商品 -入团
			data.activity_log_id = activity_log_id;
			data.video_id = video_id;
			//活动商品
		} else if (that.data.activity_id) {
			// 支付页面 请求参数 类型   4  - 视频分享常规商品
			data.activity_log_id = that.data.activity_id;
			data.puid = puid;
			// /分享者
		} else if (video_id) {
			// 支付页面 请求参数 类型   4  - 视频分享常规商品
			data.video_id = video_id;
			// /分享者
		} else if (puid) {
			// 支付页面 请求参数 类型   5  - 用户分享常规商品
			data.puid = puid;
			data.activity_log_id = that.data.activity_id;
		} else if (lives) {
			//直播商品跳转
			data.lives = lives;
		} else {
			// 常规商品;
			// 支付页面  请求参数 类型1  - 常规商品 直接购买
			data = data;
		}
		wx.showToast({
			title: '加载中...',
			icon: 'loading',
			mask: true,
		});
		app.http({
			url: app.api.get_mall_indexPay,
			data: data,
			//method: 'get',
		})
			.then((res) => {
				var orderId = res.data.data.orderid;
				let log_id = res.data.data.log_id;
				// 如果是微信支付
				if (res.data.data && orderId && res.data.data.pay_type == 1) {
					var data = res.data.data;
					wx.requestPayment({
						//调起支付
						//下边参数具体看微信小程序官方文档
						timeStamp: data.timeStamp,
						nonceStr: data.nonceStr,
						package: data.package,
						signType: data.signType,
						paySign: data.paySign,
						success(res) {
							// 微信支付ok 直接跳转到我的订单列表
							if (res.errMsg == 'requestPayment:ok') {
								wx.showToast({
									title: '支付成功!',
									icon: 'success',
									duration: 2000,
								});
								app.store.setState({
									uid: '',
								});
								that.setData({
									puid: '',
								});
								wx.requestSubscribeMessage({
									tmplIds: [
										'FddT6-1bVu2lfiggUR7FVmS0mwrvTlGIgksy6xh94XE',
									],
									success(res) {},
								});
								if (that.data.requestGoodsData.is_activity == 1) {
									setTimeout(() => {
										wx.redirectTo({
											url: `/pages/successfullyOpened/successfullyOpened?id=${log_id}`,
										});
									}, 2000);
								} else {
									setTimeout(() => {
										wx.redirectTo({
											url: `/pages/order/orderManage/orderManage?index=0`,
										});
									}, 2000);
								}
							}
						},
						fail(res) {},
					});
					// 如果是余额支付
				} else {
					wx.showToast({
						title: '支付成功!',
						icon: 'success',
						duration: 2000,
					});
					app.store.setState({
						uid: '',
					});
					that.setData({
						puid: '',
					});
					wx.requestSubscribeMessage({
						tmplIds: [
							'FddT6-1bVu2lfiggUR7FVmS0mwrvTlGIgksy6xh94XE',
						],
						success(res) {},
					});
					if (that.data.requestGoodsData.is_activity == 1) {
						setTimeout(() => {
							wx.redirectTo({
								url: `/pages/successfullyOpened/successfullyOpened?id=${log_id}`,
							});
						}, 2000);
					} else {
						setTimeout(() => {
							wx.redirectTo({
								url: `/pages/order/orderManage/orderManage?index=0`,
							});
						}, 2000);
					}
				}
			})
			.catch((err) => {
				// wx.showToast({
				// 	title: err.data.msg || '麻烦重新尝试',
				// 	icon: 'waiting',
				// 	duration: 1500,
				// });
			});
	},
	// 选择支付方式
	selectPaymentTypeFun(e) {
		let index = e.currentTarget.dataset.index;
		this.setData({
			selectPaymentType: index,
		});
	},

	// 确认使用余额支付
	payt() {
		let that = this;
		wx.showModal({
			title: '提示',
			content: '确认使用余额支付',
			success(res) {
				if (res.confirm) {
					//console.log('用户点击确定')
					that.pay();
				} else if (res.cancel) {
					//console.log('用户点击取消') } }
				}
			},
		});
	},
	// 订单备注输入框方法
	areablur: function () {
		this.setData({
			auto_height: false,
		});
	},
	areafocus: function () {
		this.setData({
			auto_height: true,
		});
	},
	// 订单备注键盘输入时事件
	commentbindinput(e) {
		this.setData({
			orderNotesTextareaValue: e.detail.value,
		});
	},
	// 选择收货地址
	address() {
		let that = this;
		let index = 0;
		wx.navigateTo({
			url: `/pages/address/addressList/addressList?id=${index}`,
		});
	},
	// 优惠卷弹窗
	onClose() {
		let that = this;
		that.setData({
			discRollup: !that.data.discRollup,
		});
	},
	// 选择优惠卷 coupon
	selectCoupon(e) {
		let index = e.currentTarget.dataset.index;
		let paymentAmountAA = this.data.paymentAmount;
		// console.log('selectCoupon---',index);
		if (index == 0 && index != this.data.selectCouponIndex){
			paymentAmountAA = paymentAmountAA * 1 + this.data.showShoppingVolume * 1
			// console.log('paymentAmount-111--', paymentAmountAA);
		} else if (index == 1 && index != this.data.selectCouponIndex){
			paymentAmountAA = this.data.paymentAmount * 1 - this.data.showShoppingVolume * 1;
			// console.log('paymentAmount--222-', paymentAmountAA);
		}
		this.setData({
			selectCouponIndex: index, // 选择购物卷索引
			paymentAmount: parseFloat(paymentAmountAA).toFixed(2),
		});

		this.requestCompute({
			selectCouponIndex:index,
			num: this.data.goodsArray[0].selectNumber
		})
	},

	// 优惠卷弹层完成
	carryOut() {
		let that = this;
		that.setData({
			// chooseShopping: that.data.chooseShoppingVchr,
			discRollup: !that.data.discRollup,
		});
		// that.calcThePrc();
	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {
		let that = this;
		app.store.setState({
			uid: '',
		});
		that.setData({
			puid: '',
		});
	},
});
