// pages/order/afterSaleDetails/afterSaleDetails.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		// 步骤条提示文字 --  初始化文字
		stepsTipsText: [
			'提交申请',
			'商家已审核',
			'待买家发货',
			'待退款',
			'完成',
		],
		stepsTipsIndex: 0, // 步骤条索引

		requestExpressArray: [], // 请求的快递公司
		ExpressArray: [], // 快递公司普通选择器，分割好的数组
		ExpressIndex: 0, // 快递公司普通选择器索引

		courierNumberValue: '', // 输入的快递单号
		orderId: null, // 订单id
		goodsData: {}, // 上个页面传过来的商品数据
		requestAfterSaleData: {}, // 请求到的售后数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		if (options.goods) {
			let goods = JSON.parse(options.goods);
			this.setData({
				goodsData: goods,
			});
		}
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.logistics();
	},

	// 售后详情
	afterSaleDetails() {
		let that = this;
		that.data.ExpressArray = [];
		app.http({
			url: app.api.service_index_get,
			data: {
				id: that.data.goodsData.id || 16,
				// id:15,
			},
			//method: 'get',
		})
			.then((res) => {
				that.data.requestExpressArray.map((mapRes) => {
					if (res.data.data.oneChangeExpress) {
						if (mapRes.id == res.data.data.oneChangeExpress.id) {
							res.data.data.oneChangeExpressName = mapRes.company;
						}
					}
					if (res.data.data.oneMerchantExpress) {
						if (mapRes.id == res.data.data.oneMerchantExpress.id) {
							res.data.data.oneMerchantExpressName =
								mapRes.company;
						}
					}
				});

				let stepsTipsText = [];
				let stepsTipsIndex = '';

				// 情况 四种 ：   1.退款退货-正常流程    2. 换货-正常流程    3    提交被拒绝     4  商家 没有收到货  则显示 待商家处理

				var linshi = [];
				var index = 1;
				// 如果状态等于  6  已拒绝
				if (res.data.data.status == 6) {
					linshi = ['提交申请', '商家已拒绝'];
					index = 1;
					// 类型 为  2   表示  退款退货   ||
				} else if (res.data.data.type == 2) {
					linshi = [
						'提交申请',
						'待审核',
						res.data.data.status > 3 ? '已发货' : '发货中',
						'处理中',
						'待退款',
						'完成',
					];
					index = res.data.data.status;
					// 类型 为2 类型1  表示 换货
				} else if (res.data.data.type == 1) {
					linshi = [
						'提交申请',
						'待审核',
						'发货中',
						'处理中',
						'已发货',
						'完成',
					];
					index = res.data.data.status;
				}
				that.setData({
					stepsTipsText: linshi,
					stepsTipsIndex: index,
				});

				/*
          if (res.data.data.status == 1){
            stepsTipsText = [
              '提交申请',
              '待商家审核',
              '商家已审核',
              '待退款',
              '完成',
            ];
            stepsTipsIndex = 1;
          } else if (res.data.data.status == 2){
            stepsTipsText = [
              '提交申请',
              '商家已审核',
              '待买家发货',
              '待退款',
              '完成',
            ];
            stepsTipsIndex = 1;
          } else if (res.data.data.status == 3){
            stepsTipsText = [
              '提交申请',
              '商家已审核',
              '待买家发货',
              '待卖家收货',
            ];
            stepsTipsIndex = 2;
          } else if (res.data.data.status == 4){
            if (res.data.data.type == 1){
              stepsTipsText = [
                '提交申请',
                '商家已审核',
                '待买家发货',
                '待卖家收货',
                '完成',
              ];
              stepsTipsIndex = 3;
            } else {
              stepsTipsText = [
                '提交申请',
                '商家已审核',
                '待买家发货',
                '待卖家发货',
                '完成',
              ];
              stepsTipsIndex = 3;
            }
          } else if (res.data.data.status == 5){
            stepsTipsText = [
              '提交申请',
              '商家已审核',
              '待买家发货',
              '待退款',
              '完成',
            ];
            stepsTipsIndex = 4;
          } else if (res.data.data.status == 6){
            stepsTipsText = [
              '提交申请',
              '商家已拒绝',
            ];
            stepsTipsIndex = 1;
          }

          that.setData({
            stepsTipsText:stepsTipsText,
            stepsTipsIndex:stepsTipsIndex,
          })
          */

				// res.data.data.map(resMap => {
				//   console.log('resMap----',resMap);
				// })
				that.setData({
					requestAfterSaleData: res.data.data,
				});
			})
			.catch((err) => {});
	},

	// 获取物流
	logistics() {
		let that = this;
		that.data.ExpressArray = [];
		app.http({
			url: app.api.service_index_logisticsList,
			data: {},
			//method: 'get',
		})
			.then((res) => {
				that.afterSaleDetails();
				that.setData({
					requestExpressArray: res.data.data,
				});
				res.data.data.map((mapRes) => {
					that.data.ExpressArray.push(mapRes.company);
					that.setData({
						ExpressArray: that.data.ExpressArray,
					});
				});
			})
			.catch((err) => {});
	},

	// 复制按钮
	copyBtn() {
		let name = this.data.requestAfterSaleData.address.name;
		let tel = this.data.requestAfterSaleData.address.tel;
		let address = this.data.requestAfterSaleData.address.address;
		let data = '收货人：' + name + ' ' + tel + ' ' + address;

		wx.setClipboardData({
			data: data,
			success(res) {
				wx.getClipboardData({
					success(res) {},
				});
			},
		});
	},

	// 监听input输入，输入的快递单号
	bindInputCourier(e) {
		let that = this;
		that.setData({
			courierNumberValue: e.detail.value,
		});
	},

	// 确认发货
	expressSubmitBtn() {
		let that = this;
		if (this.data.courierNumberValue == '') {
			wx.showToast({
				title: '请输入快递单号',
				icon: 'none',
				duration: 1500,
			});
		} else {
			let logisticsId = this.data.requestExpressArray[
				this.data.ExpressIndex
			].id; // 物流id
			// console.log('物流id',logisticsId);
			// console.log('输入的快递单号',this.data.courierNumberValue);
			// console.log('售后id',this.data.requestAfterSaleData.id);

			app.http({
				url: app.api.service_index_updateExchange,
				data: {
					id: that.data.requestAfterSaleData.id, // 售后id
					company_id: logisticsId, // 物流公司-id
					express_sn: that.data.courierNumberValue, // 快递单号
				},
				//method: 'get',
			})
				.then((res) => {
					wx.showModal({
						title: '提示',
						content: '发货成功',
						showCancel: false, // 是否显示取消按钮，默认为 true
						success(res) {
							if (res.confirm) {
								// console.log('用户点击确定')
								wx.navigateBack({
									delta: 1,
								});
								// 重新加载
								that.afterSaleDetails();
							} else if (res.cancel) {
							}
						},
					});
				})
				.catch((err) => {});
		}
	},

	// 快递公司普通选择器
	bindPickerChangeExpress: function (e) {
		this.setData({
			ExpressIndex: e.detail.value,
		});
	},
});
