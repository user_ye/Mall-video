/*
 * @Author: 红尘痴子
 * @Date: 2020-07-08 18:41:42
 * @LastEditTime: 2020-08-07 15:39:42
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\order\afterSalesList\afterSalesList.js
 * @https://www.jiangcan95.com/
 */
// pages/order/afterSalesList/afterSalesList.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		paginationList: [], // 分页列表数据
		pageNum: 1, // 第几页
		limit: 10, // 每页数量
		isMax: false, // 是否已显示最大数量
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.setData({
			paginationList: [], // 分页列表数据
			pageNum: 1, // 第几页
			limit: 10, // 每页数量
			isMax: false, // 是否已显示最大数量
		})
		this.requestList();
	},

	// 请求列表
	requestList() {
		let that = this;
		let paginationList = this.data.paginationList; // 分页列表数据
		let pageNum = this.data.pageNum; // 第几页
		let limit = this.data.limit; // 每页数量
		let isMax = this.data.isMax; // 是否已显示最大数量

		if (!isMax) {
			app.http({
				url: app.api.service_index_list,
				data: {
					page: pageNum, // 第几页
					limit: limit, // 每页数量
				},
			})
				.then((res) => {
					let listData = res.data.data.data; // 请求到的列表数据

					paginationList.push(...listData);
					that.setData({
						paginationList: paginationList,
					});

					let total = res.data.data.total; // 列表的总数量
					if (paginationList.length >= total) {
						isMax = true;
					}

					pageNum += 1;
					that.setData({
						paginationList: paginationList,
						isMax: isMax,
						pageNum: pageNum,
					});
				})
				.catch((err) => {});
		}
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.requestList();
	},

	// 点击商品
	clickCommodityItem(e) {
		let item = e.currentTarget.dataset.item;
		wx.navigateTo({
			url:
				'/pages/order/afterSaleDetails/afterSaleDetails?goods=' +
				JSON.stringify(item),
		});
	},
});
