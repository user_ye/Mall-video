// pages/order/orderDetail/orderDetail.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		orderDetailData: {}, // 请求到的订单详情数据
		orderDetailId: '', // 订单详情id

		isRefresh: false, // 是否需要刷新
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;

		// 176 待发货
		// 181 待付款

		let orderDetailId = options.id;
		that.get_order_get(orderDetailId);
		this.setData({
			orderDetailId: orderDetailId,
		});
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		if (this.data.isRefresh) {
			this.get_order_get(this.data.orderDetailId);
		}
	},

	// 订单详情
	get_order_get(id) {
		let that = this;
		app.http({
			url: app.api.get_order_get,
			data: {
				id: id,
			},
			//method: 'get',
		})
			.then((res) => {
				res.data.data.add_time = app.util.formatTimeYMDhms(
					res.data.data.add_time
				);
				that.setData({
					orderDetailData: res.data.data,
				});
			})
			.catch((err) => {});
	},

	// 取消订单
	cancellationOfOrder() {
		let that = this;

		wx.showModal({
			title: '提示',
			content: '是否取消订单？',
			showCancel: true, // 是否显示取消按钮，默认为 true
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.order_index_cancel,
						data: {
							id: that.data.orderDetailId,
						},
						//method: 'get',
					})
						.then((res) => {
							wx.navigateBack({
								delta: 1,
							});
						})
						.catch((err) => {});
				} else if (res.cancel) {
				}
			},
		});
	},

	// 确认收货
	confirmReceipt() {
		let that = this;
		wx.showModal({
			title: '提示',
			content: '是否确认收货？',
			showCancel: true, // 是否显示取消按钮，默认为 true
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.order_index_receipt,
						data: {
							id: that.data.orderDetailId,
						},
						//method: 'get',
					})
						.then((res) => {
							wx.navigateBack({
								delta: 1,
							});
						})
						.catch((err) => {});
				} else if (res.cancel) {
				}
			},
		});
	},

	// 删除订单
	deleteOrder() {
		let that = this;
		wx.showModal({
			title: '提示',
			content: '是否删除订单？',
			showCancel: true, // 是否显示取消按钮，默认为 true
			success(res) {
				if (res.confirm) {
					app.http({
						url: app.api.order_index_del,
						data: {
							id: that.data.orderDetailId,
						},
						//method: 'get',
					})
						.then((res) => {
							wx.navigateBack({
								delta: 1,
							});
						})
						.catch((err) => {});
				} else if (res.cancel) {
				}
			},
		});
	},

	// 评价
	evaluate() {
		let that = this;
		wx.navigateTo({
			url:
				'/pages/order/commodityEvaluate/commodityEvaluate?id=' +
				this.data.orderDetailId,
		});
	},

	// 付款
	payment() {
		let that = this;
		app.http({
			url: app.api.order_index_payment,
			data: {
				id: that.data.orderDetailId,
			},
			//method: 'get',
		})
			.then((res) => {
				wx.requestPayment({
					//调起支付
					//下边参数具体看微信小程序官方文档
					timeStamp: res.data.data.data.timeStamp,
					nonceStr: res.data.data.data.nonceStr,
					package: res.data.data.data.package,
					signType: res.data.data.data.signType,
					paySign: res.data.data.data.paySign,
					success(res) {
						wx.navigateBack({
							delta: 1,
						});
					},
					fail(err) {},
				});
			})
			.catch((err) => {});
	},

	// 申请售后
	applyForAfterSale(e) {
		let price = e.currentTarget.dataset.price;
		let total = e.currentTarget.dataset.total;
		let good_id = e.currentTarget.dataset.good_id;
		let num = e.currentTarget.dataset.num;
		let order_goods_id = e.currentTarget.dataset.order_goods_id; // 单个商品的id
		wx.navigateTo({
			url:
				'/pages/order/orderAfterSale/orderAfterSale?id=' +
				this.data.orderDetailId +
				'&price=' + price +
				'&total=' + total +
				'&good_id=' + good_id +
				'&num=' + num +
				'&order_goods_id=' + order_goods_id,
		});
	},
	// 跳到详情页
	jump(e) {
		let id = e.currentTarget.dataset.id;
		wx.navigateTo({
			url: `/pages/productDetails/productDetails?id=${id}`,
		});
	},
	// 复制订单号
	order() {
		let that = this;
		wx.setClipboardData({
			data: that.data.orderDetailData.order_sn,
			success(res) {
				wx.getClipboardData({
					success(res) {},
				});
			},
		});
	},
});
