// pages/order/commodityEvaluate/commodityEvaluate.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		starsNumber: 5, // 星星数量
		starsTipsText: ['非常差', '差', '一般', '满意', '超赞'],
		fileList: [], // 图片集合
		uploadFileList: [], // 上传的图片集合
		orderItem: '', // 订单id
		discuss: '', // 评价内容
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let orderItem = JSON.parse(options.item);

		this.setData({
			orderItem: orderItem,
		});
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		let token = wx.getStorageSync('token');
		this.setData({
			token: token,
		});
	},

	// 星星数量
	starsNumberOnChange(event) {
		this.setData({
			starsNumber: event.detail,
		});
	},

	// 上传图片
	afterRead(event) {
		wx.showLoading({
			title: '上传中...',
		});

		let that = this;
		const { file } = event.detail;
		// 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
		const { fileList = [] } = this.data;
		// console.log('222----', {...file})
		fileList.push({ ...file, deletable: true });
		// console.log('3333', fileList)
		this.setData({ fileList });
		let qm = app.qm({ type: 'image' });
		wx.uploadFile({
			url: app.api_url + app.api.api_index_uploads,
			filePath: file.path,
			name: 'img',
			header: {
				Authorization: that.data.token,
				'content-type': 'application/x-www-form-urlencoded',
			},
			formData: qm,
			success: (res) => {
				let requestData = JSON.parse(res.data);
				that.data.uploadFileList.push(requestData.data.result);
				that.setData({
					uploadFileList: that.data.uploadFileList,
				});
				wx.hideLoading();
			},
			fail: (err) => {},
		});
	},
	// 删除图片
	imgDel(e) {
		let index = e.detail.index;
		const { fileList = [], uploadFileList = [] } = this.data;
		fileList.splice(index, 1);
		uploadFileList.splice(index, 1);
		this.setData({
			fileList,
			uploadFileList,
		});
	},

	// 提交
	btn() {
		let that = this;

		let param = {
			oid: that.data.orderItem.id, // 订单id
			id: that.data.orderItem.goods_id, //商品id
			level: that.data.starsNumber, //评分星级
			discuss: that.data.discuss, //评论内容
			cover: that.data.uploadFileList.join(','), //	上传的图片，需要使用英文逗号切割
		};

		if (param.discuss == '') {
			wx.showToast({
				title: '请输入评价内容',
				icon: 'none',
				duration: 1500,
			});
			return;
		}

		// return
		app.http({
			url: app.api.get_comment_add,
			data: param,
			//method: 'get',
		})
			.then((res) => {
				wx.showToast({
					title: '评价成功',
					icon: 'success',
					duration: 1500,
				});

				setTimeout(() => {
					wx.redirectTo({
						url: '/pages/order/orderManage/orderManage',
					});
				}, 1500);
			})
			.catch((err) => {});
	},

	// 评论内容
	commentbindinput(e) {
		this.setData({
			discuss: e.detail.value,
		});
	},
});
