/*
 * @Author: 红尘痴子
 * @Date: 2020-07-06 15:38:30
 * @LastEditTime: 2020-08-19 16:35:37
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\allComments\allComments.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		imgUrl: [],
		current_page: 0, //当前页
		last_page: 0, //总页数
		control: false, //是否还有下一页
		limit: 10, //分页默认值
		page: 1, //分页默认值
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		that.setData({
			productId: options.id,
		});
		that.get_mall_commentgetMalls(options.id);
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {},
	// 获取商品评论
	get_mall_commentgetMalls(id) {
		let that = this;
		app.http({
			url: app.api.get_mall_commentgetMalls,
			data: {
				goods_id: id,
				limit: that.data.limit,
				page: that.data.page,
			},
			//method: 'get',
		})
			.then((res) => {
				let list = res.data.data.data;
				list.map((item) => {
					item.tiem = app.util
						.formatTimeYMDhms(item.add_time)
						.slice(0, 10);
					return;
				});
				that.setData({
					commentgetMalls:
						that.data.page == 1
							? list
							: that.data.commentgetMalls.concat(list),
					control:
						res.data.data.current_page < res.data.data.last_page,
				});
			})
			.catch((err) => {});
	},
	/**
	 * 页面上拉触底事件的处理函数
	 */ onReachBottom: function () {
		let that = this;
		let control = that.data.control;
		let id = that.data.productId; //商品id
		if (control) {
			that.setData({
				page: that.data.page + 1,
			});
			that.get_mall_commentgetMalls(id);
		} else {
			wx.showToast({
				title: '没有更多了',
				icon: 'none',
			});
		}
	},
	// 页面刷新
	onPullDownRefresh: function () {
		let that = this;
		that.setData({
			limit: 10, //分页默认值
			page: 1, //分页默认值
			current_page: 0, //当前页
			last_page: 0, //总页数
			control: false, //是否还有下一页
		});
		that.onLoad();
		wx.stopPullDownRefresh();
	},
	// 预览图片
	previewPic(e) {
		let i = e.currentTarget.dataset.i,
			item = e.currentTarget.dataset.item;
		wx.previewImage({
			current: i, // 当前显示图片的http链接
			urls: item, // 需要预览的图片http链接列表
		});
	},
});
