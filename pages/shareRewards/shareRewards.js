/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 14:57:50
 * @LastEditTime: 2020-07-29 10:29:43
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\shareRewards\shareRewards.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		status: app.globalData.statusHeight, //头部占位高度
		navHeight: app.globalData.navHeight, //头部内容区
		balPic:
			'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/Mybalance/%E5%9B%BE%E6%A0%87%EF%BC%8F%E5%96%B5%E5%B8%81%402x.png', //余额图片
		active: 0,
		numOfMessages: 99,
		num: 0,

		requestShareReward: {}, // 分享奖励余额
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		that.shareReward(4); // type：2-拼团奖励 4-分享奖励
	},
	// 获取余额
	shareReward(type) {
		let that = this;
		app.http({
			url: app.api.wallet_sharingRewards,
			data: { type: type },
		})
			.then((res) => {
				that.setData({
					requestShareReward: res.data.data,
				});
			})
			.catch((err) => {});
	},
	// 跳到历史概况页面
	historicalOverview() {
		let type = 4;
		wx.navigateTo({
			url: `/pages/historicalOverview/historicalOverview?type=${type}`,
		});
	},
	// 跳到说明页面
	explanation() {
		let key = 'share';
		wx.navigateTo({
			url: `/pages/descrRulePage/descrRulePage?key=${key}`,
		});
	},
	// 跳到订单页面
	ordrPage(e) {
		let index = e.currentTarget.dataset.index;
		wx.navigateTo({
			url: `/pages/shareOrdr/shareOrdr?index=${index}`,
		});
	},
	// 返回上一页
	back() {
		wx.navigateBack({
			delta: 1,
		});
	},
});
