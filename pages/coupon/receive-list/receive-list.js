// pages/coupon/receive-list/receive-list.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabsData:[
      {
        id: 1,
        name: '未使用'
      },
      {
        id: 2,
        name: '已使用'
      },
      {
        id: 3,
        name: '已过期'
      },
    ],
    tabActive:0,

    requestData:"", // 请求到的完整数据
    isScreenInvalid:false, // 是否筛选失效的卷
    paginationList:[], // 分页列表数据
    pageNum: 1, // 第几页
    limit: 10, // 每页数量
    isMax:false, // 是否已显示最大数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.requestList();
  },

  // 点击tabs
  onChange(ev){
    // console.log("ev---", ev.detail.index);
    this.setData({
      tabActive: ev.detail.index
    })
    this.setData({
      isScreenInvalid:false, // 是否筛选失效的卷
    })
    this.resetPaging(); // 重置分页
    this.requestList(); // 领取的优惠卷列表
  },

  // 点击筛选失效
  clickScreenInvalid(){
    this.setData({
      isScreenInvalid: !this.data.isScreenInvalid
    })
    this.resetPaging(); // 重置分页
    this.requestList(); // 领取的优惠卷列表
  },

  // 领取的优惠卷列表
  requestList(){
    let that = this;
    let paginationList = this.data.paginationList; // 分页列表数据
    let pageNum = this.data.pageNum; // 第几页
    let limit = this.data.limit;  // 每页数量
    let isMax = this.data.isMax; // 是否已显示最大数量

    if (!isMax) {
      app.http({
        url: app.api.userCouponMyCouponList,
        data: {
          // page: pageNum, // 第几页
          // limit: limit, // 每页数量
          status: that.data.tabsData[that.data.tabActive].id,
          is_expire: that.data.isScreenInvalid == true ? 1 : 0
        },
      })
        .then((res) => {
          // console.log('请求列表 res.data.data--------',res.data.data);
          let requestData = res.data.data; // 请求到的完整数据
          let paginationList = res.data.data.data; // 请求到的列表数据
          // console.log('requestData--------',requestData);
          // paginationList.push(...paginationList);
          //
          // let total = res.data.data.total; // 列表的总数量
          // if (paginationList.length >= total) {
          //   isMax = true;
          //   console.log('---------------最大啦---------------')
          // }
          //
          // pageNum += 1;
          that.setData({
            requestData: requestData,
            paginationList: paginationList,
            // isMax: isMax,
            // pageNum: pageNum
          })

        })
        .catch((err) => {
          console.log(err);
        });
    }
  },

  // 重置分页
  resetPaging(){
    this.setData({
      paginationList:[], // 分页列表数据
      pageNum: 1, // 第几页
      limit: 10, // 每页数量
      isMax:false, // 是否已显示最大数量
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // this.requestList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
