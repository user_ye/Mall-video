// pages/coupon/coupon-manage/coupon-manage.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabTipsText: ['全部', '领取中', '已结束'],
    vanTabIndex: 0, // tabs索引

    paginationList:[], // 分页列表数据
    pageNum: 1, // 第几页
    limit: 10, // 每页数量
    isMax:false, // 是否已显示最大数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.resetPaging(); // 重置分页
    this.requestList();
  },

  // 重置分页
  resetPaging(){
    this.setData({
      paginationList:[], // 分页列表数据
      pageNum: 1, // 第几页
      limit: 10, // 每页数量
      isMax:false, // 是否已显示最大数量
    })
  },

  // 获取优惠卷列表
  requestList() {
    let that = this;
    let paginationList = this.data.paginationList; // 分页列表数据
    let pageNum = this.data.pageNum; // 第几页
    let limit = this.data.limit;  // 每页数量
    let isMax = this.data.isMax; // 是否已显示最大数量

    if (!isMax) {
      app.http({
        url: app.api.shopCouponCouponList,
        data: {
          page: pageNum, // 第几页
          limit: limit, // 每页数量
        },
      })
        .then((res) => {
          let listData = res.data.data.data; // 请求到的列表数据
          listData.map(mapRes =>{
            mapRes.start_time = app.util.timestampToTime(mapRes.start_time);
            mapRes.end_time = app.util.timestampToTime(mapRes.end_time);
          })
          paginationList.push(...listData);

          let total = res.data.data.total; // 列表的总数量
          if (paginationList.length >= total) {
            isMax = true;
            console.log('---------------最大啦---------------')
          }

          pageNum += 1;
          that.setData({
            paginationList: paginationList,
            isMax: isMax,
            pageNum: pageNum
          })

          console.log("paginationList---", paginationList);

        })
        .catch((err) => {});
    }
  },

  // 点击tabs
  clickVanTabs(e) {
    let index = e.detail.index;
    // console.log('index---',index);
    this.setData({
      vanTabIndex: index,
    });

    if (index == 0) {

    } else if (index == 1) {

    }

    // 微信自有api，页面置顶
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 0,
    });
  },


  // 点击item
  clickItem(ev){
    let item = ev.currentTarget.dataset.item;
    console.log('item---',item);
    wx.navigateTo({
      url: '/pages/coupon/coupon-details/coupon-details?item=' + JSON.stringify(item),
    });
  },

  // 创建优惠卷按钮
  createCoupon(){
    wx.navigateTo({
      url: '/pages/coupon/coupon-set-up/coupon-set-up',
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.requestList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
