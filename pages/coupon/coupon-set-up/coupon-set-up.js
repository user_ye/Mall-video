// pages/coupon/coupon-set-up/coupon-set-up.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeArray: [], // 请求到的类型
    // changeArray: [], // 下拉框类型
    // changeIndex: 0,

    isTopSelectShop: true, // 顶部是否选择店铺按钮
    selectGoodsItem: [], // 选择的商品，commodity页面选择的商品item集合

    couponName:'', // 优惠卷名称

    isNoThreshold: true, // 使用条件，是否无门槛
    totalAmount:'', // 满***可用， 为0 表示无限制 超过0 表示满减

    denomination:'', // 优惠卷面额，抵扣金额

    issuedNum:'', // 发放数量

    startTime: '', // 开始时间
    endTime: '', // 结束时间
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.getTypeList();   // 获取分类
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  // // 获取分类
  // getTypeList() {
  //   let that = this;
  //   app.http({
  //     // url: app.api.getTypeList,
  //     url: app.api.get_mall_mallType,
  //     data: {},
  //   })
  //     .then((res) => {
  //       let requestData = res.data.data;
  //       console.log('获取分类*222**',requestData);
  //       let changeArray = [];
  //       requestData.map(resMap => {
  //         changeArray.push(resMap.name);
  //       })
  //       console.log('changeArray--333-',changeArray);
  //       let addObj = {
  //         status: 1,
  //         name:'本店通用',
  //       };
  //       that.setData({
  //         typeArray:[addObj,...requestData],
  //         changeArray:[addObj.name,...changeArray],
  //       })
  //     })
  //     .catch((err) => {});
  // },

  // 底部确定按钮
  submitBtn(){
    let that = this;
    console.log('selectGoodsId---',this.data);
    if (!this.data.isTopSelectShop){ // 选择商品按钮
      if (!this.data.selectGoodsItem.length){
        wx.showToast({
          title: '请选择优惠商品',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    }

    if (!this.data.couponName){
      wx.showToast({
        title: '请填写优惠卷名称',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.denomination){
      wx.showToast({
        title: '请填写优惠卷面额',
        icon: 'none',
        duration: 1500
      })
      return;
    }

    if (!this.data.isNoThreshold){ // 不选择无门槛
      if (!this.data.totalAmount || this.data.totalAmount <= 0){
        wx.showToast({
          title: '请填写正确的满减金额',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    }

    if (!this.data.issuedNum){ // 不选择无门槛
      wx.showToast({
        title: '请填写发放数量',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.startTime){ // 开始时间
      wx.showToast({
        title: '请选择开始时间',
        icon: 'none',
        duration: 1500
      })
      return;
    } else if (!this.data.endTime){ // 结束时间
      wx.showToast({
        title: '请选择结束时间',
        icon: 'none',
        duration: 1500
      })
      return;
    }

    // let startTime = new Date(that.data.startTime).getTime() / 1000 // 	优惠券 开始时间，时间戳
    // let endTime = new Date(that.data.endTime).getTime() / 1000 // 优惠券 结束时间， 时间戳


    let goodsId = []; // 选中的商品id集合
    this.data.selectGoodsItem.map(resMap => {
      goodsId.push(resMap.id);
    })
    console.log('goodsId---',goodsId.toString());

    app.http({
      // url: app.api.getTypeList,
      url: app.api.shopCouponCreateCoupon,
      data: {
        status: that.data.isTopSelectShop ? 1 : 2, // 类型：1-全店铺使用，2-指定商品使用
        related_id: that.data.isTopSelectShop ? '' : goodsId.toString(), // 如果是指定 如果类型为2 则传递 商品iD，使用逗号分隔
        name: that.data.couponName, // 优惠券名称 可写可不写 方便 商家在后台展示数据
        total_amount: that.data.isNoThreshold ? 0 : that.data.totalAmount, // 	满减抵扣必填，无条件抵扣时不用填写 为0 表示无限制 超过0 表示满减
        amount: that.data.denomination, // 	抵扣金额
        total: that.data.issuedNum, // 	发放数量
        start_time: that.data.startTime, // 	优惠券开始时间，时间戳
        end_time: that.data.endTime, // 优惠券 结束时间， 时间戳
      },
    })
      .then((res) => {
        let requestData = res.data;
        console.log('创建优惠卷*222**',requestData);
        if (requestData.code == 200){
          wx.showModal({
            // title: '创建成功',
            content: '创建成功',
            showCancel: false, // 是否显示取消按钮，默认为 true
            success (res) {
              if (res.confirm) {
                // console.log('用户点击确定')
                wx.navigateBack({
                  delta: 1
                })
              } else if (res.cancel) {
                // console.log('用户点击取消')

              }
            }
          })
        }
      })
      .catch((err) => {});

  },

  // 选择商品
  selectCommodity(){
    wx.navigateTo({
      url: '/pages/coupon/commodity/commodity?selectGoodsItem=' + JSON.stringify(this.data.selectGoodsItem)
    })
  },

  // 顶部选择店铺按钮
  topSelectShopBtn(ev){
    let type = ev.currentTarget.dataset.type;
    this.setData({
      isTopSelectShop: type
    })
  },

  // 是否选择无门槛
  isSelectNoThreshold(ev){
    let type  = ev.currentTarget.dataset.type;
    // console.log('type---',type);
    this.setData({
      isNoThreshold:type
    })
  },
  // 选择时间picker
  dateBindChange(ev){
    let type = ev.currentTarget.dataset.type
    let value = ev.detail.value
    console.log('选择时间----type---',type)
    console.log('选择时间----value---',value)
    // if (type == 'startTime'){
    //   console.log('------11111---------')
    //   let timeDate = new Date(value).getTime(); //你已知的时间
    //
    //   console.log('t---',timeDate);
    //   let getTime = new Date(timeDate).getTime();
    //   console.log('一分钟后的时间---',getTime + 1000*60); // 一分钟后的时间
    //   console.log('一分钟后的时间---',getTime +1000*60*60*24); // 一天后的时间
    //   console.log('一个月后的时间---',getTime + 1000*60*60*24*30); // 一个月后的时间
    //
    //   console.log("(getTime - 1000*60*60*24) / 1000---", (getTime - 1000 * 60 * 60 * 24) / 1000);
    //   console.log('getTime---',getTime / 1000);
    //   if ((getTime - 1000*60*60*24) / 1000 > getTime){
    //     console.log("昨天---");
    //   }
    //
    // }
    this.setData({
      [type]:value
    })
  },

  // 输入框失去焦点时
  bindBlurInput(ev){
    let type = ev.currentTarget.dataset.type;
    let value = ev.detail.value;
    // console.log('type---',type);
    // console.log('value---',ev.detail.value);

    this.setData({
      [type]:value
    })
  },

  // 选择类型
  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      changeIndex: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
