/*
 * @Author: 红尘痴子
 * @Date: 2020-07-10 11:00:45
 * @LastEditTime: 2020-08-04 19:19:05
 * @LastEditors: 红尘痴子
 * @Description:
 * @FilePath: \miaomeimei\pages\chooseBnkCrd\chooseBnkCrd.js
 * @https://www.jiangcan95.com/
 */

let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		icon: 0,
		withdrawalType: 2, // 提现类型：1银行卡，2微信零钱
		bankCardData: {}, // 银行卡数据
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		if (options.bankCardList){
			this.setData({
				bankCardData: JSON.parse(options.bankCardList),
			});
		}
		if (options.withdrawalType){
			this.setData({
				withdrawalType: options.withdrawalType,
			});
		}
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		// this.requestList();
	},
	// 添加提现账户
	addWdrlAcct() {
		wx.navigateTo({
			url:
				'/pages/verified/verified?bankCardList=' +
				JSON.stringify(this.data.bankCardData),
		});
	},
	// 选择银行卡
	chooseBnkCrd(e) {
		let that = this;

		// 提现类型：1银行卡，2微信零钱
		let type = e.currentTarget.dataset.type;

		let index = e.currentTarget.dataset.index;
		let carddata = e.currentTarget.dataset.carddata;

		that.setData({
			icon: index,
			withdrawalType: type,
		});

		let pages = getCurrentPages(); //获取所有页面
		let currentPage = null; //当前页面
		let prevPage = null; //上一个页面

		if (pages.length >= 2) {
			currentPage = pages[pages.length - 1]; //获取当前页面，将其赋值
			prevPage = pages[pages.length - 2]; //获取上一个页面，将其赋值
		}

		if (prevPage) {
			// let selectBankCardData = prevPage.data.selectBankCardData; // 拿到了上一页js中data里面定义的selectBankCardData

			let bankCardList = this.data.bankCardData;
			bankCardList.list.map((mapRes, mapIndex) => {
				if (index == mapIndex) {
					return (mapRes.is_default = true);
				} else {
					return (mapRes.is_default = false);
				}
			});

			this.setData({
				bankCardData: bankCardList,
			});

			//将想要传的信息赋值给上一个页面data中的值
			prevPage.setData({
				requestData: bankCardList,
				selectBankCardData: this.data.bankCardData.list[index],
				isSelectBankCardPage: true,
				withdrawalType: type, // 提现类型：1银行卡，2微信零钱
			});

			wx.navigateBack({
				//返回上一个页面
				delta: 1,
			});
		}
	},
});
